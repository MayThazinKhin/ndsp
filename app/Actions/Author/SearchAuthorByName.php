<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 08/12/2017
 * Time: 10:43 AM
 */

namespace App\Actions\Author;


class SearchAuthorByName extends BaseAuthorAction
{
    protected function onValidationSuccess()
    {
        $authors = $this->repository->findByName($this->data()['authorName']);
        return $authors;
    }
}