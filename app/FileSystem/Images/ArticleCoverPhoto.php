<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/03/2018
 * Time: 10:39 AM
 */

namespace App\FileSystem\Images;


class ArticleCoverPhoto extends BaseImageFileSystem
{

    private $temp_path;

    public function __construct($file)
    {

        parent::__construct($file);
        $this->path = 'images/articles';
        $this->temp_path = 'images/temp';
    }
}