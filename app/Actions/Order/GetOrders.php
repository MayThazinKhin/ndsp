<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 13/02/2018
 * Time: 3:01 PM
 */

namespace App\Actions\Order;


class GetOrders extends BaseOrderAction
{
    private  $pages=20;
    protected function onValidationSuccess()
    {
        $orders = $this->repository->getAllOrders($this->data()['status'],$this->pages);
        return $orders;
    }
}