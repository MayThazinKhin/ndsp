<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = ['categoryName'];
    protected $hidden = ['pivot'];
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}