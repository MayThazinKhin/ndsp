<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 14/11/2017
 * Time: 8:49 AM
 */

namespace App\Actions\Admin;


class GetAdmins extends BaseAdminAction
{
    protected function onValidationSuccess()
    {
        $admins = $this->repository->paginate();
        return $admins;
    }
}