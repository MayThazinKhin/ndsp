<?php

namespace App\Actions\Author;

class DeleteAuthor extends BaseAuthorAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $category = $this->repository->delete($this->data()->toArray());
        if ($category) {
            return true;
        }
        return false;
    }
}