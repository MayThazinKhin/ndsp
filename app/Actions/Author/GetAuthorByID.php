<?php

namespace App\Actions\Author;

class GetAuthorByID extends  BaseAuthorAction
{
    protected function onValidationSuccess()
    {
        $author = $this->repository->getBy($this->data()['id']);
        return $author;
    }
}