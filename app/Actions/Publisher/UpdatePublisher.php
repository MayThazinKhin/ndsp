<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/19/2017
 * Time: 3:47 PM
 */

namespace App\Actions\Publisher;


class UpdatePublisher  extends BasePublisherAction
{
    protected $rules = [
        'id' => 'required',
        'publisherName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $publisher =$this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($publisher) {
            return true;
        }
        return false;
    }
}