<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/02/2018
 * Time: 2:02 PM
 */

namespace App\Actions\Order;


class GetOrderByPhone extends BaseOrderAction
{
    private  $pages=20;
    protected function onValidationSuccess()
    {
        $orders = $this->repository->getOrderByPhone($this->data()['status'],$this->data()['phone'],$this->pages);
        return $orders;
    }
}