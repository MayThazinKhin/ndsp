<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:43 AM
 */

namespace App\Actions\Article;


use App\Actions\Action;
use App\Repositories\ArticleRepository;
use Illuminate\Support\Collection;

class BaseArticleAction extends Action
{
    protected $rules=[];
    public function __construct(ArticleRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected  function onValidationSuccess()
    {
        // TODO: Implement onValidationSuccess() method.
    }
}