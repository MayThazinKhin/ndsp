<?php


namespace App\Actions\Genre;


class SearchGenreByName extends  BaseGenreAction
{
    protected function onValidationSuccess()
    {
        $genres = $this->repository->findByName($this->data()['genreName']);
        return $genres;
    }
}