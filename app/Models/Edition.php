<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Edition extends Model
{
    public $timestamps = false;
    protected $fillable = ['editionName'];

    public function books()
    {
        return $this->hasMany(Book::class);
    }
}