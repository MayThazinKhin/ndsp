<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 21/02/2018
 * Time: 4:47 PM
 */

namespace App\Actions\Order;


class OrderStatusUpdate extends BaseOrderAction
{
    protected $rules=['id'=>'required','otw_or_deli_or_return'=>'required'];
    protected function onValidationSuccess()
    {
        $order= $this->repository->update($this->data()->toArray(),$this->data()['id']);
        if($order){
            return true;
        }
        return false;
    }
}