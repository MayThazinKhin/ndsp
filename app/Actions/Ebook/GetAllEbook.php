<?php

namespace App\Actions\Ebook;

class GetAllEbook extends  BaseEbookAction
{
    public $pages = 20;
    protected function onValidationSuccess()
    {
        $books = $this->repository->getEbooksWithPaginate($this->pages);
        return $books;
    }
}