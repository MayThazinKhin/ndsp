@extends('news.master')
@section('title','Blog Detail')

@section('content')
<div class="div_2 blogdetail">
    <img src="{{URL::to('image/blog/'.$post->cover_photo)}}" alt="">
    <div class="container">
        <div class="row">
            <div class="col-md-12 detail">
                <div class="a">
                    <a>{{$post->created_at->format('d M y')}}</a>
                </div>
                <h1>{{$post->title}}</h1>
                <p>
                    {!! $post->content !!}
                </p>

                <a href="{{route('news')}}" class="btn pull-right">
                    <span class="glyphicon glyphicon-menu-left"></span> prev
                </a>
            </div>
        </div>
    </div>
</div>
 
@endsection






