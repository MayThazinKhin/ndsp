<?php

namespace App\Actions\Ads;
class GetAdsById extends BaseAdsAction
{
    protected function onValidationSuccess()
    {
        $ads = $this->repository->getBy($this->data()['id']);
        return $ads;
    }
}