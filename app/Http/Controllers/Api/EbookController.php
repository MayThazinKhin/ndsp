<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 10:00 AM
 */

namespace App\Http\Controllers\Api;

use App\Actions\Ebook\Filter\ByCategory;
use App\Actions\Ebook\Filter\ByName;
use App\Actions\Ebook\Filter\FindByName;
use App\Actions\Ebook\GetEBook;
use App\Actions\Ebook\GetEbookByID;
use App\Actions\Ebook\GetRecentEbooks;
use App\FileSystem\PDF_Files\BookPdf;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;
use App\Repositories\EbookRepository;

class EbookController  extends Controller
{
    private $repository, $commentRepository;
    private $pages=2;
    public function __construct(EbookRepository $repository, CommentRepository $commentRepository)
    {
        $this->repository = $repository;
        $this->commentRepository = $commentRepository;
    }

    public function dowloadebook($name)
    {
        $book = new BookPdf($name);
        if ($book->exists()) {
            return $book->getFileResponse();
        }
        return null;
    }

    public function getEbooks()
    {
        $action = new GetEBook($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getRecentEbooks()
    {
        $page = ['page' => 10];
        $action = new GetRecentEbooks($this->repository, collect($page));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getEbookByID($book_id)
    {
        $request = ['id' => $book_id];

        $action = new GetEbookByID($this->repository, $this->commentRepository, collect($request));
        $book = $action->invoke();
        return response()->json($book);
    }

    public function getEbookByName($name)
    {
        $request = ['bookTitle' => $name];
        $action = new ByName($this->repository, collect($request));
        $book = $action->invoke();
        return response()->json($book);
    }

    public function getEbooksByCategoryId($category_id)
    {
        $request = ['category_id' => $category_id];
        $action = new ByCategory($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function searchEBook($name){
        $_req = ['bookTitle'=>$name];
        $action = new FindByName($this->repository,collect($_req));
        $books = $action->invoke();
        return response()->json($books);
    }
}