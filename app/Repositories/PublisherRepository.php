<?php

namespace App\Repositories;

use App\Models\Publisher;

class PublisherRepository extends  Repository
{
    function model()
    {
        return Publisher::class;
    }

    function findByName($name)
    {
        return Publisher::where('publisherName', 'LIKE', $name . '%')->take(10)->get();
    }
}