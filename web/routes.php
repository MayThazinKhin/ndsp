<?php
Route::name('home')->get('/', 'HomeController@index');

Route::name('image')->prefix('image')->group(function () {
    Route::name('image')->get('/book/{name}', 'BookController@getImage');
    Route::name('blog')->get('/blog/{name}', 'ArticleController@getImage');
});

Route::name('book.')->prefix('book')->group(function () {

    Route::name('getdetail')->get('getdetail/{book_id}', 'BookController@detail');
    Route::name('detail')->get('detail', 'BookController@detailView');

});
Route::name('comment/upload')->post('comment/upload', 'CommentController@create');
Route::name('search')->get('/search', 'SearchController@searchIndex');
Route::name('search-all')->get('/search-all', 'SearchController@search');
Route::name('search/bycategory')->get('search/bycategory/{id}', 'SearchController@byCategory');
Route::name('search/by-category-name')->get('search/by-category-name/{name}', 'SearchController@byCategoryName');
Route::name('news')->get('news', 'ArticleController@index');

Route::name('news/detail')->get('news/detail/{id}', 'ArticleController@getDetail');
Route::name('news/category')->get('news/category/{name}', 'ArticleController@byCategory');

Route::name('order')->get('/order', 'OrderController@index');
Route::name('order/create')->post('/order/create', 'OrderController@create');

Route::name('ebooks')->get('ebooks', 'SearchController@getEbook');
Route::name('ebook/download')->get('ebook/download/{name}', 'BookController@dowloadebook');


