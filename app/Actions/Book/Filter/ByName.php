<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 9:57 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;


class ByName extends BaseBookAction
{
    public $pages = 20;

    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->getBookByName($this->data()['bookTitle'], $this->pages,'bookTitle');
    }
}