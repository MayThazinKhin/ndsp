import Vue from 'vue';


new Vue({

    el: '#loginuser',

    data: {

        // bindURL:'/login',
        user:{
            username:'',
            password:'',

        }

    },

    methods: {
        clear(){
            this.user.username = '';
            this.user.password = '';
        },

        activeAdmin(){
            this.clear();
            // this.user.type = 'admin';
            // this.bindURL = '/login';
        },

        activeVolunteer(){
            this.clear();
        }
    },

    mounted(){}

});