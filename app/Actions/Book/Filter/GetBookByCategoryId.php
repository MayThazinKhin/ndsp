<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 20/11/2017
 * Time: 9:48 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class GetBookByCategoryId extends BaseBookAction
{
    public $pages = 2;
    public $filtername='categories';
    protected $rules = ['category_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterById($this->data()['category_id'],$this->pages,$this->filtername);
    }
}