<!-- spotlight map -->
<div class="spotlight">
    <div class="container">
        <div class="row">
            <!-- left side -->
            <div class="col-xs-12 col-sm-3 spotlight-left">
                <ul class="list-shipping">
                    <li class="shipping-item">
                        <em class="icon-plane">
                        </em>
                        <span class="title">Free Shipping</span>
                        <span class="desc">on order over $29</span>

                    </li>

                    <li class="shipping-item">
                        <em class="icon-plane">
                        </em>
                        <span class="title">Free Shipping</span>
                        <span class="desc">on order over $29</span>

                    </li>

                    <li class="shipping-item">
                        <em class="icon-plane">
                        </em>
                        <span class="title">Free Shipping</span>
                        <span class="desc">on order over $29</span>

                    </li>

                    <li class="shipping-item">
                        <em class="icon-plane">
                        </em>
                        <span class="title">Free Shipping</span>
                        <span class="desc">on order over $29</span>

                    </li>
                </ul>
            </div>
            <!-- spotlight map right -->
            <div class="col-xs-12 col-sm-9 spotlight-right">
                <div class="row">
                    <div class="col-xs-3 spotlight-links">
                        <p class="spotlight-header">browser	</p>
                        <ul>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-3 spotlight-links">
                        <p class="spotlight-header">information	</p>
                        <ul>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-3 spotlight-links">
                        <p class="spotlight-header">my account	</p>
                        <ul>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                            <li><a href="#">Books</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-3 spotlight-links">
                        <p class="spotlight-header">location	</p>
                        <ul>
                            <li>Address: 2411 Any Street. Any Town. United </li>
                            <li>Kingdom.</li>
                            <li>Mail to : <a href="#" class="mail-to">support@gmail.com</a></li>
                            <li>Phone : +988323043</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p class="design-by">Designed by <a href="#">MOUNTS</a></p>
        </footer>
    </div>

</div>