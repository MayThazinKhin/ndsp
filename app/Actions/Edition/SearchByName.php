<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 08/12/2017
 * Time: 11:18 AM
 */

namespace App\Actions\Edition;


class SearchByName extends BaseEditionAction
{
    protected function onValidationSuccess()
    {
        $authors = $this->repository->findByName($this->data()['editionName']);
        return $authors;
    }
}