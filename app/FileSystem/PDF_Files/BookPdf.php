<?php
namespace App\FileSystem\PDF_Files;

use App\FileSystem\AbstractFileSystem;
use Illuminate\Support\Facades\Storage;

class BookPdf extends AbstractFileSystem
{
    protected $allowedMimeTypes = [
        'application/pdf'

    ];

    private $temp_path;

    public function __construct($file) {

        parent::__construct($file);
        $this->path = 'pdf/book';
        $this->temp_path = 'pdf/temp';
    }

    public function base64StringToFile($img_info, $img_string)
    {

        $info = pathinfo($img_info);
        $content = $this->decodeBase64String($img_string);
        $name = trim(base64_encode($info['filename'] . str_random(4)), '=');
        $success = Storage::put($this->temp_path . '/' . $name, $content);


        if ($success) {

            $temp = $this->temp_path . '/' . $name;

            $filename = $this->path . '/' . $name . '.' . $info['extension'];

            if (Storage::move($temp, $filename)) return $name . '.' . $info['extension'];
        }
        return null;
    }

    function decodeBase64String($filestring)
    {
        list($type, $data) = explode(';', $filestring);
        list(, $data) = explode(',', $filestring);
        return base64_decode($data);
    }

    public function convertFileToBase64String()
    {
        $base64 = '';

        $type = pathinfo($this->getStoragePath(), PATHINFO_EXTENSION);

        $base64 = 'data:application/' . $type . ';base64,' . base64_encode(Storage::get($this->getPath() . '/' . $this->getStoredName()));

        return $base64;
    }
}