<?php

namespace Admin\Http\Controllers;

use App\Actions\Admin\AdminLogin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $action = new AdminLogin(collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return back()->withError('Invalid username or password.');
        }
        return redirect('/admin/book');
    }

    public function adminLogout()
    {
        Auth::logout();
        return redirect('/admin');
    }
}