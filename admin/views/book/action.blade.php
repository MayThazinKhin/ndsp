@extends('layout.app')
@section('title','Books')
@section('books','selected')
@section('style')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin/new-book.css')}}">

@endsection
@section('content')
    <h3>Add New Book</h3>

    <manage-books inline-template>
        <div class="content-down">
            <div class="card-content">
                <div class="content-left">
                    <img :src="book.bookCoverImgUrl" >

                </div>

                <div class="content-right">

                    <form @submit.prevent="submitdata" autocomplete="off">

                        <!-- Cover Image  -->
                        <div class="form-line">
                            <div class="left">
                                <p>Cover Image <span class="red-star">*</span></p>
                            </div>

                            <div class="right">
                                <div class=" my-input file-field input-field" id="img">
                                    <div class="btn">
                                        <span>File</span>
                                        <input type="file" v-if="!isEdit" for="input-img" v-validate.reject="'required|mimes:image/*'" v-on:change="onFileChange" name="image">
                                        <input type="file" v-if="isEdit" for="input-img"  v-on:change="onFileChange" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="full-width all-border file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Title -->
                        <div class="form-line">
                            <div class="left">
                                <p>Title <span class="red-star">*</span></p>
                            </div>
                            <div class=" right my-input input-field">
                                <input placeholder="Placeholder" id="first_name" type="text" class="validate full-width title-input" v-model="book.bookTitle">
                            </div>
                        </div>

                        <!-- Authors -->
                        <div class="form-line">
                            <div class="left">
                                <p>Authors <span class="red-star">*</span></p>
                            </div>
                            <div class=" right ">

                                <author-select :multi="true" @input="authorChange" :value="selectedAuthors"></author-select>

                            </div>
                        </div>

                        <!-- Categories -->
                        <div class="form-line">
                            <div class="left">
                                <p>Categories <span class="red-star">*</span></p>
                            </div>

                            <div class=" right ">
                                <category-select :multi="true" @input="categoryChange" :value="selectedCategories"></category-select>

                            </div>
                        </div>


                        <!-- Publisher -->
                        <div class="form-line">
                            <div class="left">
                                <p>Publisher <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div class="right-divide">

                                    <publisher-select :multi="false" @input="publisherChange"  :value="selectedPublisher"  ></publisher-select>
                                </div>
                            </div>
                        </div>

                        <!-- Genere -->
                        <div class="form-line">
                            <div class="left">
                                <p>Genere <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div >
                                    <genre-select :multi="true" @input="genreChange" :value="selectedGenres"></genre-select>

                                </div>

                            </div>
                        </div> <!-- Genere end-->

                        <!-- Genere -->
                        <div class="form-line">
                            <div class="left">
                                <p>Edition <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div >
                                    <edition-select :multi="false"  :value="selectedEdition"  @input="editionChange"></edition-select>

                                </div>

                            </div>
                        </div>
                        <!-- Edition -->
                        {{--<div class="form-line">--}}
                            {{--<div class="left">--}}
                                {{--<p>Edition <span class="red-star">*</span></p>--}}
                            {{--</div>--}}
                            {{--<div class=" right my-input input-field right-2">--}}
                                {{--<div class="right-divide">--}}
                                    {{--<edition-select :multi="false"  :value="selectedEdition"  @input="editionChange"></edition-select>--}}
                                {{--</div>--}}
                                {{--<!-- Price -->--}}
                                {{--<div class="right-2-right">--}}
                                    {{--<div class="input-field ">--}}
                                        {{--<input id="first_name" type="number"  class="validate full-width price-input" placeholder="Cost" v-model="book.total_cost_of_books">--}}

                                        {{--<label for="first_name" class="input-price-label"><p>	Cost</p></label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}

                        <!-- if edit, hide div -->
                        <!-- Cost -->
                        <div class="form-line" v-show="!isEdit">
                            <div class="left">
                                <p>Price <span class="red-star">*</span></p>
                            </div>
                            <div class=" right my-input input-field right-2">
                                <div class="right-divide">
                                    <div class="input-field ">
                                        <input id="first_name" type="number" value="0" class="validate full-width price-input" v-model="book.bookSalePrice">

                                    </div>

                                </div>
                                <!-- Qty -->
                                <div class="right-2-right">
                                    <div class="input-field ">
                                        <input id="first_name" type="number"  class="validate full-width price-input" v-model="book.qty">
                                        <label for="first_name" class="input-price-label"><p>	Qty</p></label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Description -->
                        <div class="form-line">
                            <div class="left">
                                <p>Description </p>
                            </div>
                            <div class="right ">
                                <textarea rows="7" v-model="book.bookDescription"></textarea>
                            </div>
                        </div>

                        <!-- is Consigment -->
                        <div class="form-line">
                            <div class="left">

                            </div>
                            <div class="right " style="margin-top: 10px;">
                                <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked" v-model="book.buy_or_consigment"/>
                                <label for="filled-in-box" style="left: 0; color : #333">is Consigment</label>
                            </div>
                        </div>

                        <!-- buttons -->
                        <div class="form-line ">
                            <div class="left">

                            </div>
                            <div class="right save-buttons">
                                <button class="waves-effect waves-light btn close-button"> Close</button>
                                <button class="waves-effect waves-light btn" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </manage-books>

@endsection