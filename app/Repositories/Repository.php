<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/19/2017
 * Time: 9:01 AM
 */

namespace App\Repositories;
use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;

abstract  class Repository
{
    protected  $model;

    public function __construct(App $app) {
        $this->app = $app;
        $this->makeModel();
    }

    public function  all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return $this->model->paginate($perPage, $columns);
    }

    public function getBy($value,$attribute='id',$columns=array('*')){
        return $this->model->where($attribute,'=',$value)->first($columns);
    }

    public function create(array $data=[]) {
        return $this->model->create($data);
    }

    public function update(array $data=[], $id, $attribute='id')
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    public function delete($value,$attribute='id'){
        return $this->model->where($attribute,'=',$value)->delete();
    }

    public function getLimitColumn($count){
        return $this->model()::take($count)->get();
    }

    public function getImage($path,$name){
        $image = $path  . $name;
        if(file_exists($image)){
            return Response::download($image);
        }
        return false;
    }

    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }

    abstract function model();
}