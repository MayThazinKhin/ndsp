<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:48 AM
 */

namespace App\Actions\Article;


class GetArticles extends BaseArticleAction
{
    public $_page = 20;

    protected function onValidationSuccess()
    {
        $articles = $this->repository->paginate($this->_page);
        return $articles;
    }
}