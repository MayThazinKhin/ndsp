<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable=['order_id','buyPrice','qty','created_at'];
    protected $dates=['created_at'];
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
