<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 09/11/2017
 * Time: 2:59 PM
 */

namespace App\Repositories;


use App\Models\Admin;

class AdminRepository extends Repository
{

    function model()
    {
        return Admin::class;
    }
}