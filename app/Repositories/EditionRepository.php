<?php


namespace App\Repositories;


use App\Models\Edition;

class EditionRepository extends Repository
{

    function model()
    {
      return Edition::class;
    }

    function findByName($name){
        return Edition::where('editionName','LIKE',$name.'%')->take(10)->get();
    }
}