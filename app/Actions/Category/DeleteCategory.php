<?php

namespace App\Actions\Category;

class DeleteCategory extends BaseCategoryAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $category = $this->repository->delete($this->data()->toArray());
        if ($category) {
            return true;
        }
        return false;
    }
}