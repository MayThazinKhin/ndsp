<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 13/12/2017
 * Time: 3:09 PM
 */

namespace App\Repositories;


use App\Models\Genre;

class GenreRepository extends Repository
{
    function model()
    {
        return Genre::class;
    }

    function findByName($name){
        return Genre::where('genreName','LIKE',$name.'%')->take(10)->get();
    }
}