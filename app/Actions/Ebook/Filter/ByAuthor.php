<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/01/2018
 * Time: 2:23 PM
 */
namespace App\Actions\Ebook\Filter;
use App\Actions\Ebook\BaseEbookAction;

class ByAuthor  extends BaseEbookAction
{
    public $pages = 20;
    public $filtername = 'authors';
    protected $rules = [
        'author_id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['author_id'], $this->pages, $this->filtername);
    }
}