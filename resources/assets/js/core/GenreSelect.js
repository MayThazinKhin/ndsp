import multiselect from 'vue-multiselect';
export default {
    components: {multiselect},
    props: ['value', 'multi', 'sstyle'],
    template: `            
             <multiselect :class="customstyle" 
                placeholder="Genre"  
                v-model="selectedValues"  
                label="genreName" 
                name="genreName"
                track-by="genreName"  
                :options="collections"
                :multiple="multi"
                :searchable="true"    
                :internal-search="false"
                :taggable="true" 
                @search-change="asyncFind" 
                @input="onValueChange">                                                                          
              </multiselect>`,
    data: function () {
        return {
            isSingleLoading: false,
            collections: [],
            selectedValues: [],
            customstyle: ''
        }
    },
    methods: {
        asyncFind(query) {
            if (query) {
                this.isSingleLoading = true;
                axios.get('/admin/genre/' + query + '/findbyname').then(response => {
                    this.collections = response.data;
                    this.isSingleLoading = false;
                });
            }
        },
        clearAll () {
            this.selectedCountries = []
        },
        onValueChange() {

            let temp = null;

            if (this.isbind) {

                temp = this.value;
                this.selectedValues = this.value;
            } else {

                temp = this.selectedValues;
            }
            this.isbind = false;

            if (this.multi) {

                let _lists = [];

                for (var i = 0, n = temp.length; i < n; i++) {
                    _lists.push(temp[i].id);
                }
                this.$emit('input', _lists);
            } else {

                let _id = null;
                if (temp != undefined) {
                    _id = temp;
                }
                this.$emit('input', _id);
            }
        }
    },
    watch: {
        'value': function () {
            this.isbind=true;
            this.onValueChange();
        }
    }, created: function () {
        this.customstyle = this.sstyle;
    }
}