import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';
export default {
  components: {
    VuePagination, deleteModal
  },

  data: function () {
    return {

      route: {
        delete:'/admin/order/book/delete/',
        updateStatus:'/admin/order/updatestatus'
      },
      pagination: {
        total: 0,
        per_page: 2,
        from: 1,
        to: 0,
        current_page: 1,
        last_page: 1,
      },
      orders: [],
      order_id:null,
      order_url:{email:null,phone:null,address:null,status:null},
      status:{
        confirm:'confirm',
        pending:'pending',
        ontheway:'ontheway',
        deliver:'deliver',
        return:'return'
      }
    }
  },

  methods: {

    getOrderDetails () {
      let url='/admin/order/detail?email='+this.order_url.email+'&phone='+this.order_url.phone+'&address='+this.order_url.address+'&status='+this.order_url.status;


      axios.get(url).then(response => {

        this.orders = response.data;

        if(this.orders.length<=0){ window.location.href='/admin/order';}
      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href = Helper.loginPage();
        } else {
          this.$parent.showToast('Error occured while loading data.');
        }
      });
    },
    onPaginationClick () {
      this.getGenres();
    },
    toggleDelete (id) {
      this.order_id=id;
    },
    onClickStatus(order_id,status){
      axios.post(this.route.updateStatus,{id:order_id,otw_or_deli_or_return:status}).then(response => {

        this.getOrderDetails();

        if(this.orders.length<=0){ window.location.href='/admin/order';}
      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href = Helper.loginPage();
        } else {
          this.$parent.showToast('Error occured while loading data.');
        }
      });
    },


    performdelete () {
      axios.get(this.route.delete+this.order_id).then(response => {
        this.$parent.showToast('Success', 'success');

        this.getOrderDetails();


      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href = Helper.loginPage();
        } else {
          this.$parent.showToast('Error occured while deleting data.');
        }
      });
      $('#delete-modal').modal('close');
    },
    onSearchClick () {

    },
    checkUrl(){
      let email = Helper.getUrlParameter('email');
      let address = Helper.getUrlParameter('address');
      let phone = Helper.getUrlParameter('phone');
      let status = Helper.getUrlParameter('status');
      if (email != null &&phone != null&&address != null) {

        this.order_url.email=email;
        this.order_url.address=address;
        this.order_url.phone=phone;
        this.order_url.status=status;
        this.getOrderDetails( );
      }
    }
  },
  mounted () {
    this.checkUrl();
  },
  computed: {
    total:function () {
      return this.orders.reduce((total, order) => {

        return total + (order.qty * order.book.bookSalePrice);
      }, 0);
    }
  }
}