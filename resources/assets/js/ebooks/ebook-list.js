import VuePagination from '../core/VuePagination';
import MaterialSelect from '../directives/SelectMaterialize';
import AuthorSelect from '../core/AuthorSelect';
import CategorySelect from '../core/CategorySelect';
import GenreSelect from '../core/GenreSelect';
import PublisherSelect from '../core/PublisherSelect';
import EditionSelect from '../core/EditionSelect';
import multiselect from 'vue-multiselect';
import Helper from '../core/Helper';
import deleteModal from '../core/DeleteModal';
export default {
    components: {
        VuePagination, AuthorSelect, CategorySelect, GenreSelect, multiselect, PublisherSelect, EditionSelect,deleteModal
    },
    directives: {MaterialSelect: MaterialSelect},
    data: function () {
        return {
            route: {
                bookURL: '/admin/ebook/all?page=',
                detailURL: '/admin/ebook/action?book_id=',
                deleteURL: '/admin/ebook/delete/',
                manageURL: '/admin/ebook/action',
                findByURL: '/admin/ebook/findby/',

            },
            selectedFilter: null,
            filter_consigment: false,
            selectedOption: {value: "author", text: 'Author'},
            filterOptions: [
                {value: "author", text: 'Author'},
                {value: "category", text: 'Category'},
                {value: "genre", text: 'Genre'},
            ],
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            books: [],
            book_id:null
        }
    },
    methods: {
        getBooks(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.books = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        onPaginationClick() {
            this.getBooks();
        },
        authorChange(value) {

            this.selectedFilter = value;
        },
        categoryChange(value) {

            this.selectedFilter = value;
        },
        genreChange(value){
            this.selectedFilter = value;
        },
        onSearchClick() {
            let _url = '';


            if (this.selectedFilter == null) {
                _url = this.route.bookURL;

            } else {
                _url = this.route.findByURL + this.selectedOption.value + '/' + this.selectedFilter.id + '?page=';
            }


            this.getBooks(_url);
        },
        onOptionValueChange() {
            this.selectedFilter = null;
        },
        performdelete() {
            axios.get(this.route.deleteURL + this.book_id).then(response => {
                this.getBooks(this.route.bookURL);
                this.book_id = null;
                this.$parent.showToast('Success', 'success');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        toggleDelete(id) {

            this.book_id = id;
        },
    },
    mounted() {
        this.getBooks(this.route.bookURL);
    }
}