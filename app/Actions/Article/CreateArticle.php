<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:42 AM
 */
namespace App\Actions\Article;


use App\FileSystem\Images\ArticleCoverPhoto;

class CreateArticle extends BaseArticleAction
{
    protected $rules = [
        'category'=>'required',
        'title' => 'required|max:500',
        'content' => 'required',
        'cover_photo'=>'required|file'
    ];


    protected function onValidationSuccess()
    {
        $book = $this->data();
        
               $bookImage = new ArticleCoverPhoto($book['cover_photo']);
                $cover_name = $bookImage->store();
        
     
        if (!$cover_name) return false;
        $covername=$bookImage->getStoredName();
        $book['cover_photo'] = $covername;

        $author = $this->repository->create($book->toArray());
        if ($author) {
            return true;
        }
        return false;
    }
}