<?php

namespace App\Http\Controllers\Api;

use App\Actions\Category\GetCategories;
use App\Actions\Category\GetTenCategories;
use App\Actions\Category\SearchCategoryByName;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    private  $repository;
    private $pages=2;
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $action = new GetCategories($this->repository);
        $action->_page=$this->pages;
        $categories = $action->invoke();
        return response()->json($categories);
    }

    public function getTenCategory(){
        $action = new GetTenCategories($this->repository);
        $categories = $action->invoke();
        return response()->json($categories);
    }

    public function searchCategory($name){
        $_req = ['categoryName'=>$name];
        $action = new SearchCategoryByName($this->repository,collect($_req));
        $categories = $action->invoke();
        return response()->json($categories);
    }
}

