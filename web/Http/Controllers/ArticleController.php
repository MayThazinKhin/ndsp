<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 26/02/2018
 * Time: 4:30 PM
 */

namespace Web\Http\Controllers;

use App\FileSystem\Images\ArticleCoverPhoto;
use App\Actions\Article\GetArticleByID;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Repositories\ArticleRepository;

class ArticleController extends Controller
{
    private $repository;
    public function __construct(ArticleRepository $repository)
    {

        $this->repository=$repository;
    }

    public function index(){
        $articles= Article::orderBy('created_at','desc')->paginate(10);
        return view('news.index',compact('articles'));
    }

    public function byCategory($name){
       $articles= Article::where('category',$name)->orderBy('created_at','desc')->paginate(10);
        return view('news.index',compact('articles'));
    }

    public function getDetail($id){
        $request = ['id' => $id];
        $action = new GetArticleByID($this->repository, collect($request));
        $post = $action->invoke();
        return view('news.detail',compact('post'));
    }

      public function getImage($name)
    {
        $book = new ArticleCoverPhoto($name);
        if($book->exists()){
            return $book->getFileResponse();
        }
        return null;
    }
}