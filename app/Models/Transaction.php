<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps=false;
    protected $fillable=['book_id','total_cost_of_books','qty','created_at','fix_qty'];
    protected $dates=['created_at'];


    public function book(){
        return $this->belongsTo(Book::class);
    }
}