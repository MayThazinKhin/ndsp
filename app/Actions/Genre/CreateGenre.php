<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:06 AM
 */

namespace App\Actions\Genre;


class CreateGenre extends BaseGenreAction
{
    protected $rules = [
        'genreName' => 'required|max:45'
    ];


    protected function onValidationSuccess()
    {
        $author = $this->repository->create($this->data()->toArray());
        if ($author) {
            return true;
        }
        return false;
    }
}