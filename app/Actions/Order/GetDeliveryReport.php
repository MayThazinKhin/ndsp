<?php


namespace App\Actions\Order;


class GetDeliveryReport extends BaseOrderAction
{
    private $pages = 10;


    public function onValidationSuccess()
    {
        $orders = $this->repository->getDeliveryReport($this->data()['start_date'], $this->data()['end_date']);
        return $orders;
    }
}