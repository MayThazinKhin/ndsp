<?php

namespace App\Actions\Category;

class CreateCategory extends BaseCategoryAction
{
    protected $rules = [
        'categoryName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $category = $this->repository->create($this->data()->toArray());
        if ($category) {
            return true;
        }
        return false;
    }
}