import './bootstrap';

const app = new Vue({
    el: '#app',
    data: {
        message: '',
        theme: 'v-toast-info',
        error: 'v-toast-error',
        warning: 'v-toast-warning',
        success: 'v-toast-success',
        info: 'v-toast-info'
    },
    methods: {
        showToast(message, messagetype) {
            switch (messagetype) {
                case 'error':
                    this.theme = this.error;
                    break;
                case 'warn':
                    this.theme = this.warning;
                    break;
                case 'info':
                    this.theme = this.info;
                    break;
                case 'success':
                    this.theme = this.success;
                    break;
                default :
                    this.theme = this.info;
            }
            this.message = message;
            this.$toaster.add(this.message, {theme: this.theme})
        }
    }
});