@extends('layout.master')
@section('title','Book Detail')
@section('book','active')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/detail.css')}}">
@endsection
@section('content')
    <!-- HOME -->
    <div class="container">
        <!-- SECTION 3 -->
        <section id="section-3">
            <hr>
            <div class="row section-3-row">
                <!-- left side menu -->
                <div class="div">
                    <div class="col-sm-2 left-bar">
                        <div class="search-box">
                            <form action="{{route('search-all')}}" method="get">

                                <input type="text" class="form-control"  placeholder="Search" id="srch-term" name="paramname" >
                                <button id="srch-but" type="submit">

                                </button>
                            </form>
                        </div>
                        <h1 class="heading">BROWSE BOOKS</h1>
                        <h2 class="category-header">Category</h2>
                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{route('search/bycategory',['id'=> $category->id])}}">{{$category->categoryName}}</a></li>
                            @endforeach
                                <li><a href="{{route('ebooks')}}">Ebook</a></li>
                        </ul>
                    </div> <!-- left side menu end -->
                </div>

                <!-- right side  -->
                <!-- New Product -->
                <div class="col-sm-10 right-bar">

                    <div class="row" style="padding: 0px; margin: 0px;">

                            <h1 class="new-products-text">NEW PRODUCTS</h1>


                    </div>
                    @foreach($books as $book)
                    <div class="book-grid">
                        <div class="product-img">
                            <a href="{{URL::to('book/detail?book_id='.$book->id)}}">
                                <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                            </a>
                        </div>
                        <div class="book-des">
                            <h4 class="product-name">
                                <a href="{{URL::to('book/detail?book_id='.$book->id)}}" class="pull-left" data-toggle="tooltip" title="Book Title With very very very long text">{{$book->bookTitle}}</a>
                            </h4>
                            <p class="author">{{$book->author}}</p>
                            @if($book->bookSalePrice!=0)
                            <p class="product-price">{{number_format($book->bookSalePrice)
                            }}</p>
                        @endif
                        </div>

                    </div>
                    @endforeach
                </div>
            </div>
            <div style="text-align: center;">
            {{ $books->links() }}
            </div>
        </section>

    </div><!-- end main container -->
@endsection
