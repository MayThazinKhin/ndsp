<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 13/12/2017
 * Time: 3:13 PM
 */

namespace Admin\Http\Controllers;


use App\Actions\Genre\CreateGenre;
use App\Actions\Genre\DeleteGenre;
use App\Actions\Genre\GetGenreByID;
use App\Actions\Genre\GetGenres;
use App\Actions\Genre\SearchGenreByName;
use App\Actions\Genre\UpdateGenre;
use App\Http\Controllers\Controller;

use App\Repositories\GenreRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class GenreController extends  Controller
{


    private $repository;

    public function __construct(GenreRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('genre.index');
    }

    public function getByName($name)
    {
        $request = ['genreName' => $name];
        $action = new SearchGenreByName($this->repository, collect($request));
        $genres = $action->invoke();
        return response()->json($genres);
    }

    public function create(Request $request)
    {
        $action = new CreateGenre($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {

        $action = new UpdateGenre($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $_req = ['id' => $id];
        $action = new DeleteGenre($this->repository, collect($_req));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return $result->errors();
        }
        return response()->json($result);

    }

    public function getAll()
    {
        $action = new GetGenres($this->repository);
        $genres = $action->invoke();
        return response()->json($genres);
    }

    public function getByID($id)
    {
        $_req = ['id' => $id];
        $action = new GetGenreByID($this->repository, collect($_req));
        $genre = $action->invoke();
        return $genre;
    }
}