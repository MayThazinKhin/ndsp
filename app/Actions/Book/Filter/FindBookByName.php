<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:52 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class FindBookByName extends BaseBookAction
{


    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->findByBookName($this->data()['bookTitle']);
    }
}