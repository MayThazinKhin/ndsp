<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/01/2018
 * Time: 10:16 AM
 */

namespace App\Http\Controllers\Api;


use App\Actions\Publisher\GetPublishers;
use App\Actions\Publisher\SearchByPublisher;
use App\Http\Controllers\Controller;
use App\Repositories\PublisherRepository;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    private $repository;
    private $pages=2;
    public function __construct(PublisherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $action = new GetPublishers($this->repository);
            $action->_page=$this->pages;
        $publishers = $action->invoke();
        return response()->json($publishers);
    }


    public function searchPublisher($name){
        $_req = ['publisherName'=>$name];
        $action = new SearchByPublisher($this->repository,collect($_req));
        $publishers = $action->invoke();
        return response()->json($publishers);
    }
}