<?php

namespace App\Repositories;

use App\Models\Book;

class BookRepository extends Repository
{
    private $id = 'id';
    private $bookTitle = 'bookTitle';
    private $bookCoverImgUrl = 'bookCoverImgUrl';
    private $bookSalePrice = 'bookSalePrice';
    private $ebook_size = 'ebook_size';
    private $notEbook = 0;

    function model()
    {
        return Book::class;
    }

    public function getBooksWithPaginate($page)
    {
        $books= Book::with('edition', 'publisher')->where('isEbook', 0)->orderBy('created_at','desc')->paginate($page);
        return $books;
    }

    public function getBooks($page)
    {
        $books = $this->model()::select($this->id, $this->bookTitle, $this->bookCoverImgUrl, $this->bookSalePrice)
                ->where('isEbook', '=', $this->notEbook)
                ->orderBy('created_at', 'desc')
                ->paginate($page);
        return $books;
    }

    public function getBookByID($id)
    {
        $book = Book::with('edition', 'publisher', 'genres', 'authors', 'categories','comments')->where('id', '=', $id)->first();
        return $book;
    }

    public function remove($id)
    {
        $_book = Book::find($id);
        if ($_book) {
            $_book->delete();
            return $_book;
        }
        return null;
    }

    public function getLastBooks()
    {
        $books = Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl,$this->bookSalePrice)
                    ->where('isEbook', '=', 0)
                    ->take(10)
                    ->orderBy('created_at', 'desc')
                    ->get()
                    ->makeHidden(['ebook_size']);
        return $books;
    }

    public function getTopTenBooks()
    {
        $books = Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl)
                ->where('isEbook', '=', $this->notEbook)
                ->whereHas('categories', function ($query) {$query->where('categoryName', 'Top');})
                ->take(10)
                ->orderBy('created_at', 'desc')
                ->get()->makeHidden([$this->ebook_size]);
        return $books;
    }

    public function getHotbooks()
    {
        $books = $this->model()::where('isEbook', '=', $this->notEbook)
                  ->whereHas('categories', function ($query) {$query->where('categoryName', 'Top');})
                  ->take(20)->orderBy('created_at', 'desc')->get()->makeHidden([$this->ebook_size]);
        return $books;
    }

    public function getTopAllBooks()
    {
        $books = Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl, $this->bookSalePrice)
                 ->where('isEbook', '=', 0)
                 ->whereHas('categories', function ($query) {
                    $query->where('categoryName', 'Top');})
                 ->orderBy('created_at', 'desc')->paginate(2);
        return $books;
    }

    public function filterById($id, $page, $filterby = 'authors')
    {
        $books= Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl, $this->bookSalePrice)
                ->whereHas($filterby, function ($q) use ($id) {
                    $q->where('id', $id);})
                ->where('isEbook', '=', $this->notEbook)
                ->orderBy('created_at', 'desc')
                ->paginate($page);
        return $books;
    }

    public function filterBy($id, $consigment, $page, $filterby = 'authors')
    {
        $books= Book::with('publisher', 'edition')->whereHas($filterby, function ($q) use ($id) {
            $q->where('id', $id);
        })->where('buy_or_consigment',$consigment)->orderBy('created_at', 'desc')->paginate($page);
        return $books;
    }

    public function filterByPublisherId($publisher_id,$consigment, $page)
    {
        $books=Book::with('publisher', 'edition')
                ->where('publisher_id', $publisher_id)
                ->where('buy_or_consigment',$consigment)
                 ->orderBy('created_at', 'desc')
                ->paginate($page);
        return $books;
    }

    public function getBooksByPublisherId($publisher_id,$page)
    {
        return Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
                ->where('publisher_id', $publisher_id)
                ->where('isEbook', $this->notEbook)
                
                 ->orderBy('created_at', 'desc')
                ->paginate($page);
    }

    public function getBookByPublisherName($name, $pages)
    {
        $books = Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
            ->where('isEbook', '=', $this->notEbook)
            ->whereHas('publisher', function ($q) use ($name) {
                $q->where('publisherName', 'LIKE', $name.'%');
            })
             ->orderBy('created_at', 'desc')
            ->paginate($pages);
        return $books;
    }

    public function filterByConsigment($consigment, $page)
    {
        $books= Book::with('publisher', 'edition')->where('buy_or_consigment',$consigment)
                 ->orderBy('created_at', 'desc')
                ->paginate($page);
        return $books;
    }

    public function getBookByName($name, $pages, $attributeName = 'bookTitle')
    {
        $books = Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
                ->where($attributeName, 'LIKE', $name . '%')
                ->where('isEbook', '=', $this->notEbook)
                 ->orderBy('created_at', 'desc')->paginate($pages);
        return $books;
    }

    public function findByBookName($name)
    {
        $books = Book::
            where('bookTitle', 'LIKE', $name . '%')
            ->where('isEbook', '=', $this->notEbook)
             ->orderBy('created_at', 'desc')->select('id','bookTitle')->take(10)->get();
        return $books->makeHidden(['author','ebook_size','category','rating']);
    }

    public function asyncByBookName($name)
    {
        $books = Book::
        where('bookTitle', 'LIKE', $name . '%')
            ->where('isEbook', '=', $this->notEbook)
            ->orderBy('bookTitle', 'ASC')->take(10)->get();
        return $books->makeHidden(['ebook_size','category','rating']);
    }

    public function getBookByFilterName($name, $pages, $attributeName = 'authorName', $filterby = 'authors')
    {
        $paramname = $name;

        $books= Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
                ->whereHas($filterby, function ($q) use ($paramname, $attributeName) {
                        $q->where($attributeName, 'LIKE', $paramname . '%');})
                ->where('isEbook', $this->notEbook)
                ->orderBy('created_at', 'desc')
                ->paginate($pages);
        return $books;
    }
}