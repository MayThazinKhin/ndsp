<?php

namespace App\Actions\Ebook;

use App\Repositories\CommentRepository;
use App\Repositories\EbookRepository;
use Illuminate\Support\Collection;

class GetEbookByID extends BaseEbookAction
{
    private $commentRepository;

    public function __construct(EbookRepository $repository, CommentRepository $commentRepository, Collection $data = null)
    {
        parent::__construct($repository, $data);
        $this->commentRepository = $commentRepository;
    }


    protected function onValidationSuccess()
    {
        $book = $this->repository->getEbookByID($this->data()['id']);

        $comments = $this->commentRepository->getFiveComments($this->data()['id']);

        return ['book' => $book, 'comments' => $comments];
    }
}