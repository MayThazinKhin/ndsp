import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';

export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {
            isEdit: false,
            route: {
                loginpage:'/admin',
                getAuthors: '/admin/author/all?page=',
                create: '/admin/author/create',
                edit: '/admin/author/edit',
                delete: '/admin/author/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            authors: [],
            author: {id: null, authorName: null}
        }
    },

    methods: {
        clearData() {
            this.author.id = null;
            this.author.authorName = null;
            this.$validator.clean();
        },
        getAuthors(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.authors = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        onPaginationClick() {
            this.getAuthors();
        },

        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(author) {
            var temp = Object.assign({}, author);
            this.author = temp;
            this.isEdit = true;
        },

        toggleDelete(id) {

            this.author.id = id;
        },
        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(error => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.author).then(response => {
                this.$parent.showToast('Success', 'success');
                this.getAuthors(this.route.getAuthors);
                $('#modal1').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.author.id + '/delete').then(response => {
                this.getAuthors(this.route.getAuthors);
                this.author.id = null;
                this.$parent.showToast('Success', 'success');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getAuthors(this.route.getAuthors);
        }
    },

    mounted() {
        this.getAuthors(this.route.getAuthors);
    }
}