<?php


namespace App\Actions\Book;


class GetBooks extends BaseBookAction
{
    public $pages = 2;
    protected function onValidationSuccess()
    {
        $books = $this->repository->getBooks($this->pages);
        return $books;
    }
}