<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:51 AM
 */

namespace App\Actions\Ebook\Filter;




use App\Actions\Ebook\BaseEbookAction;

//no pagination for autocomplete
class FindByName extends BaseEbookAction
{
    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->findByEBookName($this->data()['bookTitle']);
    }
}