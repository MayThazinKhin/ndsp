<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $i) {
            \App\Models\Genre::create([
                'genreName' => $this->faker->word
            ]);
        }
    }
}
