<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

trait ControllerTrait
{
    protected $repository;
    protected $pk_ID='id';

    abstract function index();

    abstract function create(Request $request);

    abstract function update(Request $request);

    abstract function delete($id);

    abstract function getAll();

    abstract function getByID($id);
}