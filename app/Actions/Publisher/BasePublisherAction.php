<?php

namespace App\Actions\Publisher;

use App\Actions\Action;
use App\Repositories\PublisherRepository;
use Illuminate\Support\Collection;

class BasePublisherAction extends Action
{
    protected $rules = [];

    public function __construct(PublisherRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected function onValidationSuccess(){}
}