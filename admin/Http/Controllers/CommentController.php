<?php

namespace Admin\Http\Controllers;

use App\Actions\Comment\DeleteComment;
use App\Actions\Comment\GetComments;
use App\Repositories\CommentRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class CommentController extends Controller
{
    private $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('comments.index');
    }

    public function delete($id)
    {

        $request = ['id' => $id];
        $action = new DeleteComment($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        if(!$result){
            return response()->json(['status'=>'failed'],500);
        }
        return response()->json($result);
    }

    public function getAllComments(){
        $action = new GetComments($this->repository);
        $result = $action->invoke();
        return response()->json($result);
    }
}