<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 9:30 AM
 */
namespace App\Actions\Ads;

class GetAllAds extends BaseAdsAction
{
    protected function onValidationSuccess()
    {
        $ads = $this->repository->paginate();
        return $ads;
    }
}