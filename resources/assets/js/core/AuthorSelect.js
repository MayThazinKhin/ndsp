import multiselect from 'vue-multiselect';
export default {
    components: {multiselect},
    props: ['value', 'multi', 'sstyle'],
    template: `            
             <multiselect :class="customstyle" 
                placeholder="Author"  
                v-model="selectedValues"  
                label="authorName" 
                name="authorName"
                track-by="authorName"  
                :options="collections"
                :multiple="multi"
                :searchable="true"    
                :internal-search="false"
                :taggable="true" 
                @search-change="asyncFind" 
                @input="onAuthorChange">                                                                          
              </multiselect>`,
    data: function () {
        return {
            isSingleLoading: false,
            collections: [],
            selectedValues: [],
            customstyle: '',
            isbind:false
        }
    },
    methods: {
        asyncFind(query) {
            if (query) {
                this.isSingleLoading = true;
                axios.get('/admin/author/' + query + '/findbyname').then(response => {
                    this.collections = response.data;
                    this.isSingleLoading = false;
                });
            }
        },
        clearAll() {
            this.selectedCountries = []
        },
        onAuthorChange() {
            let temp = null;

            if (this.isbind) {

                temp = this.value;
                this.selectedValues = this.value;
            } else {

                temp = this.selectedValues;
            }
            this.isbind = false;

            if (this.multi) {

                let _lists = [];

                for (var i = 0, n = temp.length; i < n; i++) {
                    _lists.push(temp[i].id);
                }
                this.$emit('input', _lists);
            } else {

                let _id = null;
                if (temp != undefined) {
                    _id = temp;
                }
                this.$emit('input', _id);
            }
        }
    },
    watch: {
        'value': function () {
            this.isbind=true;
            this.onAuthorChange();
        }
    },
    created: function () {
        this.customstyle = this.sstyle;
    }
}