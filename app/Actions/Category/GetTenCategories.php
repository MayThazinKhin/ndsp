<?php
/**
 * Created by PhpStorm.
 * User: yanyan
 * Date: 11/10/2017
 * Time: 11:31 PM
 */

namespace App\Actions\Category;


use App\Repositories\CategoryRepository;

class GetTenCategories extends BaseCategoryAction
{

   public function onValidationSuccess()
   {
      $categories = $this->repository->getLimitColumn(10);
      return $categories;
   }
}