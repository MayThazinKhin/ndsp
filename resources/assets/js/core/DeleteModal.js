export default {
    template: `
            <div id="delete-modal" class="modal">
                <div class="modal-content">
                    <h4>Are you sure to delete?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="modal-action modal-close waves-effect waves-green btn-flat">No</button>
                    <button v-on:click.prevent="$emit('input')" class="modal-action waves-effect waves-green btn-flat add-btn">Yes</button>
                </div>
            </div>`
}