<?php

namespace App\Actions\Book;

use App\Actions\Stock\CreateStock;
use App\Actions\Transaction\CreateTransaction;
use App\FileSystem\Images\BookImage;

use App\Repositories\BookRepository;
use App\Repositories\StockRepository;
use App\Repositories\TransactionRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Validation\Validator;


class CreateBook extends BaseBookAction
{
    protected $rules = [
        'edition_id' => 'required',
        'publisher_id' => 'required',
        'bookTitle' => 'required',
        'bookSalePrice' => 'required',
        'bookCoverImgUrl' => 'required',
    ];

    private $tranRepo, $stockRepo;

    public function __construct(BookRepository $repository, TransactionRepository $tranRepo, Collection $data = null)
    {
        parent::__construct($repository, $data);
        $this->tranRepo = $tranRepo;

    }

    protected function onValidationSuccess()
    {
        $book = $this->data();

        $bookImage = new BookImage($book['bookCoverImgUrl']);

        $cover_name = $bookImage->base64StringToImage($book['coverInfo'], $book['bookCoverImgUrl']);

        if (!$cover_name) return false;

        $book['bookCoverImgUrl'] = $cover_name;


        $insertedBook = $this->repository->create($book->toArray());

        if ($insertedBook) {

            //insert into author_book table
             $insertedBook->authors()->attach($book['authors']);

            //insert into category_book table
            $insertedBook->categories()->attach($book['categories']);

            //insert into genre_book table
            $insertedBook->genres()->attach($book['genres']);

            $transaction = new \stdClass();
            $transaction->book_id = $insertedBook['id'];
            $transaction->total_cost_of_books = $book['total_cost_of_books'];
            $transaction->qty = $book['qty'];
            $transaction->fix_qty = $book['qty'];
            $transaction->created_at = Carbon::now();
            $action = new CreateTransaction($this->tranRepo, collect($transaction));
            $action->invoke();



            return true;
        }

        return false;
    }
}