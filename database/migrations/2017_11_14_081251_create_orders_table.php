<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('book_id');
            $table->integer('qty')->default(0);
            $table->string('email',45);
            $table->string('phone',255);
            $table->mediumText('address');
            /** otw (on the way)=o
             * deli(delivery)=d
             * return = r**/
            $table->string('otw_or_deli_or_return')->nullable();
            

            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');

           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
