<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 02/01/2018
 * Time: 3:42 PM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByGenre extends BaseBookAction
{
    public $pages = 20;
    public $filtername='genres';
    protected $rules = ['genre_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['genre_id'],$this->data()['buy_or_consigment'], $this->pages,$this->filtername);
    }
}