<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 12:23 PM
 */

namespace App\Actions\Transaction;


class DeleteTransaction extends  BaseTransactionAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $transaction = $this->repository->delete($this->data()->toArray());
        if ($transaction) {
            return true;
        }
        return false;
    }
}