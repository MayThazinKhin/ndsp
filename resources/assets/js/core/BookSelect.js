import multiselect from 'vue-multiselect';
export default {
    components: {multiselect},
    props: ['value', 'multi', 'sstyle','name'],
    template: `            
             <multiselect :class="customstyle" 
                placeholder="Book"  
                v-model="selectedValues"  
                label="bookTitle" 
                name="book"
                track-by="bookTitle"  
                :options="collections"
                :multiple="multi"
                :searchable="true"    
                :internal-search="false"
                :taggable="true" 
                @search-change="asyncFind" 
                @input="onValueChange">                                                                          
              </multiselect>`,
    data: function () {
        return {
            isSingleLoading: false,
            collections: [],
            selectedValues: [],
            customstyle: ''
        }
    },
    methods: {
        asyncFind(query) {
            if (query) {
                this.isSingleLoading = true;
                axios.get('/api/book/search/' + query ).then(response => {
                    this.collections = response.data;
                    this.isSingleLoading = false;
                });
            }
        },
        clearAll () {
            this.selectedCountries = []
        },
        onValueChange() {

            let temp = null;

            if (this.isbind) {

                temp = this.value;
                this.selectedValues = this.value;
            } else {

                temp = this.selectedValues;
            }
            this.isbind = false;

            if (this.multi) {

                let _lists = [];

                for (var i = 0, n = temp.length; i < n; i++) {
                    _lists.push(temp[i].id);
                }
                this.$emit('input', _lists);
            } else {

                let _id = null;
                if (temp != undefined) {
                    _id = temp;
                }
                this.$emit('input', _id);
            }
        }
    },
    watch: {
        'value': function () {
            this.isbind=true;
            this.onValueChange();
        }
    }, created: function () {
        this.customstyle = this.sstyle;
    }
}