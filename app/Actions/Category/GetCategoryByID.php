<?php

namespace App\Actions\Category;

class GetCategoryByID extends  BaseCategoryAction
{
    protected function onValidationSuccess()
    {
        $category = $this->repository->getBy($this->data()['id']);
        return $category;
    }
}