<?php

namespace App\Actions\Author;


class CreateAuthor extends BaseAuthorAction
{
    protected $rules = [
        'authorName' => 'required|max:45'
    ];


    protected function onValidationSuccess()
    {
        $author = $this->repository->create($this->data()->toArray());
        if ($author) {
            return true;
        }
        return false;
    }
}