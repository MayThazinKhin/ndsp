<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/01/2018
 * Time: 1:55 PM
 */

namespace App\Actions\Ebook;


use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;
use App\Repositories\EbookRepository;
use Illuminate\Support\Collection;

class UpdateEbook extends  BaseEbookAction
{
    protected $rules = [
        'id' => 'required',
        'edition_id' => 'required',
        'publisher_id' => 'required',
        'bookTitle' => 'required',
        'bookSalePrice' => 'required'

    ];

    public function __construct(EbookRepository $repository, Collection $data = null)
    {
        parent::__construct($repository, $data);

    }

    protected function onValidationSuccess()
    {
        $input = $this->data()->except('_token', 'edition', 'publisher');

        $oldBook = $this->repository->getBy($input['id']);

        if (isset($input['coverInfo'])) {
            $bookImage = new BookImage($input['bookCoverImgUrl']);
            $cover_name = $bookImage->base64StringToImage($input['coverInfo'], $input['bookCoverImgUrl']);
            $input['bookCoverImgUrl'] = $cover_name;

            $bookImage = new BookImage($oldBook['bookCoverImgUrl']);
            $bookImage->delete();

        } else {
            $input['bookCoverImgUrl'] = $oldBook->bookCoverImgUrl;
        }

        if (isset($input['ebookinfo'])) {
            $pdf = new BookPdf($input['e_book_download']);
            $ebookname = $pdf->base64StringToFile($input['ebookinfo'], $input['e_book_download']);
            $input['e_book_download'] = $ebookname;

            $ebookpdf = new BookPdf($oldBook['e_book_download']);
            $ebookpdf->delete();
        } else {
            $input['e_book_download'] = $oldBook->e_book_download;
        }


        $book = $oldBook->update($input->toArray());
        if ($book) {
            //delete related authors from author_book

            $oldBook->authors()->detach();
            $oldBook->authors()->attach($input['authors']);

            //delete related categories from author_book
            $oldBook->categories()->detach();
            $oldBook->categories()->attach($input['categories']);

            //delete related categories from author_book
            $oldBook->genres()->detach();
            $oldBook->genres()->attach($input['genres']);


            return true;
        }
        return false;
    }
}