<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 21/11/2017
 * Time: 9:30 AM
 */

namespace App\Actions\Reports;

use App\Models\Book;
use App\Models\Order;
use App\Models\Sale;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CalculateMonthlyReport
{
    private $deliver = 'd';
    private $otw = 'o';
    private $return = 'r';


    public function exportReport($date)
    {
        $book_id = 2;

        $deliver = $this->deliver;
//income
        $transactionOpening = Transaction::select(DB::raw('SUM(qty) as total_qty'), DB::raw('SUM(total_cost_of_books)as total_cost'))->
                              where('book_id', 2)->where('created_at', '<', $date)->
                              groupBy('book_id')->get();
//expense
        $orderOtwOpening = Order::select(DB::raw('SUM(qty) as total_qty'), DB::raw('SUM(total_cost_of_books)as total_cost'))->
                           where('book_id', 2)->
                           where('otw_or_deli_or_return', $this->otw)->
                           where('created_at', '<', $date)->get();

//income
        $orderReturnOpening = Order::where('book_id', 2)->where('otw_or_deli_or_return', $this->return)->where('created_at', '<', $date)->get();

//expense
        $saleOpening = Sale::whereHas('order', function ($q) use ($deliver) {
            $q->where('book_id', 2)->where('otw_or_deli_or_return', '=', $deliver);
        })->where('created_at', '<', $date)->select(DB::raw('SUM(qty) as total_qty'), DB::raw('SUM(salePrice)as total_sale_price'))->get();

return $transactionOpening;
//return $saleOpening;

//        $fromDate=Carbon::create(2017,11,20);
//        $toDate=Carbon::create(2017,11,21);
//
//        $book = Book::where('id',2)->first();
//        $openingQty=0;$openingAmount=0;
//        $receiveQty=0;$receiveAmount=0;
//        $salesQty=0;$salesAmount=0;
//        $returnQty=0;$returnAmount=0;
//        $owtQty=0;$otwAmount=0;
//        $closingQty=0;$closingAmount=0;
//
//
//        //Calculate Opening
//
//          $openingTransaction=  Transaction::where('book_id',$book->id)->where('created_at',$fromDate)->get()->sum('qty');


        //SELECT BOOK
        //Loop ALl BOOK
        //FIND BOOK AND GROUP
        //transaction   Qty+

        //Otw       Qty-
        //Return    Qty+

        //Sale      Qty-

        //Insert
    }
}