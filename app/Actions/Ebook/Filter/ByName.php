<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 11:09 AM
 */

namespace App\Actions\Ebook\Filter;


use App\Actions\Ebook\BaseEbookAction;

class ByName extends BaseEbookAction
{
    public $pages = 20;

    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->getBookByName($this->data()['bookTitle'], $this->pages);
    }
}