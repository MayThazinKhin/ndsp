@extends('layout.app')
@section('title','Order')
@section('order','active')
@section('content')
    <order-detail inline-template>
    <div>
    <h3>View Order Book List</h3>

    <div class="content-down">
        <delete-modal @input="performdelete"></delete-modal>
        <div class="card-content">

            <!-- Data Lists -->
            <table class="highlight striped data-table">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <tr v-for="order,index in orders">
                    <td>@{{pagination.from+index}}</td>
                    <td>@{{ order.book.bookTitle }}</td>
                    <td>@{{ order.book.bookSalePrice }}</td>
                    <td>@{{ order.qty }}</td>
                    <td>@{{ order.book.bookSalePrice* order.qty}}</td>
                    <td>
                        <button class="btn-floating " @click="onClickStatus(order.id,status.confirm)" v-if="order_url.status==status.pending">
                            <i class="material-icons fa  fa-check-circle-o"></i>
                        </button>

                        <button class="btn-floating " @click="onClickStatus(order.id,status.ontheway)" v-if="order_url.status==status.confirm">
                            <i class="material-icons fa   fa-bus"></i>
                        </button>
                        <button class="btn-floating " @click="onClickStatus(order.id,status.deliver)" v-if="order_url.status==status.ontheway">
                            <i class="material-icons fa  fa-user"></i>
                        </button>
                        <button class="btn-floating " @click="onClickStatus(order.id,status.return)" v-if="order_url.status==status.ontheway || order_url.status==status.deliver">
                            <i class="material-icons fa   fa-mail-reply"></i>
                        </button>
                        <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(order.id)"><i class="material-icons fa fa-trash-o"></i></button>

                    </td>
                </tr>

                <!-- last row  Total Amount -->
                <tr class="total-row">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="total-amount">Total Amount:</td>
                    <td class="total-amount">@{{ total }}</td>

                </tr>






                </tbody>
            </table>
        </div><!-- card-content -->

    </div><!-- content-down end -->
    </div>
    </order-detail>
@endsection