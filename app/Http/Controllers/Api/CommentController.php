<?php

namespace App\Http\Controllers\Api;

use App\Actions\Comment\CreateComment;
use App\Actions\Comment\GetAllComment;
use App\Actions\Comment\GetCommentByBookID;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class CommentController extends Controller
{

    private $repository;
    private $pages=2;
    public function __construct(CommentRepository $repository)
    {

      $this->repository = $repository;

    }

    public function create(Request $request){
//                $datas=file_get_contents(storage_path().'/comment.json');
//                $_comments=json_decode($datas);

        $_comments= json_decode($request['comments'],true);
        $action = new CreateComment($this->repository,collect($_comments));
        $result = $action->invoke();
        if (!$result) {
            return response()->json(['success'=>'false']);
        }
        return response()->json(['success'=>'true']);
    }

    public function getByBookID($book_id){

        $rq = ['book_id'=>$book_id];

        $action = new GetCommentByBookID($this->repository,collect($rq));
        $action->_page=$this->pages;
        $result = $action->invoke();
        if (!$result) {
            return response()->json(['success'=>'false']);
        }
        return response()->json($result);
    }
}
