<?php


namespace App\Actions\Edition;


class DeleteEdition extends  BaseEditionAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $edition = $this->repository->delete($this->data()->toArray());
        if ($edition) {
            return true;
        }
        return false;
    }
}