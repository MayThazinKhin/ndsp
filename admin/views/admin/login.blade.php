<html>

<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/admin/login.css')}}" charset="utf-8">
</head>

<body>
<div class="section"></div>
<main>
    <center>
        <img class="responsive-img" style="width: 150px;" src="{{URL::asset('images/logo.png')}}" />
        <div class="container">
            <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 52px 68px 0px 68px; border: 1px solid #EEE;">

                <form class="col s12" action="{{ route('admin.login-post') }}" method="POST">
                    {{csrf_field()}}
                    <span class="error">{{ session('error') }}</span>


                    <div class='row'>
                        <div class='col s12'>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s12'>
                            <input type="text" class="validate" id="name" name="username" v-model="user.username">
                            <label for='name'>Username</label>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='input-field col s12'>
                            <input type="password" class="validate" id='password'  name="password" v-model="user.password">
                            <label for='password'>Password</label>
                        </div>
                    </div>

                    <br />
                    <center>
                        <div class='row'>
                            <button type='submit' style="background-color: #C01228!important;" name='btn_login' class='col s12 btn btn-large waves-effect indigo'>Login</button>
                        </div>
                    </center>
                </form>
            </div>
        </div>


    </center>

    <div class="section"></div>
    <div class="section"></div>
</main>

<script type="text/javascript" src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/materialize.min.js')}}"></script>
</body>

</html>
