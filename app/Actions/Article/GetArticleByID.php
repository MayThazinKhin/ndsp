<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:48 AM
 */

namespace App\Actions\Article;


class GetArticleByID extends BaseArticleAction
{
    protected function onValidationSuccess()
    {
        $artilce = $this->repository->getBy($this->data()['id']);
        return $artilce;
    }
}