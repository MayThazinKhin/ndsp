<?php


namespace App\Actions\Comment;


use App\Actions\Action;

class DeleteComment extends BaseCommentAction
{

    protected function onValidationSuccess()
    {
        $comment = $this->repository->delete($this->data()->toArray());
        if ($comment) {
            return true;
        }
        return false;
    }
}