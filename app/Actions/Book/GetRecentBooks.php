<?php
/**
 * Created by PhpStorm.
 * User: yanyan
 * Date: 11/11/2017
 * Time: 12:59 AM
 */

namespace App\Actions\Book;


class GetRecentBooks extends BaseBookAction
{

   public function onValidationSuccess()
   {
       $books = $this->repository->getLastBooks($this->data()['count']);
       return $books;
   }
}