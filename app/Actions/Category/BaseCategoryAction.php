<?php

namespace  App\Actions\Category;

use App\Actions\Action;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Collection;

abstract class BaseCategoryAction extends  Action
{

    protected $rules = [];

    public function __construct(CategoryRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }

    protected function onValidationSuccess(){}


}