<?php

namespace Admin\Http\Controllers;

use App\Actions\Order\CreateOrder;
use App\Actions\Order\DeleteOrder;
use App\Actions\Order\DeleteOrderOfBook;
use App\Actions\Order\GetDeliveryReport;
use App\Actions\Order\GetOrderByEmail;
use App\Actions\Order\GetOrderByPhone;
use App\Actions\Order\GetOrderDetails;
use App\Actions\Order\GetOrders;
use App\Actions\Order\OrderStatusUpdate;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\OrderRepository;
use http\Env\Response;
use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    use ControllerTrait;


    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('order.index');
    }

    public function createIndex()
    {
        return view('order.create');
    }

    public function detailViewIndex()
    {
        return view('order.detail');
    }

    public function detail(Request $request)
    {
        $rq = [
            'email'                 => $request->email,
            'phone'                 => $request->phone,
            'address'               => $request->address,
            'otw_or_deli_or_return' => $request->status
        ];

        $action = new GetOrderDetails($this->repository, collect($rq));
        $result = $action->invoke();

        if ($result instanceof Validator) {
            return response()->json([$result->errors()], 422);
        }
        return response()->json($result);
    }

    public function create(Request $request)
    {
        $action = new CreateOrder($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json(['success' => 'false']);
        }
        if (!$result) {

            return response()->json(['success' => 'false'], 500);
        }
        return response()->json(['success' => 'true']);
    }

    public function update(Request $request)
    {

    }

    public function updateStatus(Request $request)
    {
        $rq = ['id' => $request->id, 'otw_or_deli_or_return' => $request->otw_or_deli_or_return];

        $action = new OrderStatusUpdate($this->repository, collect($rq));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()], 422);
        }

        if (!$result) {
            return response()->json('failed', 500);
        }
        return response()->json($result);
    }

    public function delete(Request $request)
    {
        $rq = [
            'email'                 => $request->email,
            'phone'                 => $request->phone,
            'address'               => $request->address,
            'otw_or_deli_or_return' => $request->status
        ];
        $action = new DeleteOrder($this->repository, collect($rq));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()], 422);
        }

        if (!$result) {
            return response()->json('failed', 500);
        }
        return response()->json($result);
    }

    public function deleteOrderBook($id)
    {
        $rq = ['id' => $id];
        $action = new DeleteOrderOfBook($this->repository, collect($rq));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()], 422);
        }

        if (!$result) {
            return response()->json('failed', 500);
        }
        return response()->json($result);
    }

    public function getAll($status)
    {
        $req = ['status' => $status];
        $action = new GetOrders($this->repository, collect($req));
        $orders = $action->invoke();
        return response()->json($orders);
    }

    public function filterOrderByPhone($status, $q)
    {
        $req = ['status' => $status, 'phone' => $q];
        $action = new GetOrderByPhone($this->repository, collect($req));
        $orders = $action->invoke();
        return response()->json($orders);
    }

    public function filterOrderByEmail($status, $q)
    {
        $req = ['status' => $status, 'email' => $q];
        $action = new GetOrderByEmail($this->repository, collect($req));
        $orders = $action->invoke();
        return response()->json($orders);
    }

    public function getByID($id)
    {

    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDeliveryReport(Request $request)
    {
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $end_date = date('Y-m-d', strtotime($request->end_date));

        $data = collect([
            'start_date' => $start_date,
            'end_date'   => $end_date
        ]);

        $action = new GetDeliveryReport($this->repository, $data);
        $orders = $action->invoke();
        $sum = $orders->sum('deli_charge');

        return response()->json([
            'orders'            => $orders,
            'total_deli_charge' => $sum,
            'data' => $data
        ], 200);
    }

    public function showDeliveryReport(){
        return  view('order.delivery_report');

    }

}