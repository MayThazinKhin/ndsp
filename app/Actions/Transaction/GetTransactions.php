<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 31/01/2018
 * Time: 10:26 AM
 */

namespace App\Actions\Transaction;


class GetTransactions extends  BaseTransactionAction
{
    public $_page = 20;

    protected function onValidationSuccess()
    {
        $publishers = $this->repository->paginate($this->_page);
        return $publishers;
    }
}