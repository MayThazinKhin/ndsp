<?php

namespace  App\Actions\Edition;

use App\Actions\Action;
use App\Repositories\EditionRepository;
use Illuminate\Support\Collection;

class BaseEditionAction extends  Action
{


    protected $rules = [];

    public function __construct(EditionRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected function onValidationSuccess(){}
}