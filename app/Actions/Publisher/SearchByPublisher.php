<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 08/12/2017
 * Time: 11:18 AM
 */

namespace App\Actions\Publisher;


class SearchByPublisher extends BasePublisherAction
{
    protected function onValidationSuccess()
    {
        $authors = $this->repository->findByName($this->data()['publisherName']);
        return $authors;
    }
}