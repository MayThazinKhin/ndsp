import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';

export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {
            isEdit: false,
            route: {
                get: '/admin/edition/all?page=',
                create: '/admin/edition/create',
                edit: '/admin/edition/edit',
                delete: '/admin/edition/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            editions: [],
            edition: {id: null, editionName: null}
        }
    },

    methods: {
        clearData() {
            this.edition.id = null;
            this.edition.editionName = null;
            this.$validator.clean();
        },
        loadData(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.editions = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        getEditions() {
            this.loadData(this.route.get);
        },
        onPaginationClick() {
            this.getEditions();
        },

        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(data) {
            var temp = Object.assign({}, data);
            this.edition = temp;
            this.isEdit = true;
        },

        toggleDelete(id) {
            this.edition.id = id;
        },

        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.edition).then(response => {
                this.getEditions();
                $('#modal1').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.edition.id + '/delete').then(response => {
                this.getEditions();
                this.edition.id = null;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getEditions();
        }
    },

    mounted() {
        this.getEditions();
    }
}