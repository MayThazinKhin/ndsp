<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:47 AM
 */

namespace App\Actions\Article;


class DeleteArticle extends BaseArticleAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $category = $this->repository->delete($this->data()->toArray());
        if ($category) {
            return true;
        }
        return false;
    }
}