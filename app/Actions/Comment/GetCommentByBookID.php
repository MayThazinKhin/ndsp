<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 17/01/2018
 * Time: 11:23 AM
 */

namespace App\Actions\Comment;


class GetCommentByBookID extends  BaseCommentAction
{
    public  $_page=20;
    protected function onValidationSuccess()
    {
        $comments=$this->repository->getWithPaginate($this->_page,$this->data()['book_id']);
        return $comments;
    }
}