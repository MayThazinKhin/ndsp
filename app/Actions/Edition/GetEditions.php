<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/19/2017
 * Time: 3:34 PM
 */

namespace App\Actions\Edition;


class GetEditions extends BaseEditionAction
{
    protected function onValidationSuccess()
    {
        $editions = $this->repository->paginate();
        return $editions;
    }
}