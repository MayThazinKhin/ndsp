<?php

namespace App\Http\Controllers\Api;

use App\Actions\Author\GetAuthors;

use App\Actions\Author\SearchAuthorByName;
use App\Repositories\AuthorRepository;

use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    private $pages=2;
    private  $repository;
    public function __construct(AuthorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $action = new GetAuthors($this->repository);
        $action->_page=$this->pages;
        $authors = $action->invoke();
        return response()->json($authors);
    }

    public function searchAuthor($name){
        $_req = ['authorName'=>$name];
        $action = new SearchAuthorByName($this->repository,collect($_req));
        $authors = $action->invoke();
        return response()->json($authors);
    }
}
