$('.main-carousel').flickity({
  // options
  cellAlign: 'left',
  contain: true,
  autoPlay : true,
  groupCells : true
});

function toggleSidebar(){
	document.getElementById("sidebar").classList.toggle("active");
}

$(document).ready(function(){
	$("#nav-home").focus();
})