<?php

namespace  App\Actions\Sale;

use App\Actions\Action;
use App\Repositories\SaleRepository;
use Illuminate\Support\Collection;

class BaseSaleAction extends Action
{

    protected $rules=[];

    public function __construct(SaleRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }


    protected function onValidationSuccess()
    {

    }
}