import Vue from 'vue';
import axios from 'axios';
import AuthorSelect from './core/AuthorSelect';
import CategorySelect from './core/CategorySelect';
import GenreSelect from './core/CategorySelect';
import PublisherSelect from './core/PublisherSelect';
import EditionSelect from './core/EditionSelect';
import VeeValidate from 'vee-validate';
import Helper from './core/Helper';

Vue.use(VeeValidate);

new Vue({
    components: {
        AuthorSelect, CategorySelect, GenreSelect, PublisherSelect, EditionSelect
    },

    el: '#app',

    data: {
        isEdit: false,
        selectedAuthors:[],
        selectedGenres:[],
        selectedCategories:[],
        selectedPublisher:[],
        selectedEdition:[],

        book: {
            id: null,
            bookTitle: '',
            bookCoverImgUrl: '',
            coverInfo: null,
            bookSalePrice: 0,
            bookDescription: '',
            authors: [],
            categories: [],
            genres: [],
            edition_id: null,
            publisher_id: null,
            total_cost_of_books: 0,
            qty: 0,
            isEbook: false,
            e_book_download: null,
            isConsigment: false
        }
    },
    methods: {
        authorChange(value) {
            this.book.authors = value;
        },
        categoryChange(value) {
            this.book.categories = value;
        },
        genreChange(value) {
            this.book.genres = value;
        },
        publisherChange(value) {
            this.book.publisher_id = value;
        },
        editionChange(value) {
            this.book.edition_id = value;
        },

        getBookDetail(id) {
            axios.get('/admin/book/' + id + '/' + 'detail').then(response => {
                this.book = response.data.book;
                this.selectedAuthors=response.data.book.authors;
                this.selectedPublisher = response.data.book.publisher;
                this.selectedEdition=response.data.book.edition;
                this.selectedCategories = response.data.book.categories;
                this.selectedGenres =response.data.book.genres;
                this.book.bookCoverImgUrl = response.data.coverImage;
                this.book.isConsigment = response.data.book.stocks.length>0?response.data.book.stocks[0]['buy_or_consigment']:false;
            });
        },

        onFileChange(e) {
            this.book.coverInfo = e.target.value;
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;


            this.createImage(files[0]);
        },
        createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;
            reader.onload = (e) => {
                vm.book.bookCoverImgUrl = e.target.result;
            };
            reader.readAsDataURL(file);
        },

        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    this.performProcess(this.isEdit ? 'edit' : 'create');
                }
            }).catch(() => {
            });
        },
        performProcess(performtype) {
            axios.post('/admin/book/' + performtype, this.book).then(response => {
                return response.data;
            }).catch(error => {
                this.svr_errors.record(error.response.data);
            });
        },
        checkUrlParam() {
            let book_id = Helper.getUrlParameter('book_id');
            if (book_id != null) {
                this.isEdit = true;
                this.getBookDetail(book_id);
            }
        },
    },
    mounted() {
        this.checkUrlParam();
    }
});