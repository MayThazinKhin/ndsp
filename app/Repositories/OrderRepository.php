<?php
/**
 * Created by PhpStorm.
 * User: May
 * Date: 11/17/2017
 * Time: 4:59 PM
 */

namespace App\Repositories;

use App\Models\Order;
use Illuminate\Support\Facades\DB;

class OrderRepository extends Repository
{
    function model()
    {
        return Order::class;
    }

    public function getAllOrders($status, $pages)
    {
        return Order::where('otw_or_deli_or_return',
            $status)->select(DB::raw('count(*) as order_count,email,address,phone,created_at'))->groupBy([
            'email',
            'address',
            'phone',
            'created_at'
        ])->orderByDesc('created_at')->paginate($pages);
    }

    public function getOrderDetails($email, $phone, $address, $status)
    {
        return Order::with('book')->where('otw_or_deli_or_return', $status)->where('email', $email)->where('phone',
            $phone)->where('address', $address)->get();
    }

    public function destory($email, $phone, $address, $status)
    {
        $order = Order::with('book')->where('otw_or_deli_or_return', $status)->where('email', $email)->where('phone',
            $phone)->where('address', $address);

        return $order->delete();
    }

    public function getOrderByPhone($status, $phone, $pages)
    {
        return Order::where('otw_or_deli_or_return', $status)->where('phone', 'LIKE',
            $phone . '%')->select(DB::raw('count(*) as order_count,email,address,phone,created_at'))->groupBy([
            'email',
            'address',
            'phone',
            'created_at'
        ])->orderByDesc('created_at')->paginate($pages);
    }

    public function getOrderByEmail($status, $email, $pages)
    {
        return Order::where('otw_or_deli_or_return', $status)->where('email', 'LIKE',
            $email . '%')->select(DB::raw('count(*) as order_count,email,address,phone,created_at'))->groupBy([
            'email',
            'address',
            'phone',
            'created_at'
        ])->orderByDesc('created_at')->paginate($pages);
    }

    public function getDeliveryReport($start_date, $end_date)
    {
        return Order::where([
            ['date', '>=', $start_date],
            ['date', '<=', $end_date],
            ['otw_or_deli_or_return', '=', 'deliver']
        ])->orderByDesc('created_at')->get();
    }
}