import Vue from 'vue';
import axios from 'axios';
import Toaster from 'v-toaster';
import VeeValidate from 'vee-validate';
import vSelect from 'vue-select';

import ManageBooks from './books/manage-book.js';
import ManageEbooks from './ebooks/manage-ebook.js';

import BookList from './books/book-list.js';
import EBookList from './ebooks/ebook-list.js';

import GenreList from './genres/genre-list.js';
import AuthorList from './authors/author-list.js';
import CategoryList from './categories/category-list.js';
import EditionList from './editions/edition-list.js';
import PublisherList from './publishers/publisher-list.js';
import AdminList from './users/user-list.js';
import PurchaseList from './purchase/purchase-list.js';
import OrderList from './orderes/index.js';
import OrderDetail from './orderes/details.js';
import CreateOrder from './orderes/create.js';
import DeliveryReport from './orderes/delivery_report';

import CommentList from './comments/index.js';

window.Vue = Vue;
window.axios = axios;

Vue.use(Toaster, {timeout: 5000});
Vue.use(Toaster);
Vue.use(VeeValidate);

Vue.component('v-select', vSelect);
Vue.component('manageBooks', ManageBooks);
Vue.component('manageEbooks', ManageEbooks);
Vue.component('bookList', BookList);
Vue.component('ebookList', EBookList);
Vue.component('authorList', AuthorList);
Vue.component('genreList', GenreList);
Vue.component('categoryList', CategoryList);
Vue.component('editionList', EditionList);
Vue.component('publisherList', PublisherList);
Vue.component('adminList', AdminList);
Vue.component('purchaseList', PurchaseList);
Vue.component('orderList', OrderList);
Vue.component('orderDetail', OrderDetail);
Vue.component('orderCreate', CreateOrder);
Vue.component('commentList', CommentList);
Vue.component('deliveryReport', DeliveryReport);
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
