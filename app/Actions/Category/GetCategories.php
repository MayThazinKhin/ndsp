<?php


namespace App\Actions\Category;


class GetCategories extends BaseCategoryAction
{
    public $_page=20;
    protected function onValidationSuccess()
    {
        $categories = $this->repository->paginate($this->_page);
        return $categories;
    }
}