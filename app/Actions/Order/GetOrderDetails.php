<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 14/02/2018
 * Time: 12:59 PM
 */

namespace App\Actions\Order;


class GetOrderDetails extends BaseOrderAction
{
    protected $rules=['email'=>'required','phone'=>'required','address'=>'required','otw_or_deli_or_return'=>'required'];
    protected function onValidationSuccess()
    {
        $orders = $this->repository->getOrderDetails($this->data()['email'],$this->data()['phone'],$this->data()['address'],$this->data()['otw_or_deli_or_return']);
        return $orders;
    }
}