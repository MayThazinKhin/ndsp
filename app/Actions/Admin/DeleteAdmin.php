<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 9:19 AM
 */

namespace App\Actions\Admin;


class DeleteAdmin extends  BaseAdminAction
{
    protected $rules = ['username' => 'required'];

    protected function onValidationSuccess()
    {
        $admin = $this->repository->delete($this->data()->toArray(), self::PK_USERNAME);
        if ($admin) {
            return true;
        }
        return false;
    }
}