<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 17/11/2017
 * Time: 11:34 AM
 */

namespace App\Actions\Book;


class GetTopTenBooks extends  BaseBookAction
{
    protected function onValidationSuccess()
    {
        $books = $this->repository->getTopTenBooks();
        return $books;
    }
}