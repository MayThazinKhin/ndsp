<?php

namespace App\Actions\Category;

class UpdateCategory extends BaseCategoryAction
{
    protected $rules = [
        'id' => 'required',
        'categoryName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $category =$this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($category) {
            return true;
        }
        return false;
    }
}