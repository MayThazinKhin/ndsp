<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:29 AM
 */

namespace Admin\Http\Controllers;


use App\Actions\Article\CreateArticle;

use App\Actions\Article\DeleteArticle;
use App\Actions\Article\EditArticle;
use App\Actions\Article\GetArticleByID;
use App\Actions\Article\GetArticles;
use App\Http\Controllers\Controller;
use App\Repositories\ArticleRepository;


use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class ArticleController extends  Controller
{

    private $repository;

    public function __construct(ArticleRepository $repository)
    {
        $this->repository=$repository;

    }


    public function index(){
        $action = new GetArticles($this->repository);
        $articles = $action->invoke();

        return view('articles.index',compact('articles'));
    }

    public function createIndex(){

        return view('articles.add');
    }


    public function editIndex($id){
        $action = new GetArticleByID($this->repository, collect(['id'=>$id]));
        $article = $action->invoke();


        return view('articles.edit',compact('article'));
    }

    public function create(Request $request)
    {
        $action = new CreateArticle($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return back()->withInput($request->all());
        }
        return redirect('/admin/article');
    }

    public function update(Request $request)
    {

        $action = new EditArticle($this->repository, collect($request->all()),$request);

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return back()->withErrors($request->all());
        }

        return redirect('/admin/article');

    }

    public function delete($id)
    {

        $request = ['id' => $id];
        $action = new DeleteArticle($this->repository, collect($request));
        $result = $action->invoke();

      return redirect('/admin/article');
    }

    public function getAll()
    {


    }

    public function getByID($id)
    {

    }

    public function getByName($name)
    {

    }
}