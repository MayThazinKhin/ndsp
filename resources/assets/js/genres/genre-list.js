import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';
export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {
            isEdit: false,
            route: {
                get: '/admin/genre/all?page=',
                create: '/admin/genre/create',
                edit: '/admin/genre/edit',
                delete: '/admin/genre/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            genres: [],
            genre: {id: null, genreName: null}
        }
    },

    methods: {
        clearData() {
            this.genre.id = null;
            this.genre.genreName = null;
            this.$validator.clean();
        },
        loadData(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.genres = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        getGenres() {
            this.loadData(this.route.get);
        },
        onPaginationClick() {
            this.getGenres();
        },

        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(data) {
            var temp = Object.assign({}, data);
            this.genre = temp;
            this.isEdit = true;
        },

        toggleDelete(id) {
            this.genre.id = id;
        },

        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.genre).then(response => {
                this.getGenres();
                $('#modal1').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.genre.id + '/delete').then(response => {
                this.getGenres();
                this.genre.id = null;
              this.$parent.showToast('Success', 'success');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getGenres();
        }
    },

    mounted() {
        this.getGenres();
    }
}