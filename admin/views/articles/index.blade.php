@extends('layout.app')
@section('title','User')
@section('article','selected')

@section('content')

        <h3>Article List</h3>
        <div class="content-down">

            <div class="card-content">
                <div class="content-up">
                    <div class="content-up-left">
                        <a href="{{route('admin.article.create')}}" class="modal-trigger waves-effect waves-light btn"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Article</a>
                    </div>
                </div>


                <table class="highlight striped data-table">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>

                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    @php($cur=$articles->currentPage())

                        @foreach($articles as $article)

                            <tr>
                                <td>{{$cur++}}</td>
                                <td>{{ $article->title }}</td>

                                <td>
                                    <a href="{{ route('admin.article.showedit', ['id' => $article->id]) }}"

                                       class="modal-trigger btn-floating"><i class="material-icons fa fa-pencil-square-o "></i></a>
                                    <a href="{{ route('admin.article.delete', ['id' => $article->id]) }}" class="btn-floating waves-effect waves-light modal-trigger"><i class="material-icons fa fa-trash-o"></i></a>
                                </td>
                            </tr>

                        @endforeach


                    </tbody>
                </table>
            </div><!-- card-content end -->
            <div class="paginations">

                {{ $articles->links() }}

            </div>
        </div>

@endsection
