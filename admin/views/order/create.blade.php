@extends('layout.app')
@section('title','Order')
@section('order','active')
@section('style')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
@endsection
@section('content')
    <order-create inline-template>
        <div>
            <h3>Create Order</h3>
            <div class="content-down">

                <div class="card-content">
                    <h4>User Information</h4>
                    <form class="col s12" @submit.prevent="submitdata" autocomplete="off">
                        <div class="row">

                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="material-icons prefix">email</i>
                                    <input type="email" class="input" placeholder="Email" v-model="orders.email"
                                           name="email" v-validate="'required|email'">
                                    <div id="cemail-error" class="error"><span v-if="errors.has('email')">@{{ errors.first('email')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="material-icons prefix">phone</i>
                                    <input type="tel" class="input" placeholder="Phone" v-model="orders.phone"
                                           name="phone" v-validate="'required'">
                                    <div id="cemail-error" class="error"><span v-if="errors.has('phone')">Required phone.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s4">
                                    <i class="material-icons prefix">location_city</i>
                                    <input type="text" class="input" placeholder="Address" v-model="orders.address"
                                           name="address" v-validate="'required'">
                                    <div id="cemail-error" class="error"><span v-if="errors.has('address')">Required address.</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <select
                                    id="select-city"
                                    v-model="orders.city"
                                    name="city" v-validate="'required'">
                                <option value=""> -- Select a city --</option>
                                <option :value="1">Mandalay</option>
                                <option :value="2">Yangon</option>
                                <option :value="3">Other</option>
                            </select>
                        </div>


                        <div id="cemail-error" class="error"><span
                                    v-if="errors.has('city')">Required City.</span>
                        </div>
                        <h4>Book Information</h4>
                        <div class="row">

                            <div class="row">
                                <div class="input-field col s3 offset-s1">
                                    <i class="material-icons prefix">book</i>
                                    <multiselect class="customstyle"
                                                 placeholder="Book"
                                                 v-model="selected_book"
                                                 label="bookTitle"
                                                 name="book"
                                                 track-by="bookTitle"
                                                 :options="books"
                                                 :multiple="false"
                                                 :searchable="true"
                                                 :internal-search="false"
                                                 :taggable="true"
                                                 @search-change="asyncFind"
                                                 data-vv-name="book" v-validate="'required'">
                                    </multiselect>
                                    <div id="cemail-error" class="error"><span v-if="errors.has('book')">Required book name.</span>
                                    </div>
                                </div>
                                <div class="input-field col s3 offset-s1">
                                    <i class="material-icons prefix">add_circle</i>
                                    <input id="icon_telephone" type="number" class="input" placeholder="Quantity"
                                           v-model="order.qty" name="qty" v-validate="'required'">

                                </div>
                                <div class="input-field col s3 offset-s1">
                                    <button class="wave-effct wav-light btn add_btn">Add</button>

                                </div>
                            </div>

                        </div>
                    </form>
                    <!-- Data Lists -->
                    <table class="striped bordered" style="border:1px solid #F5F5F5;">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="ord,index in orders.books">
                            <td>@{{ index+1 }}</td>
                            <td>@{{ ord.bookTitle }}</td>
                            <td>@{{ ord.author }}</td>
                            <td>@{{ ord.price }}</td>
                            <td>@{{ ord.qty }}</td>
                            <td>@{{ ord.total }}</td>
                            <td>
                                <button class="btn-floating waves-effect waves-light" @click="removeRow(index)">
                                    <i class="material-icons prefix">delete</i>
                                </button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                    <h5> Delivery Charge : @{{ orders.delivery_charge }}</h5>
                    <div class="row">
                        <button @click="performProcess" class="wave-effct wav-light btn order_save_btn pull-right"
                                style="margin-top:20px;">Save
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </order-create>
@endsection
@section('script')

@endsection