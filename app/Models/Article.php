<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:36 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['category', 'title', 'content', 'cover_photo'];

    public function getPost($content, $length)
    {
        if (strlen($content) > 0) {
            return substr(strip_tags($content), 0, $length) . '...';
        }
        return '';
    }
}