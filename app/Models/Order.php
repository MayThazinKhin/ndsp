<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = ['book_id', 'qty', 'email', 'phone', 'address', 'otw_or_deli_or_return','deli_charge','town_id','date'];

    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function sale(){
        return $this->hasOne(Sale::class);
    }
}
