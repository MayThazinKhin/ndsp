<?php


namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByCategory extends BaseBookAction
{
    public $pages = 20;
    public $filtername='categories';
    protected $rules = ['category_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['category_id'],$this->data()['buy_or_consigment'], $this->pages,$this->filtername);
    }
}