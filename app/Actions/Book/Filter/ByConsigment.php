<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 02/01/2018
 * Time: 4:00 PM
 */

namespace App\Actions\Book\Filter;




use App\Actions\Book\BaseBookAction;

class ByConsigment extends BaseBookAction
{
    public $pages = 20;
    public $filtername='categories';
    protected $rules = ['buy_or_consigment' => 'required'];

    protected function onValidationSuccess()
    {

        return $this->repository->filterByConsigment($this->data()['buy_or_consigment'], $this->pages);
    }
}