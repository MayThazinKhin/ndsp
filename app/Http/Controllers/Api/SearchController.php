<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 10:21 AM
 */

namespace App\Http\Controllers\Api;


use App\Actions\Book\Filter\ByAuthorName;
use App\Actions\Book\Filter\ByCategoryName;
use App\Actions\Book\Filter\ByGenreName;
use App\Actions\Book\Filter\ByName;
use App\Actions\Book\Filter\ByPublisherName;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use App\Repositories\CommentRepository;

class SearchController  extends Controller
{
    private $repository, $commentRepository;

    public function __construct(BookRepository $repository, CommentRepository $commentRepository)
    {
        $this->repository = $repository;
        $this->commentRepository = $commentRepository;
    }

    public function getBookByName($name)
    {
        $request = ['bookTitle' => $name];
        $action = new ByName($this->repository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function getByAuthorName($name)
    {
        $request = ['authorName' => $name];
        $action = new ByAuthorName($this->repository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function getByGenreName($name)
    {
        $request = ['genreName' => $name];
        $action = new ByGenreName($this->repository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function getByCategoryName($name)
    {
        $request = ['categoryName' => $name];
        $action = new ByCategoryName($this->repository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json($result);
    }

    public function getByPublisherName($name)
    {
        $request = ['publisherName' => $name];
        $action = new ByPublisherName($this->repository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json($result);
    }
}