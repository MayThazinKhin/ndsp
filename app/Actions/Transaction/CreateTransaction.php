<?php

namespace App\Actions\Transaction;


class CreateTransaction extends BaseTransactionAction
{
    protected $rules = [
        'book_id' => 'required',
        'total_cost_of_books'=>'required',
        'qty'=>'required',
        'created_at'=>'required'
    ];

    protected function onValidationSuccess()
    {

        $transaction = $this->repository->create($this->data()->toArray());
        if ($transaction) {
            return true;
        }
        return false;
    }
}