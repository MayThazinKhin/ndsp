<?php

namespace App\Actions\Transaction;

use App\Actions\Action;
use App\Repositories\TransactionRepository;
use Illuminate\Support\Collection;

class BaseTransactionAction extends  Action
{
    protected $rules=[];

    public function __construct(TransactionRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected function onValidationSuccess()
    {

    }
}