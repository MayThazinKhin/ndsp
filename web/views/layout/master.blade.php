<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>We Distribution | @yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3'/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.9/flickity.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/responsive.css')}}">
    @yield('style')
</head>
<body>
<div id="app">
@include('layout.nav')

<!-- mobile screen show sidebar menu -->


    @yield('content')


    @include('layout.footer')
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.9/flickity.pkgd.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

{{--<script type="text/javascript" src="{{ URL::asset('js/manifest.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ URL::asset('js/vendor.js') }}"></script>--}}
<script type="text/javascript" src="{{ URL::asset('js/web/manifest.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/web/vendor.js') }}"></script>
<script type="text/javascript" src="{{URL::asset('js/web/app.js')}}"></script>


@yield('script')
</body>
</html>