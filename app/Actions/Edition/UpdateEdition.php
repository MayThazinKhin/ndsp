<?php


namespace App\Actions\Edition;


class UpdateEdition extends  BaseEditionAction
{
    protected $rules = [
        'id' => 'required',
        'editionName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $edition = $this->repository->update($this->data()->toArray(), $this->data()['id']);
        if ($edition) {
            return true;
        }
        return false;
    }
}