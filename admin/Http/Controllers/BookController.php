<?php

namespace Admin\Http\Controllers;

use App\Actions\Book\AsyncFindBook;
use App\Actions\Book\CreateBook;
use App\Actions\Book\DeleteBook;
use App\Actions\Book\Filter\ByAuthor;
use App\Actions\Book\Filter\ByCategory;
use App\Actions\Book\Filter\ByConsigment;
use App\Actions\Book\Filter\ByGenre;
use App\Actions\Book\Filter\ByPublisher;
use App\Actions\Book\GetAllBooks;
use App\Actions\Book\GetBookByID;

use App\Actions\Book\UpdateBook;
use App\FileSystem\Images\BookImage;
use App\Http\Controllers\Controller;

use App\Http\Controllers\ControllerTrait;

use App\Repositories\BookRepository;

use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;

use Illuminate\Validation\Validator;

class BookController extends Controller
{
    use ControllerTrait;

    private $transRepo;

    public function __construct(BookRepository $repository, TransactionRepository $transRepo)
    {
        $this->repository = $repository;
        $this->transRepo = $transRepo;

    }

    function index()
    {
        return view('book.index');
    }

    function showActionPage()
    {
        return view('book.action');
    }

    function create(Request $request)
    {
        $action = new CreateBook($this->repository, $this->transRepo, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }


    function update(Request $request)
    {
        $action = new UpdateBook($this->repository,  collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function delete($id)
    {
        $request = [$this->pk_ID => $id];

        $action = new DeleteBook($this->repository, collect($request));
        $action->invoke();
        return back();
    }

    function getAll()
    {
        $action = new GetAllBooks($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new GetBookByID($this->repository, collect($request));
        $book = $action->invoke();
        $coverImage = '';
        if ($book) {
            $img = new BookImage($book->bookCoverImgUrl);
            if($img->exists()){
                $coverImage = $img->convertImageToBase64String();
            }

        }
        return response()->json(['book' => $book, 'coverImage' => $coverImage]);
    }

    function viewByConsigment($consigment)
    {

        $request = ['buy_or_consigment' => $consigment];

        $action = new ByConsigment($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByAuthor($author_id, $consigment)
    {
        $request = ['author_id' => $author_id, 'buy_or_consigment' => $consigment];
        $action = new ByAuthor($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByCategory($category_id, $consigment)
    {
        $request = ['category_id' => $category_id, 'buy_or_consigment' => $consigment];
        $action = new ByCategory($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByGenre($genre_id, $consigment)
    {
        $request = ['genre_id' => $genre_id, 'buy_or_consigment' => $consigment];
        $action = new ByGenre($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByPublisher($publisher_id,$consigment)
    {
        $request = ['publisher_id' => $publisher_id, 'buy_or_consigment' => $consigment];
        $action = new ByPublisher($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function asyncbook($q){
        $request = ['bookTitle' => $q];
        $action = new AsyncFindBook($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }
}