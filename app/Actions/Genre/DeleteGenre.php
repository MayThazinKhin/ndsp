<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:06 AM
 */

namespace App\Actions\Genre;


class DeleteGenre extends BaseGenreAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $category = $this->repository->delete($this->data()->toArray());
        if ($category) {
            return true;
        }
        return false;
    }
}