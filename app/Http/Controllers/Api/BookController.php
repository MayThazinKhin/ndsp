<?php

namespace App\Http\Controllers\Api;

use App\Actions\Book\Filter\FindBookByName;
use App\Actions\Book\Filter\GetBookByAuthorId;
use App\Actions\Book\Filter\GetBookByCategoryId;
use App\Actions\Book\Filter\GetBookByGenreId;
use App\Actions\Book\Filter\GetBookByPUblisherID;


use App\Actions\Book\GetBookDetail;
use App\Actions\Book\GetBooks;

use App\Actions\Book\GetRecentBooks;


use App\Actions\Book\GetTopAllBooks;
use App\Actions\Book\GetTopTenBooks;
use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;
use App\Models\Book;
use App\Repositories\BookRepository;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;

class BookController extends Controller
{
    private $repository, $commentRepository;
    private $pages=2;
    public function __construct(BookRepository $repository, CommentRepository $commentRepository)
    {
        $this->repository = $repository;
        $this->commentRepository = $commentRepository;
    }

    public function all()
    {
        $action = new GetBooks($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getRecentBooks()
    {
        $page = ['count' => 10];
        $action = new GetRecentBooks($this->repository, collect($page));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getTopTenBooks()
    {

        $action = new GetTopTenBooks($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getTopAllBooks()
    {

        $action = new GetTopAllBooks($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getBoooksByCategoryId($category_id)
    {

        $request = ['category_id' => $category_id];
        $action = new GetBookByCategoryId($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getBooksByGenereId($genre_id)
    {

        $request = ['genre_id' => $genre_id];
        $action = new GetBookByGenreId($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getBoooksByAuthorId($author_id)
    {
        $request = ['author_id' => $author_id];
        $action = new GetBookByAuthorId($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getBoooksByPublisherId($publisher_id)
    {
        $request = ['publisher_id' => $publisher_id];
        $action = new GetBookByPUblisherID($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }



    public function getBookDetail($book_id)
    {
        $request = ['id' => $book_id];
        $action = new GetBookDetail($this->repository, $this->commentRepository, collect($request));
        $result = $action->invoke();

        if ($result) {
            return response()->json($result);
        }
        return response()->json(['isSuccess' => 'false']);
    }

    public function getImage($name)
    {
        $book = new BookImage($name);
        if($book->exists()){
            return $book->getFileResponse();
        }
        return null;
    }

    public function searchBook($name){
        $_req = ['bookTitle'=>$name];
        $action = new FindBookByName($this->repository,collect($_req));
        $books = $action->invoke();
        return response()->json($books);
    }

    public function getBookByMusicCategory(){
        $books= Book::whereHas('categories',function($q){
            $q->where('categoryName','Music');
        })->paginate($this->pages);
        return response()->json($books);
    }
}