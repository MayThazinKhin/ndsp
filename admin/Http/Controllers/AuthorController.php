<?php

namespace Admin\Http\Controllers;

use App\Actions\Author\DeleteAuthor;
use App\Actions\Author\SearchAuthorByName;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\AuthorRepository;
use App\Actions\Author\CreateAuthor;
use App\Actions\Author\UpdateAuthor;
use App\Actions\Author\GetAuthors;
use App\Actions\Author\GetAuthorByID;

use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    use ControllerTrait;

    public function __construct(AuthorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('author.index');
    }

    public function create(Request $request)
    {
        $action = new CreateAuthor($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json($result->errors(),422);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {

        $action = new UpdateAuthor($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new DeleteAuthor($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        if(!$result){
            return response()->json(['status'=>'failed'],500);
        }
        return response()->json($result);
    }

    public function getAll()
    {
        $action = new GetAuthors($this->repository);
        $authors = $action->invoke();
        return response()->json($authors);
    }

    public function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new GetAuthorByID($this->repository, collect($request));
        $author = $action->invoke();
        return response()->json($author);
    }

    public function getByName($name)
    {
        $request = ['authorName' => $name];
        $action = new SearchAuthorByName($this->repository, collect($request));
        $author = $action->invoke();
        return response()->json($author);
    }
}