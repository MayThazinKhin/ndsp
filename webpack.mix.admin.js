let mix = require('laravel-mix');
mix
    .js('resources/assets/js/app.js', 'public/js')
    .extract(['vue', 'axios', 'vee-validate', 'moment', 'vue-star-rating', 'vue-multiselect', 'v-toaster','bootstrap-vue']);