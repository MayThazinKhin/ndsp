<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/11/2017
 * Time: 2:34 PM
 */

namespace App\Actions\Order;


use Carbon\Carbon;

class CreateOrder extends BaseOrderAction
{
    protected $rules = ['email' => 'required', 'phone' => 'required', 'address' => 'required'];

    public $unSuccessfulData = [];

    protected function onValidationSuccess()
    {
        $order = $this->data();
        $data = new \stdClass();
        $data->email = $order['email'];
        $data->phone = $order['phone'];
        $data->address = $order['address'];
        $data->date = Carbon::now()->format('Y-m-d');
        foreach ($order['books'] as $i=>$book) {
            $data->book_id = $book['book_id'];
            $data->qty = $book['qty'];
            $data->otw_or_deli_or_return='pending';
            $data->town_id = $order['town_id'];
            $data->deli_charge = 1000;
            if($i==0 && ($order['town_id'] == 1 || $order['town_id'] == 2 )){
                $data->deli_charge = 1000;
            }
            elseif($i == 0 && $order['town_id'] == 3 ){
                    $data->deli_charge = 1000 + (300 * ($book['qty'] - 1));
            }
            if($i <> 0){
                if( $order['town_id'] == 1 || $order['town_id'] == 2 ){
                    $data->deli_charge = 0;
                }
                else{
                    $data->deli_charge = 300 * $book['qty'];
                }
            }
            $insertData = $this->repository->create((array)$data);

            if (!$insertData) {
                array_push($this->unSuccessfulData, $insertData);
            }
        }
        if (count($this->unSuccessfulData) > 0) {
            return false;
        }
        return true;
    }
}