@extends('layout.app')
@section('title','Ebooks')
@section('ebooks', 'selected')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vue-multiselect.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin/new-book.css')}}">

@endsection
@section('content')
    <h3>Add New EBook</h3>

    <manage-ebooks inline-template>
        <div class="content-down">
            <div class="card-content">
                <div class="content-left">
                    <img :src="book.bookCoverImgUrl" >
                </div>

                <div class="content-right">
                    <form @submit.prevent="submitdata" autocomplete="off">
                        <!-- Cover Image  -->
                        <div class="form-line">
                            <div class="left">
                                <p>Cover Image <span class="red-star">*</span></p>
                            </div>

                            <div class="right">
                                <div class=" my-input file-field input-field" id="img">
                                    <div class="btn">
                                        <span>File</span>
                                        <input type="file" v-if="!isEdit" for="input-img" v-validate.reject="'required|mimes:image/*'" v-on:change="onFileChange" name="image">
                                        <input type="file" v-if="isEdit" for="input-img"  v-on:change="onFileChange" name="image">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="full-width all-border file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Title -->
                        <div class="form-line">
                            <div class="left">
                                <p>Title <span class="red-star">*</span></p>
                            </div>
                            <div class=" right my-input input-field">
                                <input placeholder="Placeholder" id="first_name" type="text" class="validate full-width title-input" v-model="book.bookTitle">
                            </div>
                        </div>

                        <!-- Authors -->
                        <div class="form-line">
                            <div class="left">
                                <p>Authors <span class="red-star">*</span></p>
                            </div>
                            <div class=" right ">

                                <author-select :multi="true" @input="authorChange" :value="selectedAuthors"></author-select>

                            </div>
                        </div>

                        <!-- Categories -->
                        <div class="form-line">
                            <div class="left">
                                <p>Categories <span class="red-star">*</span></p>
                            </div>

                            <div class=" right ">
                                <category-select :multi="true" @input="categoryChange" :value="selectedCategories"></category-select>

                            </div>
                        </div>


                        <!-- Publisher -->
                        <div class="form-line">
                            <div class="left">
                                <p>Publisher <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div class="right-divide">
                                    <publisher-select  @input="publisherChange" :value="selectedPublisher"></publisher-select>
                                </div>
                            </div>
                        </div>

                        <!-- Genere -->
                        <div class="form-line">
                            <div class="left">
                                <p>Genere <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div >
                                    <genre-select :multi="true" @input="genreChange" :value="selectedGenres"></genre-select>

                                </div>

                            </div>
                        </div> <!-- Genere end-->

                        <!-- Genere -->
                        <div class="form-line">
                            <div class="left">
                                <p>Edition <span class="red-star">*</span></p>
                            </div>
                            <div class=" right">
                                <div >
                                    <edition-select @input="editionChange" :value="selectedEdition"></edition-select>
                                </div>

                            </div>
                        </div> <!-- Genere end-->

                        <!-- Description -->
                        <div class="form-line">
                            <div class="left">
                                <p>Description </p>
                            </div>
                            <div class="right ">
                                <textarea rows="7" v-model="book.bookDescription"></textarea>
                            </div>
                        </div>

                        <!-- Upload ebook  -->
                        <div class="form-line upload-ebook">
                            <div class="left">
                                <p>Upload Ebook <span class="red-star">*</span></p>
                            </div>

                            <div class="right">
                                <div class=" my-input file-field input-field" id="img">
                                    <div class="btn">
                                        <span>File</span>
                                        <input type="file"  v-if="!isEdit"  for="input-img" v-validate.reject="'required'" v-on:change="onEbookFileChange" name="ebookfile">
                                        <input type="file"  v-if="isEdit"  for="input-img"  v-on:change="onEbookFileChange" name="ebookfile">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="full-width all-border file-path validate" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- buttons -->
                        <div class="form-line ">
                            <div class="left">

                            </div>
                            <div class="right save-buttons">
                                <button class="waves-effect waves-light btn close-button"> Close</button>
                                <button class="waves-effect waves-light btn" type="submit">Save Changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </manage-ebooks>

@endsection