<?php

namespace App\Actions\Publisher;


class GetPublisherID extends BasePublisherAction
{
    protected function onValidationSuccess()
    {
        $publisher = $this->repository->getBy($this->data()['id']);
        return $publisher;
    }
}