<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/12/2017
 * Time: 3:36 PM
 */

namespace App\Actions\Book;


class GetHotBook extends BaseBookAction
{
    public $pages = 20;

    protected function onValidationSuccess()
    {
        $books = $this->repository->getBooks();
        return $books;
    }
}