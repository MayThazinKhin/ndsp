@extends('layout.app')
@section('title','Genre')
@section('setup','active')

@section('content')
    <genre-list inline-template>
        <div>
            <h3>Genre List</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <button class="modal-trigger waves-effect waves-light btn" data-target="modal1" @click="toggleAddModal"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Genre</button>
                        </div>
                    </div>

                    <!-- Data Lists -->
                    <div id="modal1" class="modal">
                        <form @submit.prevent="submitdata">
                            <div class="modal-content">
                                <div class="modal-header"><h4>@{{ isEdit?'Edit':'Add'  }} Genre</h4></div>
                                <input type="text" name="genreName"  v-model="genre.genreName" v-validate="'required'" placeholder="Genre Name">
                                <div id="cemail-error" class="error" v-show="errors.has('genreName')">Required genre name.</div>
                            </div>
                            <div class="modal-footer">
                                <a data-target="modal1"  class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                <button type="submit"  class="modal-action waves-effect waves-green btn-flat add-btn">Add</button>
                            </div>
                        </form>
                    </div>

                    <table class="highlight striped data-table">
                        <thead>
                        <tr>

                            <th>No.</th>
                            <th>Name</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="genre,index in genres">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{genre.genreName}}</td>
                            <td>
                                <button  data-target="modal1" class="modal-trigger btn-floating" @click="toggleEdit(genre)"><i class="material-icons fa fa-pencil-square-o "></i></button>
                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(genre.id)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div><!-- card-content end -->
            </div>
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </genre-list>
@endsection