<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/12/2017
 * Time: 1:04 PM
 */

namespace App\Actions\Home;


use App\Actions\Action;
use App\Repositories\BookRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Collection;

class LoadHomePage extends  Action
{
    private $categoryRepository;
    public function __construct(BookRepository $repository,CategoryRepository $categoryRepository,Collection $data = null)
    {
        parent::__construct($data);
        $this->categoryRepository = $categoryRepository;
        $this->repository = $repository;
    }

    protected function onValidationSuccess()
    {
        $hot_books = $this->repository->getHotbooks();
        $categories =$this->categoryRepository->getAllCategories();
        $recent_books=$this->repository->getLastBooks(12);

        return compact('hot_books','categories','recent_books');
    }
}