<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 11/01/2018
 * Time: 2:38 PM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class GetBookByAuthorId extends BaseBookAction
{
    public $pages = 2;
    public $filtername='authors';
    protected $rules = ['author_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterById($this->data()['author_id'],$this->pages,$this->filtername);
    }
}