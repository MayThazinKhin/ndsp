<?php


namespace App\Http\Controllers\Api;


use App\Actions\Genre\GetGenres;
use App\Actions\Genre\SearchGenreByName;
use App\Http\Controllers\Controller;
use App\Repositories\GenreRepository;

class GenreController extends Controller
{
    private $repository;
    private $pages=2;
    public function __construct(GenreRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $action = new GetGenres($this->repository);
        $action->_page = $this->pages;
        $genres = $action->invoke();
        return response()->json($genres);
    }



    public function searchGenre($name){
        $_req = ['genreName'=>$name];
        $action = new SearchGenreByName($this->repository,collect($_req));
        $genres = $action->invoke();
        return response()->json($genres);
    }
}