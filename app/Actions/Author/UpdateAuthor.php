<?php
namespace App\Actions\Author;

class UpdateAuthor extends BaseAuthorAction
{
    protected $rules = [
        'id'=>'required',
        'authorName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $author= $this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($author) {
            return true;
        }
        return false;
    }
}