<?php

namespace App\Actions\Book\Filter;

use App\Actions\Book\BaseBookAction;

class ByAuthor extends BaseBookAction
{
    public $pages = 20;
    public $filtername='authors';
    protected $rules = [
        'author_id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['author_id'],$this->data()['buy_or_consigment'], $this->pages,$this->filtername);
    }
}