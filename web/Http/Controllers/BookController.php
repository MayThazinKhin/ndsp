<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/12/2017
 * Time: 4:01 PM
 */

namespace Web\Http\Controllers;



use App\Actions\Book\GetBookByID;
use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use App\Repositories\BookRepository;
use function foo\func;


class BookController extends  Controller
{
    private $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;

    }

    public function getImage($name)
    {
        $book = new BookImage($name);
        return $book->getFileResponse();
    }

    public function detailView(){
        return view('book.detail.index');
    }

    public function detail($book_id)
    {

        $request = ['id' => $book_id];
        $action = new GetBookByID($this->repository, collect($request));
        $book = $action->invoke();

        if ($book) {
            $comments=$book->comments()->orderbydesc('created_at')->take(5)->get();
            $_id = $book->id;

            $category_id = Category::whereHas('books', function ($q) use ($_id) {
                $q->where('id', $_id);
            })->select('id')->get()->pluck('id');

            $relatedbooks = Book::whereHas('categories', function ($q) use ($category_id) {
                $q->whereIn('category_id', $category_id);
            })->where('id', '<>', $book_id)->take(4)->get();
            $topbooks = Book::whereHas('categories', function ($q) {
                $q->where('categoryName', 'Top');
            })->where('id', '<>', $book_id)->take(10)->get();

             return response()->json(['book'=>$book,'relatedbooks'=>$relatedbooks,'topbooks'=>$topbooks,'comments'=>$comments]);


//            return view('book.detail.index', compact('book', 'relatedbooks','topbooks'));
        }

        return redirect()->back();
    }
    public function dowloadebook($name)
    {
        $book = new BookPdf($name);
        if ($book->exists()) {
            return $book->getFileResponse();
        }
        return null;
    }
}