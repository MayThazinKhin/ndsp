<?php

namespace Admin\Http\Controllers;


use App\Actions\Book\GetBookDetail;
use App\Actions\Ebook\CreateEbook;
use App\Actions\Ebook\DeleteEBook;
use App\Actions\Ebook\EbookDetails;
use App\Actions\Ebook\Filter\ByAuthor;
use App\Actions\Ebook\Filter\ByCategory;
use App\Actions\Ebook\Filter\ByGenre;
use App\Actions\Ebook\GetAllEbook;
use App\Actions\Ebook\UpdateEbook;
use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerTrait;

use App\Repositories\EbookRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EbookController extends  Controller
{
    use ControllerTrait;
    private $commentRepository;
    public function __construct(EbookRepository $repository)
    {
        $this->repository = $repository;

    }

    public function index()
    {
        return view('ebook.index');
    }

    function showActionPage()
    {
        return view('ebook.action');
    }

    function create(Request $request)
    {
        $action = new CreateEbook($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()], 422);
        }
        return response()->json($result);
    }

    function update(Request $request)
    {
        $action = new UpdateEbook($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {

                return response()->json([$result->errors()],422);
        }
        return response()->json($result,200);
    }

    function delete($id)
    {
        $request = [$this->pk_ID => $id];

        $action = new DeleteEBook($this->repository, collect($request));
        $action->invoke();
        return back();
    }

    function getAll()
    {
        $action = new GetAllEbook($this->repository);
        $books = $action->invoke();
        return response()->json($books);
    }

    function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new EbookDetails($this->repository, collect($request));
        $book = $action->invoke();
        return response()->json($book);
    }

    function viewByAuthor($author_id)
    {
        $request = ['author_id' => $author_id];
        $action = new ByAuthor($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByCategory($category_id)
    {
        $request = ['category_id' => $category_id];
        $action = new ByCategory($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }

    function viewByGenre($genre_id)
    {
        $request = ['genre_id' => $genre_id];
        $action = new ByGenre($this->repository, collect($request));
        $books = $action->invoke();
        return response()->json($books);
    }
}