import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';

export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {

            route: {
                get: '/admin/user/all?page=',
                create: '/admin/user/create',
                edit: '/admin/user/edit',
                delete: '/admin/user/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            users: [],
            user: {id: null, username: null,password:null,password_confirmation:null},

        }
    },

    methods: {
        clearData() {
            this.user.id = null;
            this.user.username = null;
            this.user.password=null;
            this.user.password_confirmation=null;
            this.$validator.clean();
        },
        getUsers(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.users = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        onPaginationClick() {
            this.getUsers();
        },

        toggleAddModal() {
            this.clearData();

        },
        toggleEdit(user) {

            var temp = Object.assign({}, user);
            this.user = temp;
            this.$validator.clean();
        },

        toggleDelete(username) {

            this.user.username = username;
        },
        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    axios.post(this.route.create, this.user).then(response => {
                        this.getUsers(this.route.get);
                        $('#user-modal').modal('close');
                    }).catch(error => {
                    });
                }
            }).catch(() => {
            });
        },
        changePassword(){
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    axios.post(this.route.create, this.useredit).then(response => {
                        this.getUsers(this.route.get);
                        $('#modal2').modal('close');
                    }).catch(error => {
                        if (error.response.data.status == 401) {
                            window.location.href=Helper.loginPage();
                        }else if(error.response.data.status == 422) {
                            this.$parent.showToast('Invalid data.', 'warn');
                        }else{
                            this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                        }
                    });
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.user.username + '/delete').then(response => {
                this.getUsers(this.route.get);
                this.user.username = null;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getUsers(this.route.get);
        }
    },

    mounted() {
        this.getUsers(this.route.get);
    }
}