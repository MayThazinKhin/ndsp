<?php

namespace App\Models;

use App\FileSystem\PDF_Files\BookPdf;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = ['edition_id', 'publisher_id',
        'bookTitle', 'bookDescription', 'bookSalePrice',
        'bookCoverImgUrl', 'isEbook', 'e_book_download','buy_or_consigment'];

    protected $appends = ['author', 'ebook_size','category','rating'];

    public function edition()
    {
        return $this->belongsTo(Edition::class);
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
    /**
     * 1 Book may have many authors
     * Any author may be applied to many books
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    /**
     * 1 Book may have many categories
     * Any category may be applied to many books
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * 1 Book may have many genres
     * Any genre may be applied to many books
     */
    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function getAuthorAttribute()
    {
        $authors = $this->authors()->where('book_id', $this->id)->get()->pluck('authorName');
        return implode(",", $authors->toArray());
    }

    public function getCategoryAttribute()
    {
        $categories = $this->categories()->where('book_id', $this->id)->where('categoryName','!=','Top')->get()->pluck('categoryName');

        return implode(",", $categories->toArray());
    }

    public function getEbookSizeAttribute()
    {
        if ($this->e_book_download) return (new BookPdf($this->e_book_download))->getFileSize();
    }

    public function getRatingAttribute(){
        $rating_0_5 = Comment::where('book_id', '=', $this->id)->where('rating', '=', 1)->count() * 1;
        $rating_1 = Comment::where('book_id', '=', $this->id)->where('rating', '=', 2)->count() * 2;
        $rating_1_5 = Comment::where('book_id', '=', $this->id)->where('rating', '=', 3)->count() * 3;
        $rating_2 = Comment::where('book_id', '=', $this->id)->where('rating', '=', 4)->count() * 4;
        $rating_2_5 = Comment::where('book_id', '=', $this->id)->where('rating', '=', 5)->count() * 5;
        $total_rating = Comment::where('book_id', '=', $this->id)->count();
        $rating = 0;
        if ($total_rating > 0) {
            $rating = ($rating_0_5 + $rating_1 + $rating_1_5 + $rating_2 + $rating_2_5) / $total_rating;
        }
        return floor($rating);
    }

}