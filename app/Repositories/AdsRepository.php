<?php

namespace App\Repositories;

use App\Models\Advertisment;

class AdsRepository extends Repository
{

    function model()
    {
       return Advertisment::class;
    }
}