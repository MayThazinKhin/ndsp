<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 1:37 PM
 */

namespace App\Actions\Sale;


class DeleteSale extends  BaseSaleAction
{
    protected function onValidationSuccess()
    {
        $sale = $this->repository->delete($this->data()->toArray());
        if ($sale) {
            return true;
        }
        return false;
    }
}