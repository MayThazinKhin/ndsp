import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';

export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {
            isEdit: false,
            route: {
                get: '/admin/category/all?page=',
                create: '/admin/category/create',
                edit: '/admin/category/edit',
                delete: '/admin/category/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            categories: [],
            category: {id: null, categoryName: null}
        }
    },

    methods: {
        clearData() {
            this.category.id = null;
            this.category.categoryName = null;
            this.$validator.clean();
        },
        getList(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.categories = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        onPaginationClick() {
            this.getList(this.route.get);
        },

        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(category) {
            var temp = Object.assign({}, category);
            this.category = temp;
            this.isEdit = true;
        },

        toggleDelete(id) {
            this.category.id = id;
        },
        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.category).then(response => {
                this.$parent.showToast('Success', 'success');
                this.getList(this.route.get);
                $('#modal1').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.category.id + '/delete').then(response => {
                this.$parent.showToast('Success', 'success');
                this.getList(this.route.get);
                this.category.id = null;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getList(this.route.get);
        }
    },
    mounted() {
        this.getList(this.route.get);
    }
}