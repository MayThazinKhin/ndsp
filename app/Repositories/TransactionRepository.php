<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 12:24 PM
 */

namespace App\Repositories;


use App\Models\Transaction;

class TransactionRepository extends Repository
{

    function model()
    {
       return Transaction::class;
    }
    public function paginate($perPage = 15, $columns = array('*')) {
        return Transaction::with('book')->orderby('id','desc')->paginate($perPage, $columns);
    }
}