@extends('layout.app')
@section('title','Order')
@section('order','active')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin/order-list.css')}}">
    @endsection
@section('content')
    <order-list inline-template>
        <div>
            <h3>Order List</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <a href="{{route('admin.order.create')}}" class="waves-effect waves-light btn"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Order</a>
                        </div>

                        <div class="content-up-right">
                            <div class="radio-group-1">
                                <!-- Pending radio -->
                                <p>
                                    <input class="with-gap" name="group1" type="radio" id="pending"  :value="status.pending" v-model="filterchecked" @change="filterCheckedChange"/>
                                    <label for="pending">Pending</label>
                                </p>
                                <!-- confirm radio button -->
                                <p>
                                    <input class="with-gap"  name="group1" type="radio" id="confirm"  :value="status.confirm" v-model="filterchecked" @change="filterCheckedChange"/>
                                    <label for="confirm">Confirm</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="group2" type="radio"  id="ontheway" :value="status.ontheway" v-model="filterchecked" @change="filterCheckedChange"/>
                                    <label for="ontheway">On the way</label>
                                </p>
                                <p>
                                    <input class="with-gap" name="group2" type="radio"  id="deliver" :value="status.deliver" v-model="filterchecked" @change="filterCheckedChange"/>
                                    <label for="deliver">Deliver</label>
                                </p>
                                <!-- return radio -->
                                <p>
                                    <input class="with-gap" name="group2" type="radio" id="return" :value="status.return" v-model="filterchecked" @change="filterCheckedChange"/>
                                    <label for="return">Return</label>
                                </p>
                            </div>



                            <div class="group-3">
                                <multiselect :show-labels="false" :searchable="false" class="search-input" @input="onOptionValueChange"
                                             v-model="selectedOption"  track-by="text" label="text" :options="filterOptions"  :allow-empty="false" ></multiselect>


                                <div class="search-bar">
                                    <input type="text" placeholder="search" class="search-box" v-model="filteretext">
                                    <button class="search-btn waves-effect waves-light" @click="onSearchClick">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div> <!-- content-up end -->


                    <!-- Data Lists -->
                    <table class="highlight striped data-table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Eamil</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Order Date</th>
                            <th>Order Count</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="order,index in orders">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{order.email}}</td>
                            <td>@{{order.phone}}</td>
                            <td>@{{order.address}}</td>
                            <td>@{{formatDate(order.created_at)}}</td>
                            <td>@{{order.order_count}}</td>
                            <td>
                                <a :href="detail(order.email,order.phone,order.address)" class="btn-floating "><i class="material-icons fa fa-eye fa-lg"></i></a>

                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(order)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div><!-- card-content -->

            </div><!-- content-down end -->

            <!-- paginations -->
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </order-list>
@endsection