<?php
namespace App\Actions\Ebook;

use App\Repositories\CommentRepository;
use App\Repositories\EbookRepository;
use Illuminate\Support\Collection;

class EbookDetails extends BaseEbookAction
{


    public function __construct(EbookRepository $repository, Collection $data = null)
    {
        parent::__construct($repository, $data);

    }


    protected function onValidationSuccess()
    {
        $book = $this->repository->getEbookByID($this->data()['id']);
        return $book;
    }
}