<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/01/2018
 * Time: 2:35 PM
 */

namespace App\Actions\Ebook\Filter;


use App\Actions\Ebook\BaseEbookAction;

class ByCategory  extends BaseEbookAction
{
    public $pages = 20;
    public $filtername='categories';
    protected $rules = [
        'category_id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['category_id'], $this->pages, $this->filtername);
    }
}