<?php


namespace App\Actions\Order;


use App\Actions\Action;
use App\Repositories\OrderRepository;
use Illuminate\Support\Collection;

abstract class BaseOrderAction extends Action
{
    protected $rules = [];

    public function __construct(OrderRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;

    }


    protected function onValidationSuccess()
    {
    }
}