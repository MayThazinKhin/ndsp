<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 17/11/2017
 * Time: 3:13 PM
 */

namespace App\Actions\Book;


class GetTopAllBooks extends  BaseBookAction
{
    protected function onValidationSuccess()
    {
        $books = $this->repository->getTopAllBooks();
        return $books;
    }

}