<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 08/02/2018
 * Time: 11:38 AM
 */

namespace App\Http\Controllers\Api;


use App\Actions\Article\GetArticleByID;
use App\Actions\Article\GetArticles;
use App\FileSystem\Images\ArticleCoverPhoto;
use App\Http\Controllers\Controller;
use App\Repositories\ArticleRepository;

class BlogController extends Controller
{
    private $repository;
    private $pages=2;
    public function __construct(ArticleRepository $repository)
    {
        $this->repository=$repository;
    }

    public function getAll(){
        $action = new GetArticles($this->repository);
        $action->_page=$this->pages;
        $result = $action->invoke();
        return response()->json($result);
    }

    public function getByID($id)
    {
        $request = ['id' => $id];
        $action = new GetArticleByID($this->repository, collect($request));
        $result = $action->invoke();
        return response()->json($result);
    }

    public function getImage($name)
    {
        $book = new ArticleCoverPhoto($name);
        if($book->exists()){
            return $book->getFileResponse();
        }
        return null;
    }
}