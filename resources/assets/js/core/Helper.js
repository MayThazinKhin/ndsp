import  moment from 'moment';

class Helper {

    static  loginPage(){
        return '/admin';
    }

    static   getUrlParameter(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    static  formatDate(date){
        return moment(date).format("YYYY-MM-DD");
    }
}

export default Helper;