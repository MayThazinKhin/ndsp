<?php


namespace App\Actions\Ads;


use App\FileSystem\Images\AdsImage;

class CreateAds extends BaseAdsAction
{

    protected $rules = ['name' => 'required|max:255', 'image' => 'required'];

    protected function onValidationSuccess()
    {
        $ads = $this->data();

        $adsImage = new AdsImage($ads['image']);
        $status = $adsImage->store();
        if (!$status) return false;

        $ads['image'] = $adsImage->getStoredName();

        $this->repository->create($ads->toArray());
        if ($ads) {
            return true;
        }
        return false;
    }
}