<?php


namespace App\Repositories;


use App\Models\Book;

class EbookRepository extends  Repository
{
    private $id = 'id';
    private $bookTitle = 'bookTitle';
    private $bookCoverImgUrl = 'bookCoverImgUrl';
    private $ebook_size = 'ebook_size';
    private $e_book_download = 'e_book_download';

    private $ebook = 1;

    function model()
    {
        return Book::class;
    }


    public function getEbooksWithPaginate($page)
    {
        return Book::with('edition', 'publisher')->where('isEbook', 1)->paginate($page);
    }

    //new off
    public function filterBy($id, $page, $filterby = 'authors')
    {

        return Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
                ->where('isEbook', 1)
                ->whereHas($filterby, function ($q) use ($id) {
                    $q->where('id', $id);
                })
              ->orderBy('created_at', 'desc')
                ->paginate($page);
    }

    public function getEbookByID($id)
    {
        $book = Book::with('edition', 'publisher', 'genres', 'authors', 'categories')->where('id', '=', $id)->first();
        return $book;
    }

    public function getEBooks()
    {
        $books = Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl, $this->e_book_download)
                ->where('isEbook', '=', $this->ebook)
                ->orderBy('created_at', 'desc')->paginate(20);
        return $books;
    }

    public function getBookByName($name,$pages)
    {
        $books = Book::with('edition', 'publisher', 'genres', 'authors', 'categories')
                 ->where('isEbook',1)
                 ->where( 'bookTitle', 'LIKE', $name.'%')
                 ->orderBy('created_at', 'desc')
                 ->paginate($pages);
        return $books;
    }

    public function findByEBookName($name)
    {
        $ebooks = Book::where('bookTitle', 'LIKE', $name . '%')
            ->where('isEbook', '=', 1)
            ->orderBy('bookTitle', 'ASC')->select('id','bookTitle')->take(10)->get();
        return $ebooks->makeHidden(['author','ebook_size','category','rating']);
    }

    public function getLastEBooks($_takeCount)
    {
        $books = Book::select($this->id, $this->bookTitle, $this->bookCoverImgUrl, $this->e_book_download)->
        where('isEbook', '=', $this->ebook)->take($_takeCount)->orderBy('created_at', 'desc')->get();
        return $books;
    }
}