@extends('layout.app')
@section('title','Edition')
@section('setup','active')

@section('content')
    <edition-list inline-template>
        <div>
            <h3>Edition List</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <button class="modal-trigger waves-effect waves-light btn" data-target="modal1" @click="toggleAddModal"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Edition</button>
                        </div>
                    </div>

                    <!-- Data Lists -->
                    <div id="modal1" class="modal" ref="addauthormodal">
                        <form @submit.prevent="submitdata">
                            <div class="modal-content">
                                <div class="modal-header"><h4>@{{ isEdit?'Edit':'Add'  }} Edition</h4></div>
                                <input type="text" name="editionName"  v-model="edition.editionName" v-validate="'required'" placeholder="Edition Name">
                                <div id="cemail-error" class="error" v-show="errors.has('editionName')">Required edition name.</div>
                            </div>
                            <div class="modal-footer">
                                <a data-target="modal1"  class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                <button type="submit"  class="modal-action waves-effect waves-green btn-flat add-btn">Add</button>
                            </div>
                        </form>
                    </div>

                    <table class="highlight striped data-table">
                        <thead>
                        <tr>

                            <th>No.</th>
                            <th>Name</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="edition,index in editions">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{edition.editionName}}</td>
                            <td>
                                <button  data-target="modal1" class="modal-trigger btn-floating" @click="toggleEdit(edition)"><i class="material-icons fa fa-pencil-square-o "></i></button>
                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(edition.id)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div><!-- card-content end -->
            </div>
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </edition-list>
@endsection