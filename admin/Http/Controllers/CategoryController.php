<?php

namespace Admin\Http\Controllers;

use App\Actions\Category\CreateCategory;
use App\Actions\Category\DeleteCategory;
use App\Actions\Category\GetCategories;
use App\Actions\Category\GetCategoryByID;
use App\Actions\Category\SearchCategoryByName;
use App\Actions\Category\UpdateCategory;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\CategoryRepository;

use Illuminate\Validation\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CategoryController extends Controller
{
    use ControllerTrait;


    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('category.index');
    }

    public function create(Request $request)
    {
        $action = new CreateCategory($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($request instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {

        $action = new UpdateCategory($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json($result->errors());
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new DeleteCategory($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function getAll()
    {
        $action = new GetCategories($this->repository);
        $categories = $action->invoke();
        return response()->json($categories);
    }

    public function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new GetCategoryByID($this->repository, collect($request));
        $category = $action->invoke();
        return response()->json($category);
    }

    public function getByName($name){
        $request = ['categoryName' => $name];
        $action = new SearchCategoryByName($this->repository, collect($request));
        $category = $action->invoke();
        return response()->json($category);
    }
}