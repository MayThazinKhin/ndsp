<?php
/**
 * Created by PhpStorm.
 * User: yanyan
 * Date: 11/11/2017
 * Time: 2:06 AM
 */

namespace App\Actions\Ebook;


class GetEBook extends BaseEbookAction
{
    public $pages = 20;
    protected function onValidationSuccess()
    {
        $books = $this->repository->getEBooks();
        return $books;
    }
}