import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';
import multiselect from 'vue-multiselect';
export default {
  components: {
    VuePagination,deleteModal,multiselect
  },

  data: function () {
      return {

        route: {
          get: '/admin/order/all/',
          filterbyEmail: '/admin/order/filterbyemail/',
          filterbyPhone: '/admin/order/filterbyphone/',
        },
        filterOptions: [
          {value: null, text: 'Filter By',url:null},
          {value: "phone", text: 'Phone',url:'/admin/order/filterbyphone/'},
          {value: "email", text: 'Email',url:'/admin/order/filterbyemail/'},
          {value: 'delivery_charge',text:'Delivery Charge'}
        ],
        selectedOption:  {value: null, text: 'Filter By',url:null},
        pagination: {
          total: 0,
          per_page: 2,
          from: 1,
          to: 0,
          current_page: 1,
          last_page: 1,
        },
        orders:[],
        orderdelete_url:'',
        filterchecked:'',
        filteretext:null,
        status:{
          confirm:'confirm',
          pending:'pending',
          ontheway:'ontheway',
          return:'return',
          deliver:'deliver'
        }
      }
  },

  methods: {
    onOptionValueChange () {
        if(this.selectedOption.value==null){
          this.filteretext=null;
        }
    },
    filterCheckedChange () {
      this.loadData(this.route.get + this.filterchecked);
    },
    toggleDelete (order) {

      this.orderdelete_url = '/admin/order/delete?email=' + order.email + '&phone=' + order.phone + '&address=' + order.address+'&status='+this.filterchecked;
    },
    detail (email, phone, address) {
      return '/admin/order/view-detail?email=' + email + '&phone=' + phone + '&address=' + address+'&status='+this.filterchecked;
    },
    performdelete () {

      axios.get(this.orderdelete_url).then(response => {
        this.$parent.showToast('Success', 'success');
        this.loadData(this.route.get+this.filterchecked);
      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href = Helper.loginPage();
        } else {
          this.$parent.showToast('Error occured while deleting data.');
        }
      });
      $('#delete-modal').modal('close');
    },
    formatDate (date) {
      return Helper.formatDate(date);
    },
    loadData (url) {
      axios.get(url + '?page=' + this.pagination.current_page).then(response => {
        this.pagination = response.data;
        this.orders = response.data.data;
      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href = Helper.loginPage();
        } else {
          this.$parent.showToast('Error occured while loading data.');
        }
      });
    },
    onSearchClick () {
      if (this.selectedOption.value == null) {
        this.loadData(this.route.get + this.filterchecked);

      } else {
        if (this.filteretext) {
          let url = this.selectedOption.url + this.filterchecked + '/' + this.filteretext;
          this.loadData(url);
        }else{
          this.loadData(this.route.get+this.filterchecked);
        }
      }
    }
  },
  mounted () {
    this.filterchecked=this.status.pending;
    this.loadData(this.route.get+this.filterchecked);
  }
}