<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 23/02/2018
 * Time: 3:25 PM
 */

namespace App\Actions\Book;


class AsyncFindBook extends BaseBookAction
{

    protected function onValidationSuccess()
    {
        return $this->repository->asyncByBookName($this->data()['bookTitle']);
    }
}