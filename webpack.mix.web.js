let mix = require('laravel-mix');

mix.js('web/assets/add-to-card.js', 'public/js/web')
    .js('web/assets/order.js', 'public/js/web')
    .extract(['vue', 'axios', 'vee-validate', 'moment', 'vue-star-rating', 'vue-multiselect', 'v-toaster']);