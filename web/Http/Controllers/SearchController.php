<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/02/2018
 * Time: 11:06 AM
 */

namespace Web\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use App\Models\Publisher;
use function foo\func;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $pages = 20;

    public function searchIndex()
    {
        $books = Book::orderBy('created_at', 'desc')->paginate($this->pages);
        $categories = Category::orderBy('categoryName', 'asc')->get();
        return view('search.search', compact('books', 'categories'));
    }

    public function search(Request $request)
    {
        $categories = Category::orderBy('categoryName', 'asc')->get();
        $paramname = $request->paramname;

        $col = [];
        $booksByGenre = Book::whereHas('genres', function ($q) use ($paramname) {
            $q->where('genreName', 'Like', $paramname . '%');
        })->select('id')->get()->makeHidden(['author', 'ebook_size', 'category', 'rating'])->pluck('id');;

        $booksByCategories = Book::whereHas('categories', function ($q) use ($paramname) {
            $q->where('categoryName', 'Like', $paramname . '%');
        })->select('id')->get()->makeHidden(['author', 'ebook_size', 'category', 'rating'])->pluck('id');

        $col = $booksByGenre->merge($booksByCategories);

        $booksByAuthors = Book::whereHas('authors', function ($q) use ($paramname) {
            $q->where('authorName', 'Like', $paramname . '%');
        })->select('id')->get()->makeHidden(['author', 'ebook_size', 'category', 'rating'])->pluck('id');
        $col = $col->merge($booksByAuthors);

        $booksByPublisher = Book::whereHas('publisher', function ($q) use ($paramname) {
            $q->where('publisherName', 'Like', $paramname . '%');
        })->select('id')->get()->makeHidden(['author', 'ebook_size', 'category', 'rating'])->pluck('id');
        $col = $col->merge($booksByPublisher);

        $booksByTitle = Book::where('bookTitle', 'LIKE', $paramname . '%')->select('id')->get()->makeHidden(['author', 'ebook_size', 'category', 'rating'])->pluck('id');
        $col = $col->merge($booksByTitle);

        $books = Book::whereIn('id', $col)->orderby('created_at', 'desc')->paginate($this->pages);

        return view('search.search', compact('books', 'categories'));
    }

    public function byCategory($id)
    {
        $paramname = $id;
        $categories = Category::orderBy('categoryName', 'asc')->get();
        $books = Book::whereHas('categories', function ($q) use ($paramname) {
            $q->where('id', $paramname);
        })->orderBy('created_at', 'desc')->paginate($this->pages);

        return view('search.search', compact('books', 'categories'));
    }
    public function byCategoryName($name)
    {
        $paramname = $name;
        $categories = Category::orderBy('categoryName', 'asc')->get();
        $books = Book::whereHas('categories', function ($q) use ($paramname) {
            $q->where('categoryName', $paramname);
        })->orderBy('created_at', 'desc')->paginate($this->pages);

        return view('search.search', compact('books', 'categories'));
    }
    public function getEbook()
    {

        $categories = Category::orderBy('categoryName', 'asc')->get();
        $books = Book::where('isEbook',1)->orderBy('created_at', 'desc')->paginate($this->pages);

        return view('search.search', compact('books', 'categories'));
    }
}