// Register a global custom directive called `v-focus`
export default {
    bind: function(el, binding, vnode) {
        $(function() {
            $(el).material_select();
        });
        var arg = binding.arg;
        if (!arg) arg = "change";
        arg = "on" + arg;
        el[arg] = function() {
            vnode.context.$data.selected = [];
            for (let i = 0; i < 12; i++) {
                if (el[i].selected === true) {
                    vnode.context.$data.selected.push(el[i].value);
                }
            }
        };
    },
    unbind: function(el) {
        $(el).material_select("destroy");
    }
};