<?php


namespace Admin\Http\Controllers;

use App\Actions\Publisher\CreatePublisher;
use App\Actions\Publisher\DeletePublisher;
use App\Actions\Publisher\GetPublisherID;
use App\Actions\Publisher\GetPublishers;
use App\Actions\Publisher\SearchByPublisher;
use App\Actions\Publisher\UpdatePublisher;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\PublisherRepository;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class PublisherController extends  Controller
{
    use ControllerTrait;

    public function __construct(PublisherRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('publisher.index');
    }

    public function create(Request $request)
    {
        $action = new CreatePublisher($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($request instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $action = new UpdatePublisher($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new DeletePublisher($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function getAll()
    {
        $action = new GetPublishers($this->repository);
        $publishers = $action->invoke();
        return response()->json($publishers);
    }

    public function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new GetPublisherID($this->repository, collect($request));
        $publisher = $action->invoke();
        return response()->json($publisher);
    }

    public function getByName($name){
        $request = ['publisherName' => $name];
        $action = new SearchByPublisher($this->repository, collect($request));
        $publisher = $action->invoke();
        return response()->json($publisher);
    }
}