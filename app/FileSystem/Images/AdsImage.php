<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 9:50 AM
 */

namespace App\FileSystem\Images;


class AdsImage extends BaseImageFileSystem
{
    public function __construct($file) {

        parent::__construct($file);
        $this->path = 'images/ads';
    }
}