<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 8:56 AM
 */

namespace App\Actions\Admin;


class AdminChangePassword extends BaseAdminAction
{
    protected $rules = [
        'username' => 'required',
        'password' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $admin = $this->data();

        $admin['password'] = $this->cryptPassword();

        $admin = $this->repository->update($admin->toArray(), $admin[self::PK_USERNAME],self::PK_USERNAME);
        if ($admin) {
            return true;
        }
        return false;
    }
}