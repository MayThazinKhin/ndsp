import Vue from 'vue';
import axios from 'axios';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);


new Vue({
    el: '#orderconfirm',

    data: {
        confirm: true,
        books: [],
        email: null,
        phone: null,
        address: null,
        city: null,
        delivery_charge: 0,
        qty: 0,
        tempindex: null,
    },
    updated() {
    },
    methods: {

        calculateDeliveryCharge() {
            console.log('hello');
            let totalBookQty = 0;
            this.delivery_charge = 0;
            if (this.books.length > 0) {
                switch (Number(this.city)) {
                    case 1:
                        this.delivery_charge = 1000;
                        break;
                    case 2:
                        this.delivery_charge = 1000;
                        break;
                    case 3:
                        this.delivery_charge = 0;
                        this.books.forEach((book) => {
                            totalBookQty += Number(book.qty);
                        });
                        if (totalBookQty === 1) {
                            this.delivery_charge = 1000;
                        } else if (totalBookQty > 1) {
                            this.delivery_charge += 1000 + ((totalBookQty - 1) * 300)
                        }
                        break;
                }
            }
            console.log(this.delivery_charge);
        },
        remove(index) {
            console.log(index);
            this.books.splice(index, 1);
            this.calculateDeliveryCharge();
            var localStoreItems = localStorage.getItem('cart_books');

            if (localStoreItems) {
                let a = localStorage.removeItem('cart_books');
                if (this.books.length > 0) {
                    localStorage.setItem('cart_books', JSON.stringify(this.books));
                    $('#edit-modal').modal('hide');
                }
            }
        },
        editQty() {
            var localStoreItems = localStorage.getItem('cart_books');

            if (localStoreItems) {
                if (this.books.length > 0) {
                    localStorage.removeItem('cart_books');
                    this.books[this.tempindex].qty = this.qty;
                    localStorage.setItem('cart_books', JSON.stringify(this.books));
                    $('#edit-modal').modal('hide');
                    this.calculateDeliveryCharge();
                    this.calculateDeliveryCharge();
                }
            }
        },
        editClick(index, qty) {
            this.qty = qty;
            this.tempindex = index;
        },
        submitdata() {

            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    var config = {
                        headers: {'X-My-Custom-Header': 'Header-Value'}
                    };
                    axios.post('/order/create', {
                        email: this.email,
                        phone: this.phone,
                        address: this.address,
                        books: this.books,
                        town_id: this.city
                    }, config).then(response => {
                        localStorage.removeItem('cart_books');
                        window.location.href = '/';
                        console.log(response.data);
                    });
                }
            });
        },

        selectCity() {
            this.calculateDeliveryCharge();
        }

    },
    mounted() {
        this.books = JSON.parse(localStorage.getItem('cart_books'));
        if (this.books) {
            this.confirm = this.books.length > 0 ? false : true;

        } else {
            this.confirm = true;
        }
    }

});