import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';


export default {

  components: {
    VuePagination, deleteModal
  },

  data: function () {
    return {

      pagination: {
        total: 0,
        per_page: 2,
        from: 1,
        to: 0,
        current_page: 1,
        last_page: 1,
      },
      comments:[],
      comment_id:null
    }
  },

  methods: {
    getComments(){
      axios.get('/admin/comment/get_comments?page='+ this.pagination.current_page).then(response=>{
          this.comments=response.data.data;
          this.pagination=response.data;
      });
    },
    toggleDelete(id) {

      this.comment_id= id;
    },
    performdelete(){
      axios.get('/admin/comment/delete/'+this.comment_id).then(response => {
        this.getComments();
        this.comment_id = null;
        this.$parent.showToast('Success', 'success');
      }).catch(error => {
        if (error.response.data.status == 401) {
          window.location.href=Helper.loginPage();
        }else{
          this.$parent.showToast('Error occured while deleting data.');
        }
      });
      $('#delete-modal').modal('close');
    }
  },

  mounted() {
    this.getComments();
  }
}