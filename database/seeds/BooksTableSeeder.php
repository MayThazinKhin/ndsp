<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{

    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 10) as $i) {
            \App\Models\Book::create([
                'edition_id'        => rand(1, 5),
                'publisher_id'      => rand(1, 5),
                'bookTitle'         => $this->faker->text(5),
                'bookDescription'   => $this->faker->paragraph(),
                'bookSalePrice'     => $this->faker->numberBetween(1000, 10000),
                'bookCoverImgUrl'   => $this->faker->imageUrl(),
                'isEbook'           => 0,
                'buy_or_consigment' => 0
            ]);
        }
    }
}
