@extends('layout.master')
@section('title','Book Detail')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/slider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/detail.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/component.css')}}">
    <script typeof="text/" type="text/css" src="URL::{{asset('js/web/modernizr.custom.js')}}"></script>
    <style>
        .haha {
            display: inline-block !important;
            align-items: center;
        }

        [v-cloak] {
            display: none;
        }
    </style>

@endsection
@section('content')

    <div class=" product-view" id="addtocard">
        <div class="container" v-cloak>

            <!-- *********** Modal Start *********** -->
            <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Rating this Book!</h4>
                        </div>
                        <form role="form" @submit.prevent="submitdata">
                            <div class="modal-body">


                                <star-rating v-model="comment.rating" :star-size="30" class="haha"
                                             :show-rating="false"></star-rating>
                                <div class="form-group">
                                    <input id="modal-name" placeholder="Username" type="text" class="form-control"
                                           v-model="comment.username" name="username" v-validate="'required'">
                                    <div style="color: red;" v-show="errors.has('comment')">Required username.</div>
                                </div>
                                <div class="form-group">
                                    <textarea id="modal-name" class="form-control" placeholder="Comment"
                                              v-model="comment.comment" name="comment"
                                              v-validate="'required'"></textarea>
                                    <div style="color: red;" v-show="errors.has('comment')">Required comment.</div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- left-side -->
                <div class="col-md-9">
                    <div class="row">
                        <!-- left inner side -->

                        <div class="col-md-4">
                            <div class="book-image">
                                <img :src="'/image/book/'+book.bookCoverImgUrl">
                            </div>
                        </div><!-- left inner side end -->

                        <!-- right inner side -->
                        <div class="col-md-8">
                            {{--<div class="product-shop">--}}
                            <h1 class="book-name">@{{book.bookTitle}}</h1>
                            <p class="book-author">@{{book.author}}</p>
                            <div class="rating">
                                <star-rating v-model="book.rating" :star-size="20" :read-only="true"
                                             :show-rating="false"></star-rating>

                            </div>
                            {{--@if(!$book->isEbook)--}}
                            {{--<p class="availability">Availability: <span>In stock</span></p>--}}
                            <div v-if="!book.isEbook">
                                <div class="qty-box">
                                    <label for="qty" style="margin-right: 30px;">Qty:</label>
                                    <div class="qty-box-count">

                                        <input type="text" id="qty" v-model="qty">
                                        <button class="countBtn btn" style="position:absolute;left: 43px"
                                                @click="subQty" type="button">-
                                        </button>
                                        <button class="countBtn btn" @click="addQty" type="button">+</button>

                                    </div>
                                </div>

                                <p class="price-box">@{{formatNumber(book.bookSalePrice)}}</p>
                            </div>
                            <button class="add-to-cart" @click="addToStorage" type="button"><i
                                        class="fa fa-shopping-cart" aria-hidden="true" v-if="!book.isEbook"></i> add to
                                cart
                            </button>
                            {{--@endif--}}

                            <button class="add-comment" data-toggle="modal" data-target="#commentModal"> comment
                            </button>
                            {{--@if($book->isEbook)--}}
                            <a :href="'/ebook/download'+ book.e_book_download" v-if="book.isEbook">
                                <button class="add-comment"> download</button>
                            </a>
                            {{--@endif--}}

                            <div class="short-description">
                                <ul>
                                    <li>Publisher : <span>@{{book.publisher.publisherName}}</span></li>
                                    <li>Release Date : <span>@{{formatDate(book.created_at)}}</span></li>
                                    <li>Edition : <span>@{{book.edition.editionName}}</span></li>
                                </ul>
                                {{-- <p>@{{book.bookDescription}}</p> --}}
                            </div>
                            {{--</div><!-- product shop end -->--}}
                        </div> <!--right innr side end -->
                    </div>

                    <!-- product description -->
                    <div class="tab-books">
                        <!--tab nav menu -->
                        <div class="book-nav">
                            <ul class="nav nav-tabs">
                                <li><a href="#description" data-toggle="tab">description</a></li>
                                <li><a href="#comment" data-toggle="tab">comment</a></li>
                                {{--<li><a href="#additional" data-toggle="tab">additional</a></li>--}}
                                {{--<li><a href="#bookTags" data-toggle="tab">book tags</a></li>--}}
                            </ul>
                        </div>
                        <!-- Tab nav content -->
                        <div class="box">
                            <div class="tab-content">
                                <!-- description content -->
                                <div id="description" class="tab-pane fade in active">
                                    <p>
                                        @{{book.bookDescription}}
                                    </p>
                                </div>
                                <div id="comment" class="tab-pane fade">

                                    <div class="commment-box" v-for="cms in     comments">
                                        <span style="font-size:18px;">@{{ cms.username +'  '}}<i>@{{ formatDate(cms.created_at) }}</i></span>
                                        {{--<div class="mini-rating" style="padding:10px 0;">--}}

                                        <star-rating :rating="convertoInt(cms.rating)" :star-size="15" :read-only="true"
                                                     :show-rating="false"></star-rating>
                                        {{--</div>--}}
                                        <div class="cmt">
                                            <p>@{{ cms.comment }}</p>
                                        </div>
                                        <hr>
                                    </div>
                                    {{--<a href="#" style="text-align:center; color:red;" class="pull-right">Read More</a>--}}

                                </div>
                            </div>
                        </div><!-- tab nav content end-->
                    </div><!-- book description -->

                    <!-- INTERESTED BOOK SELL -->
                    <div class="interested-books">
                        <h1 class="interested-title">You may also be interested in the following product(s)</h1>
                        <ul class="product-grid">
                            {{--@foreach($relatedbooks as $book)--}}
                            <li class="item" v-for="rlbook in relatedbooks">
                                <div class="product-image">
                                    <a :href="'/book/detail?book_id='+rlbook.id">
                                        <img :src="'/image/book/'+rlbook.bookCoverImgUrl">
                                    </a>
                                </div>
                                <div class="product-information">

                                    <star-rating v-model="rlbook.rating" :star-size="20" :read-only="true"
                                                 :show-rating="false"></star-rating>
                                    {{--<i class="fa fa-star fa-x" aria-hidden="true" v-for="index in rlbook.rating"></i>--}}

                                    <p class="name"><a
                                                :href="'/book/detail?book_id='+rlbook.id">@{{rlbook.bookTitle}}</a></p>
                                    <p class="author">@{{rlbook.author}}</p>
                                    <p class="price">@{{formatNumber(rlbook.bookSalePrice)}}</p>
                                    <a :href="'/book/detail?book_id='+rlbook.id" class="quick-view"> Quick View</a>
                                </div>
                            </li>
                            {{--@endforeach--}}

                        </ul>
                    </div>
                </div> <!-- left side end -->

                <!-- right side -->
                <div class="col-md-3 right">
                    <div class="right-side">
                        <div class="block-banner">
                            <a href="#">
                                <img src="{{URL::asset('images/hp-new.jpg')}}">
                            </a>
                        </div>

                        <!-- related products -->
                        <div class="block-related">
                            <div class="block-title">
                                <p>related books</p>
                            </div>


                            <ol class="mini-book-list">
                                {{--@foreach($relatedbooks as $book)--}}
                                <li class="item" v-for="tpbook in topbooks">

                                    <div class="book">
                                        <a :href="'/book/detail?book_id='+tpbook.id">
                                            <img :src="'/image/book/'+tpbook.bookCoverImgUrl">
                                        </a>
                                    </div>
                                    <div class="book-detail">
                                        <p class="name"><a :href="'/book/detail?book_id='+tpbook.id">@{{tpbook.bookTitle}}</a>
                                        </p>
                                        <p class="author">@{{tpbook.author}}</p>
                                        <star-rating v-model="tpbook.rating" :star-size="15" :read-only="true"
                                                     :show-rating="false"></star-rating>

                                        <p class="price">@{{formatNumber(tpbook.bookSalePrice)}}</p>

                                    </div>
                                </li>
                                {{--@endforeach--}}
                            </ol>
                        </div>
                    </div>

                </div><!-- right side end -->

            </div>
        </div>


    </div>

@endsection

@section('script')


{{--    <script src="{{URL::asset('js/web/manifest.js')}}"></script>--}}
{{--    <script src="{{URL::asset('js/web/vendor.js')}}"></script>--}}
    <script src="{{URL::asset('js/web/add-to-card.js')}}"></script>
    <script>
        // this is important for IEs
        var polyfilter_scriptpath = '/js/';
    </script>
    <script src="{{URL::asset('js/web/cssParser.js')}}"></script>
    <script src="{{URL::asset('js/web/css-filters-polyfill.js')}}"></script>
    <script src="{{URL::asset('js/web/classie.js')}}"></script>
    <script src="{{URL::asset('js/web/modalEffects.js')}}"></script>
{{--    <script src="{{URL::asset('js/web/app.js')}}"></script>--}}
@endsection