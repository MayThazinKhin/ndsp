<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/19/2017
 * Time: 3:35 PM
 */

namespace App\Actions\Edition;


class GetEditionByID extends BaseEditionAction
{
    protected function onValidationSuccess()
    {
        $edition = $this->repository->getBy($this->data()['id']);
        return $edition;
    }
}