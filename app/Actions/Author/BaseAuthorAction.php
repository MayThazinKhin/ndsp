<?php

namespace App\Actions\Author;

use App\Actions\Action;
use App\Repositories\AuthorRepository;
use Illuminate\Support\Collection;

abstract class BaseAuthorAction extends  Action
{
    protected $rules = [];

    public function __construct(AuthorRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }

    protected function onValidationSuccess(){}
}