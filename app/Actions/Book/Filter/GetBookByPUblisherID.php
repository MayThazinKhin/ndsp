<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/01/2018
 * Time: 10:06 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class GetBookByPUblisherID extends BaseBookAction
{
    public $pages = 2;

    protected $rules = ['publisher_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->getBooksByPublisherId($this->data()['publisher_id'],$this->pages);
    }
}

