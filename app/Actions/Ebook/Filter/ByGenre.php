<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/01/2018
 * Time: 2:38 PM
 */

namespace App\Actions\Ebook\Filter;


use App\Actions\Ebook\BaseEbookAction;

class ByGenre extends BaseEbookAction
{
    public $pages = 20;
    public $filtername='genres';
    protected $rules = ['genre_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterBy($this->data()['genre_id'], $this->pages, $this->filtername);
    }
}