<?php

namespace App\Actions\Stock;

class CreateStock extends BaseStockAction
{
    protected function onValidationSuccess()
    {

        $stock = $this->repository->create($this->data()->toArray());
        if ($stock) {
            return true;
        }
        return false;
    }
}