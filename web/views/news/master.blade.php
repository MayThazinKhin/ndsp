<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book | Blog</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href='https://mmwebfonts.comquas.com/fonts/?font=myanmar3' />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.9/flickity.min.css">
    <link rel="stylesheet" href="{{URL::asset('css/web/blog-style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/web/animated.css')}}">
    <link rel="stylesheet" type="text/css"  href="{{URL::asset('css/web/style.css')}}">

</head>
<body>

<div class="div_1 blogpage" style="background-image: url('/images/hp1.jpg')">
    <nav class="navbar navbar-inverse navbar-fixed-top navbar1">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="{{URL::asset('images/logo-2.png')}}" alt="">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar1" id="navbar1">
                <ul class="nav navbar-nav navbar-right navbar1">
                      <li><a href="{{route('home')}}" id="nav-home">HOME</a></li>
        <li><a href="{{route('search')}}">BOOKS</a></li>
                    <li><a href="{{route('search/by-category-name',['name'=>'Music'])}}">Music</a></li>
        <li class="active"><a href="{{route('news')}}">NEWS</a></li>

                </ul>
            </div>
        </div>
    </nav>
    <h1>Blog</h1>
</div>


@yield('content')
<div class="div_1 blogpage">
    <!-- spotlight map -->
   @include('layout.footer')
</div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{URL::asset('js/web/blog-js.js')}}"></script>
    <script type="text/javascript">
      $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if(scroll >= 300) {

          $(".navbar1").addClass("change");
        } else {
          $(".navbar1").removeClass("change");
        }
      });
    </script>
    <script src="{{URL::asset('js/web/wow.min.js')}}"></script>
    <script>
      new WOW().init();
    </script>
</body>
</html>
