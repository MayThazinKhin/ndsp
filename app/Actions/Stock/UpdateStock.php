<?php

namespace App\Actions\Stock;



class UpdateStock extends BaseStockAction
{
    protected function onValidationSuccess()
    {
        $stock = $this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($stock) {
            return true;
        }
        return false;
    }
}