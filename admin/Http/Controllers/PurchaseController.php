<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 31/01/2018
 * Time: 10:11 AM
 */

namespace Admin\Http\Controllers;
use App\Actions\Transaction\CreateTransaction;
use App\Actions\Transaction\DeleteTransaction;
use App\Actions\Transaction\GetTransactions;
use App\Actions\Transaction\UpdateTransaction;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class PurchaseController extends Controller
{
    use ControllerTrait;
    public function __construct(TransactionRepository $repository)
    {
        $this->repository=$repository;
    }

    function index()
    {
        return view('purchase.index');
    }

    function create(Request $request)
    {
        $action = new CreateTransaction($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result  instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function update(Request $request)
    {
        $action = new UpdateTransaction($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function delete($id)
    {
        $request = [$this->pk_ID => $id];

        $action = new DeleteTransaction($this->repository, collect($request));
        $result = $action->invoke();
        if(!$result){
            return response()->json(['status'=>'failed'],500);
        }
        return response()->json($result);

    }

    function getAll()
    {
        $action = new GetTransactions($this->repository);
        $transactions = $action->invoke();
        return response()->json($transactions);
    }

    function getByID($id)
    {
        // TODO: Implement getByID() method.
    }
}