<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 07/03/2018
 * Time: 12:12 PM
 */

namespace App\Actions\Comment;


class GetComments extends  BaseCommentAction
{
    public  $_page=20;
    protected function onValidationSuccess()
    {
        $comments=$this->repository->getAll($this->_page);
        return $comments;
    }
}