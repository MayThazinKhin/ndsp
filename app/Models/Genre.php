<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $timestamps = false;
    protected $fillable = ['genreName'];
    protected $hidden = ['pivot'];
    public function books()
    {
        return $this->belongsToMany(Book::class);
    }
}
