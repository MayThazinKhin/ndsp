<?php


namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends Repository
{
    function model()
    {
        return Category::class;
    }

    public function getAllCategories(){
        return Category::orderBy('categoryName','ASC')->get();
    }

    public function allCategory(){
        return Category::orderBy('categoryName','ASC')->paginate(20);
    }

    function findByName($name){
        return Category::where('categoryName','LIKE',$name.'%')->take(10)->get();
    }

    public function paginate($perPage = 15, $columns = array('*')) {
        return Category::where('categoryName','<>','Top')->where('categoryName','<>','Music')->orderby('id','desc')->paginate($perPage, $columns);
    }
    public function getLimitColumn($count)
    {
     return Category::where('categoryName','<>','Top')->where('categoryName','<>','Music')->orderby('categoryName','asc')->take($count)->get();
    }
}