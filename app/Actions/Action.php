<?php

namespace App\Actions;

use Illuminate\Support\Collection;
use Illuminate\Validation\Validator;

abstract class Action
{
    protected $repository;
    protected $rules;

    private $data;

    protected function data(): Collection
    {
        return $this->data;
    }

    public function __construct(Collection $data = null)
    {
        if ($data != null) {
            $this->data = $data;
        }
    }

    protected function validate(): Validator
    {
        return validator($this->data->toArray(), $this->rules);
    }

    public function invoke()
    {
        if ($this->data) {
            $validator = $this->validate();
            if ($validator->fails()) {
                return $this->onValidationFails($validator);
            }
        }

        return $this->onValidationSuccess();
    }

    protected function onValidationFails(Validator $validator)
    {
        return $validator;
    }

    protected abstract function onValidationSuccess();
}