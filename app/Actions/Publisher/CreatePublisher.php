<?php

namespace App\Actions\Publisher;

class CreatePublisher extends BasePublisherAction
{
    protected $rules = [
        'publisherName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $publisher = $this->repository->create($this->data()->toArray());
        if ($publisher) {
            return true;
        }
        return false;
    }
}