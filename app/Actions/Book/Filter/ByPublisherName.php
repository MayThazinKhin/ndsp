<?php


namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByPublisherName extends  BaseBookAction
{
    public $pages = 20;

    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->getBookByPublisherName($this->data()['publisherName'],$this->pages);
    }
}