<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:32 AM
 */

namespace App\Repositories;


use App\Models\Article;

class ArticleRepository extends Repository
{
    function model()
    {
        return Article::class;
    }

    function paginate($perPage = 15, $columns = array('*'))
    {
        return Article::orderByDesc('created_at')->paginate($perPage);
    }
}