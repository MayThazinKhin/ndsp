<?php


namespace App\Actions\Comment;

class CreateComment extends BaseCommentAction
{
    protected $rules = [  'comment' => 'required',];

    protected function onValidationSuccess()
    {
        $comment = $this->repository->create($this->data()->toArray());
        if ($comment) {
            return true;
        }
        return false;
    }
}