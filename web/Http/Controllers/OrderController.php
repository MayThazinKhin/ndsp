<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 28/02/2018
 * Time: 10:22 AM
 */

namespace Web\Http\Controllers;


use App\Actions\Order\CreateOrder;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use Illuminate\Validation\Validator;
use Illuminate\Http\Request;


class OrderController extends Controller
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('book.orderconfirm');
    }

    public function create(Request $request)
    {
        $action = new CreateOrder($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json(['success' => 'false']);
        }
        if (!$result) {

            return response()->json(['success' => 'false'], 500);
        }
        return response()->json(['success' => 'true']);
    }
}