import multiselect from 'vue-multiselect';

export default {
    components: {
        multiselect
    },

    data: function () {
        return {
            books: [],
            selected_book: null,
            cities: [1, 2, 3],
            order: {
                book_id: null,
                bookTitle: null,
                price: 0,
                author: null,
                qty: 0,
                otw_or_deli_or_return: 'pending',

            },
            orders: {
                email: null,
                phone: null,
                address: null,
                books: [],
                delivery_charge: 0,
                city: null,
            },
        }
    },

    methods: {
        calculateDeliveryCharge() {
            this.orders.delivery_charge = 0;
            let totalBookQty = 0;
            //    Calculate delivery charge
            if (this.orders.books.length > 0) {
                switch (Number(this.orders.city)) {
                    case 1:
                        this.orders.delivery_charge = 1000;
                        break;
                    case 2:
                        this.orders.delivery_charge = 1000;
                        break;
                    case 3:
                        this.orders.delivery_charge = 0;
                        this.orders.books.forEach((book) => {
                            totalBookQty += Number(book.qty);
                        });
                        console.log(totalBookQty);
                        if (totalBookQty === 1) {
                            this.orders.delivery_charge = 1000;
                        } else if (totalBookQty > 1) {
                            this.orders.delivery_charge += 1000 + ((totalBookQty - 1) * 300)
                        }
                        break;
                }
            }
            console.log('delivery charge: ' + this.orders.books.length);
        },
        asyncFind(query) {
            if (query) {
                this.isSingleLoading = true;
                axios.get('/admin/asyncbook/' + query).then(response => {
                    this.books = response.data;
                    this.isSingleLoading = false;
                });
            }
        },

        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let temp = {};


                    temp.book_id = this.selected_book.id;
                    temp.bookTitle = this.selected_book.bookTitle;
                    temp.price = this.selected_book.bookSalePrice;
                    temp.qty = this.order.qty;
                    temp.otw_or_deli_or_return = this.order.otw_or_deli_or_return;
                    temp.author = this.selected_book.author;
                    temp.total = temp.price * temp.qty;

                    this.orders.books.push(temp);
                    this.calculateDeliveryCharge();
                    this.selected_book = null;
                    this.order.qty = 0;


                }
            }).catch(error => {
                console.log(error);
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess() {
            this.orders.town_id = Number(this.orders.city);
            axios.post('/admin/order/create', this.orders).then(response => {
                window.location.href = '/admin/order';


            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href = Helper.loginPage();
                } else if (error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }
            });
        },
        removeRow(index) {
            this.orders.books.splice(index, 1);
            this.calculateDeliveryCharge();
        },

    },
    mounted() {
        let vm = this;
        let element = $('#select-city');
        element.on('change', function () {
            vm.orders.city = element.val();
        });

    },
    updated() {
    }
}
