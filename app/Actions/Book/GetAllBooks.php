<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 07/12/2017
 * Time: 1:18 PM
 */

namespace App\Actions\Book;


class GetAllBooks extends BaseBookAction
{
    public $pages = 20;

    protected function onValidationSuccess()
    {
        $books = $this->repository->getBooksWithPaginate($this->pages);
        return $books;
    }
}