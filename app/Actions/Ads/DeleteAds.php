<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 9:30 AM
 */

namespace App\Actions\Ads;

use App\FileSystem\Images\AdsImage;

class DeleteAds extends BaseAdsAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $_ads = $this->repository->getBy($this->data()['id']);

        //if ads does not exists
        if (!$_ads) return false;


        //if ads exists delete ads image
        $adsImg = new AdsImage($_ads->image);
        $status = $adsImg->delete();

        //if not success image delete
        if (!$status) return false;

        //delete ads from table
        $ads = $this->repository->delete($_ads->id);

        if ($ads) {
            return true;
        }
        return false;
    }
}