<div class="sidebar">
    <div class="testing">
        <div class="side-header">
            <button id="close-btn"><i class="fa fa-bars fa-lg material-icons" aria-hidden="true"></i></button>
            <h1>Ndsp</h1>
        </div>
        <li><a href="#"><i class="fa fa-book " aria-hidden="true"></i> Books</a></li>
        <li>
            <ul class="collapsible collapsible-dropdown" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header"><i class="fa fa-sliders" aria-hidden="true"></i> &nbsp;&nbsp; Setup
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <i
                                class="fa fa-caret-down" aria-hidden="true"></i>
                    </div>
                    <ul class="collapsible-body">
                        <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edition</a></li>
                        <li><a href="#"><i class="fa fa-list-ol" aria-hidden="true"></i> Genre</a></li>
                        <li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Category</a></li>
                        <li><a href="#"><i class="fa fa-user-circle" aria-hidden="true"></i> Author</a></li>
                        <li class="selected"><a href="#"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                Publisher</a></li>
                    </ul>
                </li>

            </ul>
        </li>
        <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Ebooks</a></li>
        <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Orders</a></li>
        <li><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Users</a></li>

        <ul id='sidebar_dropdown' class='dropdown-content'>
            <li><a href="#!">Genre</a></li>
            <li><a href="#!">Edition</a></li>
            <li><a href="#!">Category</a></li>
        </ul>
    </div>

</div>