<?php


namespace App\Actions\Author;

class GetAuthors extends BaseAuthorAction
{
    public $_page = 20;

    protected function onValidationSuccess()
    {
        $authors = $this->repository->paginate($this->_page);
        return $authors;
    }
}