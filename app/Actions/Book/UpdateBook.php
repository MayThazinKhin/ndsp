<?php

namespace App\Actions\Book;


use App\Actions\Stock\CreateStock;
use App\Actions\Stock\UpdateStock;
use App\FileSystem\Images\BookImage;
use App\Repositories\BookRepository;
use App\Repositories\StockRepository;
use Illuminate\Support\Collection;


class UpdateBook extends BaseBookAction
{
    protected $rules = [
        'id' => 'required',
        'edition_id' => 'required',
        'publisher_id' => 'required',
        'bookTitle' => 'required',
        'bookSalePrice' => 'required'

    ];

    public function __construct(BookRepository $repository, Collection $data = null)
    {
        parent::__construct($repository, $data);

    }

    protected function onValidationSuccess()
    {
        $input = $this->data()->except('_token', 'edition', 'publisher');

        $oldBook = $this->repository->getBy($input['id']);


        if (isset($input['coverInfo'])) {
            $bookImage = new BookImage($input['bookCoverImgUrl']);
            $cover_name = $bookImage->base64StringToImage($input['coverInfo'], $input['bookCoverImgUrl']);
            $input['bookCoverImgUrl'] = $cover_name;

            $bookImage = new BookImage($oldBook['bookCoverImgUrl']);
            $bookImage->delete();

        } else {

            $input['bookCoverImgUrl'] = $oldBook->bookCoverImgUrl;
        }
        $book = $oldBook->update($input->toArray());
        if ($book) {
            //delete related authors from author_book

            $oldBook->authors()->detach();
            $oldBook->authors()->attach($input['authors']);

            //delete related categories from author_book
            $oldBook->categories()->detach();
            $oldBook->categories()->attach($input['categories']);

            //delete related categories from author_book
            $oldBook->genres()->detach();
            $oldBook->genres()->attach($input['genres']);


            //current book not exists in stock table need to update
//            $stock = new \stdClass();
//            $stock->book_id = $oldBook['id'];
//            $stock->buy_or_consigment = $input['isConsigment'];
//            if($input['stock_id']){
//                $stock->id = $input['stock_id'];
//                $action = new UpdateStock($this->stockRepo, collect($stock));
//                $action->invoke();
//            }else{
//                $action = new CreateStock($this->stockRepo, collect($stock));
//                $action->invoke();
//            }

            return true;
        }
        return false;
    }
}