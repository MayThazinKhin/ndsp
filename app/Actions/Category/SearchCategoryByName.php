<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 08/12/2017
 * Time: 11:06 AM
 */

namespace App\Actions\Category;


class SearchCategoryByName extends  BaseCategoryAction
{
    protected function onValidationSuccess()
    {
        $authors = $this->repository->findByName($this->data()['categoryName']);
        return $authors;
    }
}