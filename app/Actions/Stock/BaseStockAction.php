<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 12:48 PM
 */

namespace App\Actions\Stock;


use App\Actions\Action;
use App\Repositories\StockRepository;
use Illuminate\Support\Collection;

class BaseStockAction extends  Action
{
    protected $rules=[];
    public function __construct(StockRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected  function onValidationSuccess()
    {

    }
}