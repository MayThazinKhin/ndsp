@extends('news.master')
@section('title','Blog')

@section('content')



<div class="div_2 blogpage">
    <div>

        <div class="small2">
            <div>
                <h2>Categories</h2>
                <div class="h2_divider"></div>
                <a class="btn" href="{{route('news/category',['name'=>'Blog'])}}"><span class="glyphicon glyphicon-menu-right"></span> Blog</a>
                <a class="btn" href="{{route('news/category',['name'=>'Article'])}}"><span class="glyphicon glyphicon-menu-right"></span> Article</a>
                <a class="btn" href="{{route('news/category',['name'=>'Media'])}}"><span class="glyphicon glyphicon-menu-right"></span> Media</a>
                <a class="btn" href="{{route('news/category',['name'=>'News'])}}"><span class="glyphicon glyphicon-menu-right"></span> News</a>
            </div>
        </div>

        <div class="small1">
            @foreach($articles as $article)

               
            <div class="wow fadeInUp">
                <img src="{{URL::to('image/blog/'.$article->cover_photo)}}" alt="">
                <div>
                    <div>
                        <a>{{($article->created_at)->format('d M Y')}}</a>

                    </div>
                    <a href="{{route('news/detail',['id'=>$article->id])}}"><h2>{{$article->title}}</h2></a>
                    <p>{{ $article->getPost($article->content,500) }}</p>
                    <a href="{{route('news/detail',['id'=>$article->id])}}" class="btn">Read More <span class="glyphicon glyphicon-play"></span></a>
                </div>
            </div>
                @endforeach
<div style="text-align: center;">
                {{$articles->links()}}
                </div>
        </div>

    </div>
</div>






@endsection
