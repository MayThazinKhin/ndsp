<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 12:43 PM
 */

namespace App\Actions\Transaction;


class UpdateTransaction extends BaseTransactionAction
{
    protected $rules = [
        'id' => 'required',
        'book_id' => 'required',
        'total_cost_of_books'=>'required',
        'qty'=>'required',
        'created_at'=>'required'
    ];

    protected function onValidationSuccess()
    {

        $transaction = $this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($transaction) {
            return true;
        }
        return false;
    }
}