<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $i) {
            \App\Models\Author::create([
                'authorName' => $this->faker->name
            ]);
        }
    }
}
