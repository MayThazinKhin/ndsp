<?php

namespace App\Actions\Ads;


use App\Actions\Action;
use App\Repositories\AdsRepository;
use Illuminate\Support\Collection;

class BaseAdsAction extends Action
{
    protected $rules=[];

    public function __construct(AdsRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected  function onValidationSuccess(){}
}