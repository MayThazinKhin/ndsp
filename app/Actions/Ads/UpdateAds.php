<?php

namespace  App\Actions\Ads;

use App\FileSystem\Images\AdsImage;

class UpdateAds extends BaseAdsAction
{
    protected $rules = [
        'id' => 'required',

    ];

    protected function onValidationSuccess()
    {
        $input = $this->data()->except('_token');

        $oldAds = $this->repository->getBy($input['id']);

        if (isset($input['image'])) {
            //delete old image
            $adsImg = new AdsImage($oldAds['image']);
            $status = $adsImg->delete();

            //store new image
            $adsImg = new AdsImage($input['image']);
            $status = $adsImg->store();
            if (!$status) return false;

            $input['image'] = $adsImg->getStoredName();
        }

        $ads = $this->repository->update($input->toArray(),$input['id']);
        if ($ads) {
            return true;
        }
        return false;
    }
}