<?php

namespace App\Actions\Publisher;

class DeletePublisher  extends BasePublisherAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $publisher = $this->repository->delete($this->data()->toArray());
        if ($publisher) {
            return true;
        }
        return false;
    }
}