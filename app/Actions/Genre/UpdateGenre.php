<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:08 AM
 */

namespace App\Actions\Genre;


class UpdateGenre extends BaseGenreAction
{
    protected $rules = [
        'id'=>'required',
        'genreName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $author= $this->repository->update($this->data()->toArray(),$this->data()['id']);
        if ($author) {
            return true;
        }
        return false;
    }
}