<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 10:23 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByAuthorName extends  BaseBookAction
{
    public $pages = 20;

    protected $rules = [];

    protected function onValidationSuccess()
{
    return $this->repository->getBookByFilterName($this->data()['authorName'], $this->pages);
}
}