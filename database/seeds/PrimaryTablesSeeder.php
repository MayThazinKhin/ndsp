<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PrimaryTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $primary_tables = ['admins', 'authors', 'categories', 'editions', 'genres', 'publishers'];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        foreach ($primary_tables as $primary_table) {
            DB::table($primary_table)::truncate();
        }

        $this->call([
            AdminsTableSeeder::class,
            AuthorsTableSeeder::class,
            CategoriesTableSeeder::class,
            EditionsTableSeeder::class,
            GenresTableSeeder::class,
            PublishersTableSeeder::class
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
