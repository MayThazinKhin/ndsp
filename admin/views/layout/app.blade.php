<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>We Distribution | @yield('title')</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/vue-multiselect.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/admin/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/v-toaster.css')}}" charset="utf-8">

    <style>
        .pagination__navigation--disabled {
            opacity: .6;
            pointer-events: none;
        }

        .pagination__more {
            pointer-events: none;

        }

        .v-toaster .v-toast.v-toast-error {
            background: rgb(201, 36, 49);
        }

        .v-toaster .v-toast.v-toast-warning {
            background: #fdd835;
            border-color: #fdd835;
        }


    </style>
    @yield('style')
</head>
<body>


<div id="app">
    <div class="sidebar">
        <div class="testing">
            <div class="side-header">
                <button id="close-btn"><i class="fa fa-bars fa-lg material-icons" aria-hidden="true"></i></button>
                <h1>Ndsp</h1>
            </div>
            <li><a href="#"><i class="fa fa-book " aria-hidden="true"></i> Books</a></li>
            <li>
                <ul class="collapsible collapsible-dropdown" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="fa fa-sliders" aria-hidden="true"></i> &nbsp;&nbsp;
                            Setup &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <i
                                    class="fa fa-caret-down" aria-hidden="true"></i>
                        </div>
                        <ul class="collapsible-body">
                            <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edition</a></li>
                            <li><a href="#"><i class="fa fa-list-ol" aria-hidden="true"></i> Genre</a></li>
                            <li><a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Category</a></li>
                            <li><a href="#"><i class="fa fa-user-circle" aria-hidden="true"></i> Author</a></li>
                            <li class="selected"><a href="#"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    Publisher</a></li>
                        </ul>
                    </li>

                </ul>
            </li>
            <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Ebooks</a></li>
            <li><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Orders</a></li>
            <li><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Users</a></li>

            <ul id='sidebar_dropdown' class='dropdown-content'>
                <li><a href="#!">Genre</a></li>
                <li><a href="#!">Edition</a></li>
                <li><a href="#!">Category</a></li>
            </ul>
        </div>

    </div>
    <!-- SIDEBAR  -->
    <div class="sidebar">
        <div class="testing">
            <div class="side-header">
                <button id="close-btn"><i class="fa fa-bars fa-lg material-icons" aria-hidden="true"></i></button>
                <h1>Ndsp</h1>
            </div>
            <li class="@yield('admins')"><a href="{{route('admin.user.index')}}"><i class="fa fa-users"
                                                                                    aria-hidden="true"></i> User</a>
            </li>
            <li>
                <ul class="collapsible collapsible-dropdown" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header @yield('setup')"><i class="fa fa-sliders" aria-hidden="true"></i>
                            &nbsp;&nbsp; Setup &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            &nbsp; <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </div>
                        <ul class="collapsible-body">
                            <li><a href="{{route('admin.edition.index')}}"><i class="fa fa-chevron-right"
                                                                              aria-hidden="true"></i> Edition</a></li>
                            <li><a href="{{route('admin.genre.index')}}"><i class="fa fa-chevron-right"
                                                                            aria-hidden="true"></i> Genre</a></li>
                            <li><a href="{{route('admin.category.index')}}"><i class="fa fa-chevron-right"
                                                                               aria-hidden="true"></i> Category</a></li>
                            <li><a href="{{route('admin.author.index')}}"><i class="fa fa-chevron-right"
                                                                             aria-hidden="true"></i> Author</a></li>
                            <li><a href="{{route('admin.publisher.index')}}"><i class="fa fa-chevron-right"
                                                                                aria-hidden="true"></i> Publisher</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <li class="@yield('books')"><a href="{{route('admin.book.index')}}"><i class="fa fa-book "
                                                                                   aria-hidden="true"></i> Book</a></li>
            <li class="@yield('ebooks')"><a href="{{route('admin.ebook.index')}}"><i class="fa fa-file-pdf-o"
                                                                                     aria-hidden="true"></i> Ebook</a>
            </li>
            <li class="@yield('purchase')"><a href="{{route('admin.purchase.index')}}"><i class="fa fa-cart-arrow-down"
                                                                                          aria-hidden="true"></i>
                    Purchase</a></li>
            <li class="@yield('comments')"><a href="{{route('admin.comment.index')}}"><i class="fa fa-book "
                                                                                         aria-hidden="true"></i>
                    Comments</a></li>
            <li class="@yield('order')"><a href="{{route('admin.order.index')}}"><i class="fa fa-cart-plus"
                                                                                    aria-hidden="true"></i> Orders</a>
            </li>
            <li class="@yield('article')"><a href="{{route('admin.article.index')}}"><i class="fa fa-cart-arrow-down"
                                                                                        aria-hidden="true"></i> Article</a>
            </li>
            <li class="@yield('delivery_report')"><a href="{{ route('admin.order.show_delivery_report') }}"><i
                            class="fa fa-truck"
                            aria-hidden="true"
                    ></i>Delivery Report</a>

            </li>
        </div>
    </div>

    <div class="content">

        @yield('content')
    </div>
</div>
<script type="text/javascript" src="{{URL::asset('js/jquery-3.2.1.min.js')}}"></script>
{{--<script type="text/javascript" src="{{URL::asset('js/admin/manifest.js')}}"></script>--}}
<script type="text/javascript" src="{{ URL::asset('js/manifest.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/vendor.js') }}"></script>
<script type="text/javascript" src="{{URL::asset('js/app.js')}}"></script>

<script type="text/javascript" src="{{URL::asset('js/ui-app.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/materialize.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/admin/tinymce/jquery.tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/admin/tinymce/tinymce.min.js')}}"></script>

@yield('script')
</body>
</html>
