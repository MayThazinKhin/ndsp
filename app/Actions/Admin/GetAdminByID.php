<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 14/11/2017
 * Time: 8:59 AM
 */

namespace App\Actions\Admin;


class GetAdminByID extends BaseAdminAction
{
    protected function onValidationSuccess()
    {
        $admin = $this->repository->getBy($this->data()['id']);
        return $admin;
    }
}