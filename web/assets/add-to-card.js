import Vue from 'vue';
import axios from 'axios';
import VueStarRating from 'vue-star-rating';
import VeeValidate from 'vee-validate';
import moment from 'moment';


Vue.use(VeeValidate);
Vue.component('star-rating', VueStarRating);

new Vue({
  el: '#addtocard',

  data: {

    book:{
      id: null,
      bookTitle: '',
      bookCoverImgUrl: '',
      coverInfo: null,
      bookSalePrice: 0,
      bookDescription: '',
      author: [],
      publisher:{id:null,publisherName:''},
      edition:{id:null,editionName:''},
      categorie: [],
      genres: [],
      edition_id: null,
      publisher_id: null,
      total_cost_of_books: 0,
      qty: 0,
      isEbook: false,
      rating:0,
      comments:[]

    },
    relatedbooks:[],
    topbooks:[],
    comments:[],
    total:0,
    qty:1,

    comment:{
      book_id:null,
      username:null,
      comment:null,
      rating:0,
      approve:true
    },
    isdisabled:false
  },
  methods : {
    formatNumber(number){
      return parseInt( number ).toLocaleString();
    },
    formatDate(date){
       return moment(date).format("YYYY-MM-DD");
    },

    convertoInt(val){
      return Number(val);
    },

    getUrlParameter(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    subQty(){
      if(this.qty>0){
      this.qty=this.qty-1;  
      }
      
    },
    addQty(){
      this.qty=this.qty+1;
    },
    getdetail(){
      axios.get('/book/getdetail/'+this.book.id).then(response=>{
        this.book=response.data.book;
        this.relatedbooks=response.data.relatedbooks;
        this.topbooks=response.data.topbooks;
        this.comments=response.data.comments;
      });
    },
    getUrlParam(){
      let book_id = this.getUrlParameter('book_id');
      if (book_id != null) {
        this.book.id=book_id;
        this.getdetail();

      }

    },

    submitdata(){

      this.comment.book_id=this.book.id;

      var config = {
        headers: {'X-My-Custom-Header': 'Header-Value'}
      }
      this.$validator.validateAll().then(successsValidate => {
        if (successsValidate) {
          axios.post('/comment/upload',this.comment,config).then(response=> {

            $('#commentModal').modal('hide');
            this.getdetail();
          })
            .catch(error=>{

            });
        }
      }).catch(error => {
        this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
      });

    },
    addToStorage () {
      var temp = Object.assign({}, this.book);
      var a=[];
      var localStoreItems=localStorage.getItem('cart_books');

      if(localStoreItems){
        a=(JSON.parse(localStoreItems));
        a.push({book_id:temp.id,bookTitle:temp.bookTitle,price:temp.bookSalePrice,qty:this.qty,total:temp.bookSalePrice*this.qty});
        localStorage.setItem('cart_books', JSON.stringify(a));

      }else{

        a.push({book_id:temp.id,bookTitle:temp.bookTitle,price:temp.bookSalePrice,qty:this.qty,total:temp.bookSalePrice*this.qty});
        localStorage.setItem('cart_books', JSON.stringify(a));
      }

    },
  },
  mounted(){
    this.getUrlParam();
  }
});