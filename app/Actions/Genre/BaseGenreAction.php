<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 13/12/2017
 * Time: 3:10 PM
 */
namespace App\Actions\Genre;

use App\Actions\Action;
use App\Repositories\GenreRepository;
use Illuminate\Support\Collection;

abstract  class BaseGenreAction extends  Action
{
    protected $rules = [];

    public function __construct(GenreRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }
}