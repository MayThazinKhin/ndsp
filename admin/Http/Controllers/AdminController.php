<?php

namespace Admin\Http\Controllers;

use App\Actions\Admin\AdminChangePassword;
use App\Actions\Admin\CreateAdmin;
use App\Actions\Admin\DeleteAdmin;
use App\Actions\Admin\GetAdminByID;
use App\Actions\Admin\GetAdmins;
use App\Http\Controllers\ControllerTrait;
use App\Repositories\AdminRepository;
use Illuminate\Cache\Repository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class AdminController extends Controller
{
    use ControllerTrait;

    public function __construct(AdminRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('admin.index');
    }

    public function create(Request $request)
    {
        $action = new CreateAdmin($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function update(Request $request)
    {
        $action = new AdminChangePassword($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function delete($username)
    {
        $request = ['username' => $username];
        $action = new DeleteAdmin($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    function getAll()
    {
        $action = new GetAdmins($this->repository);
        $admins = $action->invoke();
        return response()->json($admins);
    }

    function getByID($id)
    {
        $request = ['id' => $id];
        $action = new GetAdminByID($this->repository, collect($request));
        $admin = $action->invoke();
        return response()->json($admin);
    }
}