<?php


namespace App\Actions\Publisher;


class GetPublishers  extends BasePublisherAction
{
    public $_page = 20;

    protected function onValidationSuccess()
    {
        $publishers = $this->repository->paginate($this->_page);
        return $publishers;
    }
}