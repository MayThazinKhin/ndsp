<?php
/**
 * Created by PhpStorm.
 * User: yanyan
 * Date: 11/11/2017
 * Time: 2:06 AM
 */

namespace App\Actions\Ebook;


class GetRecentEbooks extends BaseEbookAction
{
    public function onValidationSuccess()
    {
        $books = $this->repository->getLastEBooks($this->data()['page']);
        return $books;
    }
}