<?php


namespace  App\Actions\Comment;

use App\Actions\Action;
use App\Repositories\CommentRepository;
use Illuminate\Support\Collection;

class BaseCommentAction extends  Action
{

    protected $rules = [];

    public function __construct(CommentRepository $repository, Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }


    protected function onValidationSuccess(){}
}