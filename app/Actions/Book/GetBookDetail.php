<?php

namespace App\Actions\Book;
use App\Repositories\BookRepository;
use App\Repositories\CommentRepository;
use Illuminate\Support\Collection;

class GetBookDetail extends BaseBookAction
{
    private $commentRepository;
    public function __construct(BookRepository $repository,CommentRepository $commentRepository,Collection $data = null)
    {
        parent::__construct($repository, $data);
        $this->commentRepository=$commentRepository;
    }


    protected function onValidationSuccess()
    {
        $book = $this->repository->getBookByID($this->data()['id']);

        $comments = $this->commentRepository->getFiveComments($this->data()['id']);
        return ['book' => $book, 'comments'=>$comments];
    }
}