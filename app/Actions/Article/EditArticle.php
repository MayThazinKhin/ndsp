<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/02/2018
 * Time: 8:47 AM
 */

namespace App\Actions\Article;


use App\FileSystem\Images\ArticleCoverPhoto;
use App\Repositories\ArticleRepository;
use Illuminate\Support\Collection;
class EditArticle extends BaseArticleAction
{
    protected $rules = [
//        'id' => 'required',
//        'article_type_id' => 'required',
//        'title' => 'required|max:500',
//        'content' => 'required'
    ];
    private $_req;
        public function __construct(ArticleRepository $repository, Collection $data = null,$req)
        {
            parent::__construct($repository, $data);
            $this->_req=$req;
        }

    protected function onValidationSuccess()
    {
        $input = $this->data();

        $oldBook = $this->repository->getBy($input['id']);
        if ($this->_req->hasFile('cover_photo')) {

            $bookImage = new ArticleCoverPhoto($this->_req['cover_photo']);

            $cover_name = $bookImage->store();
            if (!$cover_name) return false;
            $covername=$bookImage->getStoredName();

            $input['cover_photo'] = $covername;


            if($oldBook['cover_photo']){
                $bookImage = new ArticleCoverPhoto($oldBook['cover_photo']);
                $bookImage->delete();
            }

        } else {

            $input['cover_photo'] = $oldBook->cover_photo;
        }

        $book = $oldBook->update($input->toArray());

        if ($book) {
            return true;
        }
        return false;
    }
}