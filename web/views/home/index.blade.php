@extends('layout.master')
@section('title','Home')

@section('home','active')
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.9/flickity.min.css">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/detail.css')}}">
    @endsection
@section('content')
    <div class="container">

        <section id="home">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="slide-1-heading myanmar-text">
                                <h1>အိမ်တိုင်ရာရောက်</h1>
                                <h4>စာအုပ်တစ်အုပ်ဝယ်ဖို့ ကားကျပ်လူကျပ်</h4>
                                <h4>ဒဏ်တွေ ခံစရာမလိုတော့ဘူး</h4>
                                <button class="btn btn-full">Book</button>
                                <button class="btn btn-ghost">Book</button>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <img src="{{URL::asset('images/slider.png')}}" >
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="slide-1-heading myanmar-text">
                                <h1>အိမ်တိုင်ရာရောက်</h1>
                                <h4>စာအုပ်တစ်အုပ်ဝယ်ဖို့ ကားကျပ်လူကျပ်</h4>
                                <h4>ဒဏ်တွေ ခံစရာမလိုတော့ဘူး</h4>
                                <button class="btn btn-full">Book</button>
                                <button class="btn btn-ghost">Book</button>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <img src="{{URL::asset('images/slider.png')}}" >
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="slide-1-heading myanmar-text">
                                <h1>အိမ်တိုင်ရာရောက်</h1>
                                <h4>စာအုပ်တစ်အုပ်ဝယ်ဖို့ ကားကျပ်လူကျပ်</h4>
                                <h4>ဒဏ်တွေ ခံစရာမလိုတော့ဘူး</h4>
                                <button class="btn btn-full">Book</button>
                                <button class="btn btn-ghost">Book</button>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <img src="{{URL::asset('images/slider.png')}}" >
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>

    <section id="section-2">
        <h1>HOT BOOKS</h1>
        <div class="main-carousel">

            @foreach($hot_books as $book)
                <div class="carousel-cell">
                    <a href="{{URL::to('book/detail?book_id='.$book->id)}}">
                        <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                        <div class="book-detail">
                            <h4>{{$book->bookTitle}}</h4>
                            <p>{{$book->author}}</p>
                            <p>{{number_format($book->bookSalePrice)}}</p>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
    </section>


    <section id="section-3">
        <hr>

        <div class="row section-3-row">

            <div class="div">
                <div class="col-sm-2 left-bar">
                    <div class="search-box">
                        <form action="{{route('search-all')}}" method="get">
                            <input type="text" class="form-control"  placeholder="Search" id="srch-term" name="paramname" >
                            <button id="srch-but" type="submit">

                            </button>
                        </form>
                    </div>


                    <h1 class="heading">BROWSE BOOKS</h1>
                    <h2 class="category-header">Category</h2>
                    <ul>
                        @foreach($categories as $category)
                            <li><a href="{{route('search/bycategory',['id'=> $category->id])}}">{{$category->categoryName}}</a></li>
                        @endforeach
                        <li><a href="{{route('ebooks')}}">Ebook</a></li>
                    </ul>
                </div> <!-- left side menu end -->
            </div>

            <div class="col-sm-10 right-bar">

                <div class="row" style="padding: 0px; margin: 0px;">

                        <h1 class="new-products-text">NEW PRODUCTS</h1>


                </div>
                @foreach($recent_books as $book)
                <div class="book-grid">
                    <div class="product-img">
                        <a href="{{URL::to('book/detail?book_id='.$book->id)}}">
                            <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                        </a>
                    </div>
                    <div class="book-des">
                        <h4 class="product-name">
                            <a class="pull-left" data-toggle="tooltip" title="Book Title With very very very long text" href="{{URL::to('book/detail?book_id='.$book->id)}}">{{$book->bookTitle}}</a>
                        </h4>
                        <p class="author">{{$book->author}}</p>
                        <p class="product-price">{{number_format($book->bookSalePrice)}}</p>
                    </div>

                </div>
                @endforeach
            </div>
        </div>
    </section>

</div>
@endsection
