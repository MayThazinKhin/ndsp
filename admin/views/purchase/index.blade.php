@extends('layout.app')
@section('title','Purchase')
@section('purchase','active')

@section('content')
    <purchase-list inline-template>
        <div>
            <h3>Purchase List</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <button class="modal-trigger waves-effect waves-light btn" data-target="stock_modal" @click="toggleAddModal"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Stock</button>

                        </div>
                    </div>
                    <div id="stock_modal" class="modal">
                        <form @submit.prevent="submitdata" autocomplete="off">
                        <div class="modal-content">
                            <h4>Add Stock</h4>

                            <div class="date-div"><datepicker v-model="transaction.created_at"></datepicker></div>
                            <multiselect class="customstyle"
                                         placeholder="Book"
                                         v-model="selectedValues"
                                         label="bookTitle"
                                         name="book"
                                         track-by="bookTitle"
                                         :options="collections"
                                         :multiple="false"
                                         :searchable="true"
                                         :internal-search="false"
                                         :taggable="true"
                                         @search-change="asyncFind"
                                         @input="onValueChange" data-vv-name="book" v-validate="'required'">
                            </multiselect>
                            {{--<book-select :multi="false" name="book" :value="selectedBook"  @input="onValueChange" data-vv-name="book" v-validate="'required'"></book-select>--}}
                            <div id="cemail-error" class="error" ><span v-if="errors.has('book')">Required book name.</span></div>
                            <input type="number" name="total_cost_of_books" placeholder="Total Cost" v-model="transaction.total_cost_of_books" v-validate="'required'">
                            <div id="cemail-error" class="error" ><span v-if="errors.has('total_cost_of_books')">Required total cost.</span></div>
                            <input type="number" name="qty" placeholder="Qty" v-model="transaction.qty" v-validate="'required'">
                            <div id="cemail-error" class="error" ><span v-if="errors.has('qty')">Required Qty.</span></div>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                            <button type="submit" class="modal-action waves-effect waves-green btn-flat add-btn">Add</button>
                        </div>
                        </form>
                    </div>

                    <table class="highlight striped data-table">
                        <thead>
                        <tr>

                            <th>No.</th>
                            <th>Date</th>
                            <th>BookName</th>
                            <th>Qty</th>

                            <th>Total Cost</th>

                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="trans,index in transactions">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{formatDate(trans.created_at)}}</td>
                            <td>@{{trans.book.bookTitle}}</td>
                            <td>@{{trans.qty}}</td>
                            <td>@{{trans.total_cost_of_books}}</td>
                            <td>
                                <button  data-target="stock_modal" class="modal-trigger btn-floating" @click="toggleEdit(trans)"><i class="material-icons fa fa-pencil-square-o "></i></button>
                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(trans.id)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div><!-- card-content end -->
            </div>
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </purchase-list>
@endsection