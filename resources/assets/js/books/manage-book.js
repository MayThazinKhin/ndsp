import Helper from '../core/Helper';
import AuthorSelect from '../core/AuthorSelect';
import CategorySelect from '../core/CategorySelect';
import GenreSelect from '../core/GenreSelect';
import PublisherSelect from '../core/PublisherSelect';
import EditionSelect from '../core/EditionSelect';

let boolListUrl="/admin/book";
export default {
    components: {
        AuthorSelect, CategorySelect, GenreSelect, PublisherSelect, EditionSelect
    },
    data: function () {
        return {
            isEdit: false,

            selectedAuthors: [],
            selectedGenres: [],
            selectedCategories: [],
            selectedPublisher: [],
            selectedEdition: [],

            book: {
                id: null,
                bookTitle: '',
                bookCoverImgUrl: '',
                coverInfo: null,
                bookSalePrice: 0,
                bookDescription: '',
                authors: [],
                categories: [],
                genres: [],
                edition_id: null,
                publisher_id: null,
                total_cost_of_books: 0,
                qty: 0,
                isEbook: false,
                e_book_download: null,
                stock_id: null,
              buy_or_consigment: false
            }
        }
    },
    methods: {
        authorChange(value) {
            this.book.authors = value;
        },
        categoryChange(value) {
            this.book.categories = value;
        },
        genreChange(value) {
            this.book.genres = value;
        },
        publisherChange(value) {
            this.book.publisher_id = value.id;
        },
        editionChange(value) {
            this.book.edition_id = value.id;
        },
        getBookDetail(id) {
            axios.get('/admin/book/' + id + '/' + 'detail').then(response => {
                this.book = response.data.book;
                this.selectedAuthors = response.data.book.authors;
                this.selectedPublisher = response.data.book.publisher;
                this.selectedEdition = response.data.book.edition;
                this.selectedCategories = response.data.book.categories;
                this.selectedGenres = response.data.book.genres;
                this.book.bookCoverImgUrl = response.data.coverImage;
                // this.book.stock_id = response.data.book.stocks.length > 0 ? response.data.book.stocks[0]['id'] : null;
                // this.book.isConsigment = response.data.book.stocks.length > 0 ? response.data.book.stocks[0]['buy_or_consigment'] : false;
            }).catch(error =>{
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        asyncFindGenre(query) {
            if (query) {
                axios.get('/admin/genre/' + query + '/findbyname').then(response => {
                    this.genre_collections = response.data;
                });
            }
        },
        onFileChange(e) {
            this.book.coverInfo = e.target.value;
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;
            this.createImage(files[0]);
        },
        createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;
            reader.onload = (e) => {
                vm.book.bookCoverImgUrl = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    this.performProcess(this.isEdit ? 'edit' : 'create');
                } else {
                    this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },

        performProcess(performtype) {
            axios.post('/admin/book/' + performtype, this.book).then(response => {
                this.$parent.showToast(this.isEdit ? 'Updated' : 'Inserted' + ' successfully.', 'success');
              window.location.href = boolListUrl;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        checkUrlParam() {
            let book_id = Helper.getUrlParameter('book_id');
            if (book_id != null) {
                this.isEdit = true;
                this.getBookDetail(book_id);
            }
        }
    },
    mounted() {
        this.checkUrlParam();
    }
}