<?php

namespace App\Http\Controllers\Api;

use App\Actions\Order\CreateOrder;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;


class OrderController extends Controller
{
    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(Request $request)
    {
        $_order = json_decode($request['order'], true);
        $action = new CreateOrder($this->repository, collect($_order));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json(['success' => 'false']);
        }
        if (!$result) {

            return response()->json(['success' => 'false']);
        }
        return response()->json(['success' => 'true']);
    }
}