webpackJsonp([2],{

/***/ 141:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(142);


/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_star_rating__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vue_star_rating___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_vue_star_rating__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vee_validate__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);






__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_3_vee_validate__["default"]);
__WEBPACK_IMPORTED_MODULE_0_vue___default.a.component('star-rating', __WEBPACK_IMPORTED_MODULE_2_vue_star_rating___default.a);

new __WEBPACK_IMPORTED_MODULE_0_vue___default.a({
  el: '#addtocard',

  data: {

    book: {
      id: null,
      bookTitle: '',
      bookCoverImgUrl: '',
      coverInfo: null,
      bookSalePrice: 0,
      bookDescription: '',
      author: [],
      publisher: { id: null, publisherName: '' },
      edition: { id: null, editionName: '' },
      categorie: [],
      genres: [],
      edition_id: null,
      publisher_id: null,
      total_cost_of_books: 0,
      qty: 0,
      isEbook: false,
      rating: 0,
      comments: []

    },
    relatedbooks: [],
    topbooks: [],
    comments: [],
    total: 0,
    qty: 1,

    comment: {
      book_id: null,
      username: null,
      comment: null,
      rating: 0,
      approve: true
    },
    isdisabled: false
  },
  methods: {
    formatNumber: function formatNumber(number) {
      return parseInt(number).toLocaleString();
    },
    formatDate: function formatDate(date) {
      return __WEBPACK_IMPORTED_MODULE_4_moment___default()(date).format("YYYY-MM-DD");
    },
    convertoInt: function convertoInt(val) {
      return Number(val);
    },
    getUrlParameter: function getUrlParameter(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    subQty: function subQty() {
      if (this.qty > 0) {
        this.qty = this.qty - 1;
      }
    },
    addQty: function addQty() {
      this.qty = this.qty + 1;
    },
    getdetail: function getdetail() {
      var _this = this;

      __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/book/getdetail/' + this.book.id).then(function (response) {
        _this.book = response.data.book;
        _this.relatedbooks = response.data.relatedbooks;
        _this.topbooks = response.data.topbooks;
        _this.comments = response.data.comments;
      });
    },
    getUrlParam: function getUrlParam() {
      var book_id = this.getUrlParameter('book_id');
      if (book_id != null) {
        this.book.id = book_id;
        this.getdetail();
      }
    },
    submitdata: function submitdata() {
      var _this2 = this;

      this.comment.book_id = this.book.id;

      var config = {
        headers: { 'X-My-Custom-Header': 'Header-Value' }
      };
      this.$validator.validateAll().then(function (successsValidate) {
        if (successsValidate) {
          __WEBPACK_IMPORTED_MODULE_1_axios___default.a.post('/comment/upload', _this2.comment, config).then(function (response) {

            $('#commentModal').modal('hide');
            _this2.getdetail();
          }).catch(function (error) {});
        }
      }).catch(function (error) {
        _this2.$parent.showToast('Opps,Something has gone wrong.', 'warn');
      });
    },
    addToStorage: function addToStorage() {
      var temp = Object.assign({}, this.book);
      var a = [];
      var localStoreItems = localStorage.getItem('cart_books');

      if (localStoreItems) {
        a = JSON.parse(localStoreItems);
        a.push({ book_id: temp.id, bookTitle: temp.bookTitle, price: temp.bookSalePrice, qty: this.qty, total: temp.bookSalePrice * this.qty });
        localStorage.setItem('cart_books', JSON.stringify(a));
      } else {

        a.push({ book_id: temp.id, bookTitle: temp.bookTitle, price: temp.bookSalePrice, qty: this.qty, total: temp.bookSalePrice * this.qty });
        localStorage.setItem('cart_books', JSON.stringify(a));
      }
    }
  },
  mounted: function mounted() {
    this.getUrlParam();
  }
});

/***/ })

},[141]);