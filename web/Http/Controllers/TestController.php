<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 14/11/2017
 * Time: 10:50 AM
 */

namespace Web\Http\Controllers;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function detail(){
        return view('book.detail.index');
    }
}