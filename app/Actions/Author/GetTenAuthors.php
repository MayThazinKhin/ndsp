<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 11/01/2018
 * Time: 1:47 PM
 */

namespace App\Actions\Author;


class GetTenAuthors extends BaseAuthorAction
{

    public function onValidationSuccess()
    {
        $categories = $this->repository->getLimitColumn(10);
        return $categories;
    }
}