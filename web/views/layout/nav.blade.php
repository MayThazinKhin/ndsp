<nav class="my-nav">
    <div class="nav-container" id="noticard">
        <a href="#"><img alt="Brand" src="{{URL::asset('images/logo-2.png')}}" class="logo"></a>
        <ul class="nav-items">
            <li><a href="{{route('home')}}" id="nav-home">HOME</a></li>
            <li class="active"><a href="{{route('search')}}">BOOKS</a></li>
            <li class="active"><a href="{{route('search/by-category-name',['name'=>'Music'])}}">Music</a></li>
            <li><a href="{{route('news')}}">NEWS</a></li>
            <li class="pull-right">
                <a href="{{route('order')}}">
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </li>
        </ul>

        <div class="toggle-btn" onclick="toggleSidebar()">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</nav>
<div id="sidebar">
    <ul>
        <li class="@yield('home')"><a href="{{route('home')}}" id="nav-home">HOME</a></li>
        <li class="@yield('book')"><a href="{{route('search')}}">BOOKS</a></li>
        <li class="@yield('book')"><a href="{{route('search/by-category-name',['name'=>'Music'])}}">Music</a></li>
        <li><a href="{{route('news')}}">NEWS</a></li>

        <li>
            <a href="{{route('order')}}">
            <i class="fa fa-shopping-cart"></i>
            </a>
        </li>

    </ul>
</div>