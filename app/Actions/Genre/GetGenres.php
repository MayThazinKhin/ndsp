<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/01/2018
 * Time: 10:20 AM
 */

namespace App\Actions\Genre;


class GetGenres extends BaseGenreAction
{
    public $_page = 20;

    protected function onValidationSuccess()
    {
        $genres = $this->repository->paginate($this->_page);
        return $genres;
    }
}