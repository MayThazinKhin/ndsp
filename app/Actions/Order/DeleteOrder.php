<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 06/11/2017
 * Time: 2:35 PM
 */

namespace App\Actions\Order;


class DeleteOrder extends BaseOrderAction
{
    protected $rules=['email'=>'required','phone'=>'required','address'=>'required','otw_or_deli_or_return'=>'required'];
    protected function onValidationSuccess()
    {
        $order= $this->repository->destory($this->data()['email'],$this->data()['phone'],$this->data()['address'],$this->data()['otw_or_deli_or_return']);
        if($order){
            return true;
        }
        return false;
    }
}