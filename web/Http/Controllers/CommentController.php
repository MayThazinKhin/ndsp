<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 26/02/2018
 * Time: 2:40 PM
 */

namespace Web\Http\Controllers;


use App\Actions\Comment\CreateComment;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    private $repository;

    public function __construct(CommentRepository $repository)
    {

        $this->repository = $repository;

    }

    public function create(Request $request){
        $action = new CreateComment($this->repository,collect($request->all()));

        $result = $action->invoke();
        if (!$result) {
            return response()->json(['success'=>'false']);
        }
        return response()->json(['success'=>'true']);
    }
}