@extends('layout.app')
@section('title','Articles')
@section('article','selected')
@section('content')
<div class="content-down">
    <div class="card-content">
    <form method="post" action="{{route('admin.article.create')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="content-upp">
            <div class="content-up-left">
                <select class="filter-box" name="category">
                    <option value="Blog">Blog</option>
                    <option value="Article">Article</option>
                    <option value="Media">Media</option>
                    <option value="News">News</option>
                </select>
            </div>
            <div class="content-up-left">
                <input type="file" name="cover_photo">
            </div>
            <div class="content-up-right">
                <button class="btn btn-save">Save</button>
            </div>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Add Title" class="form-control" name="title" value="{{old('title')}}"/>
        </div>
        <div class="form-group">
                  <textarea name="content">
                        {{old('content')}}
                </textarea>
        </div>

    </form>
    </div>

</div>
@endsection
@section('script')
    <script>
      tinymce.init({
        selector: 'textarea',
        height: 500,
        menubar: false,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor textcolor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        content_css: [
          '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
          '//www.tinymce.com/css/codepen.min.css']
      });
    </script>
@endsection