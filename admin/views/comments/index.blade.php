@extends('layout.app')
@section('title','Comment List')
@section('comments','selected')

@section('content')
    <comment-list inline-template>
    <div>
    <h3>Comment List</h3>

    <div class="content-down">

        <div class="card-content">
            <div class="content-up">



            </div>
            <delete-modal @input="performdelete"></delete-modal>

            <table class="highlight striped data-table" >
                <thead>
                <tr>

                    <th>No.</th>
                    <th>Username</th>
                    <th>Comment</th>

                    <th></th>
                </tr>
                </thead>

                <tbody>
                <tr v-for="comment,index in comments">
                    <td>@{{pagination.from+index}}</td>
                    <td>@{{ comment.username }}</td>
                    <td>@{{ comment.comment }}</td>
                    <td>
                        <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(comment.id)"><i class="material-icons fa fa-trash-o"></i></button>

                    </td>
                </tr>
                </tbody>
            </table>
        </div><!-- card-content end -->

    </div><!-- content-down end -->

        <div class="paginations">
            <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="getComments"></vue-pagination>
        </div>
    </div>
    </comment-list>
@endsection