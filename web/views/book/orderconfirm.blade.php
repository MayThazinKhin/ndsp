@extends('layout.master')
@section('title','Book Detail')
@section('content')
    <div class="container">
        <section id="order_form">


            <div class="row" id="orderconfirm">
                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="edit-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Qty.</h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="form-control" id="modal-name" placeholder="Qty" v-model="qty">
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-primary" @click="editQty">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="top" style="padding-top: 20px;">
                    <div class="col-md-3">
                        <div class="well well-left-1">
                            <ul>
                                <li id="scroll"><a href="#order_form">User Information</a></li>
                                <li id="scroll"><a href="#bottom" id="order_border">Order Information</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="well well-right col-md-9" id="user">
                        <form action="" method="">
                            <legend>User Information</legend>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" v-model="email"
                                       v-validate="'required'" name="email">
                                <div style="color: red;" v-show="errors.has('email')">Required username.</div>

                            </div>
                            <div class="form-group">
                                <label for="phone">Ph-no</label>
                                <input type="text" class="form-control" placeholder="Phone-number" v-model="phone"
                                       v-validate="'required'" name="phone">
                                <div style="color: red;" v-show="errors.has('phone')">Required phone.</div>
                            </div>
                            <div class="form-group">

                                <label for="city">City</label>
                                <select
                                        v-model="city"
                                        @change="selectCity"
                                        name="city" class="form-control" v-validate="'required'">
                                    <option value=""> -- Select city --</option>
                                    <option :value="1">Mandalay</option>
                                    <option :value="2">Yangon</option>
                                    <option :value="3">Other</option>
                                </select>
                                <div style="color: red;" v-show="errors.has('city')">Required address.</div>
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control" id="address" rows="5" placeholder="Address"
                                          v-model="address" v-validate="'required'" name="address"></textarea>
                                <div style="color: red;" v-show="errors.has('address')">Required address.</div>
                            </div>
                            <div class="form-group" id="scroll">
                                <a href="#bottom" class="pull-right order_next_a_tag" id="next_btn">Next&nbsp;<i
                                            class="fa fa-angle-double-right"></i><i
                                            class="fa fa-angle-double-right"></i></a>
                            </div>

                        </form>

                    </div>

                </div>

                <hr style="padding-bottom:10px;">

                <div class="row">
                    <div class="col-md-9 col-md-offset-3 table_div">
                        <table class="table table-striped" id="bottom">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Book Name</th>
                                <th>Qty.</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="book,index in books">
                                <td>@{{ index+1 }}</td>
                                <td>@{{ book.bookTitle }}</td>
                                <td>@{{ book.qty }}</td>
                                <td>@{{ book.price }}</td>
                                <td>@{{ book.total }}</td>
                                <td>

                                    <a class="add-comment" data-toggle="modal" data-target="#edit-modal"
                                       @click="editClick(index,book.qty)"> <i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <a role="button" @click="remove(index)" type="button"><i
                                                class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            </tbody>

                        </table>
                        <h3>Total delivery fee: @{{ delivery_charge }} kyats</h3>
                    </div>
                </div>

                <div class="row" id="scroll">
                    <a href="#order_form" type="button" class="btn btn-danger scrolling"><i
                                class="fa fa-level-up fa-2x"></i></a>
                </div>
                <div class="row">
                    <button class="btn btn-full pull-right order_save_btn" @click="submitdata" :disabled="confirm">
                        Save
                    </button>
                </div>

            </div>
        </section>

    </div>
@endsection



@section('script')
    <script src="{{URL::asset('js/web/order.js')}}"></script>
    <script src="{{URL::asset('js/web/wow.min.js')}}"></script>
    <script>
        new WOW().init();
        $(document).ready(function () {
            $("#scroll a").click(function (bb) {
                bb.preventDefault();
                var hash = this.hash;
                $("html,body").animate({
                    scrollTop: $(hash).offset().top
                }, 'slow', function () {
                    window.location.hash = hash;
                });
            });
        });
        $(document).ready(function () {
            var offset = 250;
            var duration = 300;
            $(window).scroll(function () {
                if ($(this).scrollTop() > offset) {
                    $('.scrolling').fadeIn(duration);
                } else {
                    $('.scrolling').fadeOut(duration);
                }
            });
        });

    </script>

@endsection