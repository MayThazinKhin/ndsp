<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/02/2018
 * Time: 12:31 PM
 */

namespace App\Actions\Order;


class DeleteOrderOfBook extends  BaseOrderAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $edition = $this->repository->delete($this->data()->toArray());
        if ($edition) {
            return true;
        }
        return false;
    }
}