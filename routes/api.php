<?php


Route::name('category')->prefix('category')->group(function(){
    Route::name('all')->get('/all','Api\CategoryController@getAll');
    Route::name('show')->get('/show','Api\CategoryController@getTenCategory');
    Route::name('search')->get('/search/{name}','Api\CategoryController@searchCategory');
});

Route::name('publisher')->prefix('publisher')->group(function(){
    Route::name('all')->get('/all','Api\PublisherController@getAll');
    Route::name('search')->get('/search/{name}','Api\PublisherController@searchPublisher');
});

Route::name('author')->prefix('author')->group(function(){
    Route::name('all')->get('/all','Api\AuthorController@getAll');
    Route::name('search')->get('/search/{name}','Api\AuthorController@searchAuthor');
});

Route::name('genre')->prefix('genre')->group(function() {
    Route::name('all')->get('/all', 'Api\GenreController@getAll');
    Route::name('search')->get('/search/{name}','Api\GenreController@searchGenre');
});

Route::name('book')->prefix('book')->group(function(){
    Route::name('all')->get('/all','Api\BookController@all');
    Route::name('topten')->get('topten', 'Api\BookController@getTopTenBooks');
    Route::name('topall')->get('/topall','Api\BookController@getTopAllBooks');
    Route::name('recent')->get('/recent','Api\BookController@getRecentBooks');
    Route::name('detail')->get('/detail/{book_id}','Api\BookController@getBookDetail');
    Route::name('search')->get('/search/{name}','Api\BookController@searchBook');
    Route::name('by_music')->get('/by_music','Api\BookController@getBookByMusicCategory');
    Route::name('book')->get('/book/{name}','Api\SearchController@getBookByName');
    Route::name('by_category')->get('/by_category/{category_id}','Api\BookController@getBoooksByCategoryId');
    Route::name('by_author')->get('/by_author/{author_id}','Api\BookController@getBoooksByAuthorId');
    Route::name('by_publisher')->get('/by_publisher/{publisher_id}','Api\BookController@getBoooksByPublisherId');
    Route::name('by_genre')->get('/by_genre/{genre_id}','Api\BookController@getBooksByGenereId');
});

Route::name('search')->prefix('search')->group(function(){
    Route::name('book')->get('/book/{name}','Api\SearchController@getBookByName');
    Route::name('author')->get('/author/{name}','Api\SearchController@getByAuthorName');
    Route::name('category')->get('/category/{name}','Api\SearchController@getByCategoryName');
    Route::name('genre')->get('/genre/{name}','Api\SearchController@getByGenreName');
    Route::name('publisher')->get('/publisher/{name}','Api\SearchController@getByPublisherName');
});

Route::name('image')->prefix('image')->group(function (){
    Route::name('image')->get('/book/{name}','Api\BookController@getImage');
    Route::name('blog')->get('/blog/{name}','Api\BlogController@getImage');
});

Route::name('comment')->prefix('comment')->group(function (){
    Route::name('upload')->post('/upload','Api\CommentController@create');
    Route::name('all')->get('/all/{book_id}','Api\CommentController@getByBookID');
});

Route::name('ebook')->prefix('ebook')->group(function(){
    Route::name('all')->get('/all','Api\EbookController@getEBooks');
    Route::name('recent')->get('/recent','Api\EbookController@getRecentEBooks');
    Route::name('detail')->get('/detail/{book_id}','Api\EbookController@getEbookByID');

    Route::name('download')->get('/download/{name}','Api\EbookController@dowloadebook');
    Route::name('search')->get('/search/{name}','Api\EbookController@searchEBook');
    Route::name('by_name')->get('/by_name/{name}','Api\EbookController@getEbookByName');
    Route::name('by_category')->get('/by_category/{category_id}','Api\EbookController@getEbooksByCategoryId');
});

Route::name('article')->prefix('article')->group(function(){
    Route::name('all')->get('/all','Api\BlogController@getAll');
    Route::name('detail')->get('/detail/{id}','Api\BlogController@getByID');

});

Route::name('order')->post('/order','Api\OrderController@create');