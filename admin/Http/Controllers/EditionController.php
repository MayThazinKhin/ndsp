<?php


namespace Admin\Http\Controllers;

use App\Actions\Edition\CreateEdition;
use App\Actions\Edition\SearchByName;
use App\Actions\Edition\UpdateEdition;
use App\Actions\Edition\DeleteEdition;
use App\Actions\Edition\GetEditions;
use App\Actions\Edition\GetEditionByID;

use App\Http\Controllers\ControllerTrait;
use App\Repositories\EditionRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Http\Controllers\Controller;
class EditionController extends Controller
{
    use ControllerTrait;

    public function __construct(EditionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('edition.index');
    }

    public function create(Request $request)
    {
        $action = new CreateEdition($this->repository, collect($request->all()));

        $result = $action->invoke();

        if ($request instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $action = new UpdateEdition($this->repository, collect($request->all()));

        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return response()->json($result);
    }

    public function delete($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new DeleteEdition($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json($result->errors());
        }
        return response()->json($result);
    }

    public function getAll()
    {
        $action = new GetEditions($this->repository);
        $editions = $action->invoke();
        return response()->json($editions);
    }

    public function getByID($id)
    {
        $request = [$this->pk_ID => $id];
        $action = new GetEditionByID($this->repository, collect($request));
        $edition = $action->invoke();
        return response()->json($edition);
    }

    public function getByName($name){
        $request = ['editionName' => $name];
        $action = new SearchByName($this->repository, collect($request));
        $edition = $action->invoke();
        return response()->json($edition);
    }
}