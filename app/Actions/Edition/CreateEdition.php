<?php

namespace App\Actions\Edition;


class CreateEdition extends BaseEditionAction
{
    protected $rules = [
        'editionName' => 'required|max:45'
    ];

    protected function onValidationSuccess()
    {
        $edition = $this->repository->create($this->data()->toArray());
        if ($edition) {
            return true;
        }
        return false;
    }
}