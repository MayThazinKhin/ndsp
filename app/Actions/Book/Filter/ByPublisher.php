<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 02/01/2018
 * Time: 3:43 PM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByPublisher extends BaseBookAction
{
    public $pages = 20;

    protected $rules = ['publisher_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->getBooksByPublisherId($this->data()['publisher_id'],$this->data()['buy_or_consigment'],$this->pages);
    }
}