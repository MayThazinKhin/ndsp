import VuePagination from '../core/VuePagination';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';

export default {

    components: {
        VuePagination, deleteModal
    },

    data: function () {
        return {
            isEdit: false,
            route: {
                get: '/admin/publisher/all?page=',
                create: '/admin/publisher/create',
                edit: '/admin/publisher/edit',
                delete: '/admin/publisher/'
            },
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            publishers: [],
            publisher: {id: null, publisherName: null}
        }
    },

    methods: {
        clearData() {
            this.publisher.id = null;
            this.publisher.publisherName = null;
            this.$validator.clean();
        },
        loadData(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.publishers = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        getPublishers() {
            this.loadData(this.route.get);
        },
        onPaginationClick() {
            this.getPublishers();
        },

        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(data) {
            var temp = Object.assign({}, data);
            this.publisher = temp;
            this.isEdit = true;
        },
        toggleDelete(id) {
            this.publisher.id = id;
        },

        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.publisher).then(response => {
                this.getPublishers();
                $('#modal1').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.publisher.id + '/delete').then(response => {
                this.getPublishers();
                this.publisher.id = null;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
        onSearchClick() {
            this.getPublishers();
        }
    },
    mounted() {
        this.getPublishers();
    }
}