<?php


namespace App\Actions\Admin;


class CreateAdmin extends BaseAdminAction
{
    protected $rules = [
        'username' => 'required|unique:admins',
        'password' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $admin=$this->data();

        $admin['password']=$this->cryptPassword();

        $admin = $this->repository->create($admin->toArray());
        if ($admin) {
            return true;
        }
        return false;
    }
}