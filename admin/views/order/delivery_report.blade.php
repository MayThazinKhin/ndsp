@extends('layout.app')
@section('title','Delivery charge report')
@section('delivery_report','active')
@section('content')
    <delivery-report inline-template>
        <div>
            <h3>Delivery Report</h3>
            <div class="content-down">
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-right">
                            <label>Start date</label>
                            <div class="input-field">
                                <datepicker v-model="start_date" placeholder="Start date"
                                ></datepicker>
                            </div>
                            <label>End date</label>
                            <div class="input-field">
                                <datepicker v-model="end_date" placeholder="End date">End date
                                </datepicker>
                            </div>
                        </div>
                        <input type="hidden">
                        <div class="content-up-left">
                            <a class="waves-effect waves-light btn"
                               @click="getReport">Get report</a>
                        </div>
                    </div>

                    <table

                            id="report-table-row"
                            class="data-table table-striped highlight ">
                        <h6>Total Delivery Charge : @{{ total_deli_charge }} Kyats</h6>
                        <thead>
                        <th>Order no</th>
                        <th>City</th>
                        <th>Date</th>
                        <th>Delivery Charge</th>
                        </thead>
                        <tbody>

                        <tr v-if="orders.length > 0"
                            v-for="order in orders"
                            :key="order.id">
                            <td>@{{ order.id }}</td>
                            <td>@{{ order.city }}</td>
                            <td>@{{ order.created_at }}</td>
                            <td>@{{ order.deli_charge }}</td>

                        </tr>
                        </tbody>

                    </table>
                    <b-pagination
                            v-model="current_page"
                            :total-rows="rows"
                            :per-page="per_page"
                            aria-controls="report-table-row"
                    ></b-pagination>
                </div>
            </div>
        </div>
    </delivery-report>
@endsection
