<?php

namespace App\Actions\Admin;

use App\Actions\Action;
use App\Repositories\AdminRepository;
use Illuminate\Support\Collection;

class BaseAdminAction extends Action
{
    protected $rules=[];
    public const PK_USERNAME='username';

    public function __construct(AdminRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository = $repository;
    }

    protected function onValidationSuccess()
    {

    }

    public function cryptPassword(){


        return bcrypt($this->data()['password']);
    }
}