<?php

namespace App\Actions\Book;

use App\FileSystem\Images\BookImage;

class DeleteBook extends BaseBookAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $_book = $this->repository->remove($this->data()['id']);

        //if book does not exists
        if (!$_book) return false;


        //if book exists delete book image
        $bookImg = new BookImage($_book->bookCoverImgUrl);
        $status = $bookImg->delete();


        return true;

    }
}