<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/01/2018
 * Time: 10:30 AM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class GetBookByGenreId extends BaseBookAction
{
    public $pages = 2;
    public $filtername='genres';
    protected $rules = ['genre_id' => 'required'];

    protected function onValidationSuccess()
    {
        return $this->repository->filterById($this->data()['genre_id'],$this->pages,$this->filtername);
    }
}