@extends('layout.master')
@section('title','Book Detail')
@section('style')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/slider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/detail.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/web/component.css')}}">
    <script typeof="text/"   type="text/css" src="URL::{{asset('js/web/modernizr.custom.js')}}"></script>
    <style>
        .vue-star-rating {
            display: inline-block !important;
            align-items: center;
        }
    </style>

@endsection
@section('content')

    <div class=" product-view" id="addtocard">
        <div class="container">

            <!-- *********** Modal Start *********** -->
            <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Rating this Book!</h4>
                        </div>
                        <form role="form" @submit.prevent="submitdata">
                            <div class="modal-body">



                                <star-rating v-model="comment.rating" :star-size="30"></star-rating>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" v-model="comment.username" name="username"  v-validate="'required'">
                                    <div  style="color: white;" v-show="errors.has('comment')">Required username.</div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control"   placeholder="Comment" v-model="comment.comment" name="comment"  v-validate="'required'"></textarea>
                                    <div  style="color: white;" v-show="errors.has('comment')">Required comment.</div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- left-side -->
                <div class="col-md-9">
                    <div class="row">
                        <!-- left inner side -->

                        <div class="col-md-4">
                            <div class="book-image">
                                <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                            </div>
                        </div><!-- left inner side end -->

                        <!-- right inner side -->
                        <div class="col-md-8">
                            <div class="product-shop">
                                <h1 class="book-name">{{$book->bookTitle}}</h1>
                                <p class="book-author">{{$book->author}}</p>
                                <div class="rating">
                                    <div class="rating-box">
                                        @for($i=0;$i<$book->rating;$i++)
                                            <i class="fa fa-star fa-x" aria-hidden="true"></i>

                                        @endfor
                                    </div>
                                    {{--<p class="rating-link ">--}}
                                    {{--<a href="#">2 Review</a>--}}
                                    {{--</p>--}}

                                </div>
                                <p class="availability">Availability: <span>In stock</span></p>
                                <div class="qty-box">
                                    <label for="qty">Qty:</label>
                                    <div class="qty-box-count">

                                        <input type="text" id="qty" v-model="qty">
                                        <button class="countBtn btn" @click="subQty" type="button">-</button>
                                        <button class="countBtn btn" @click="addQty" type="button">+</button>

                                    </div>
                                </div>
                                <p class="price-box">{{$book->bookSalePrice}}</p>
                                {{--<button class="add-to-cart" @click="addToStorage" type="button"><i class="fa fa-shopping-cart" aria-hidden="true"></i>  add to cart</button>--}}
                                <button class="add-comment" data-toggle="modal" data-target="#commentModal">  comment</button>
                                <a href="{{route('ebook/download',['name'=>$book->e_book_download])}}">
                                    <button class="add-comment"> download</button>
                                </a>
                                <div class="short-description">
                                    <ul>
                                        <li>Publisher : <span>{{$book->publisher->publisherName}}</span></li>
                                        <li>Release Date : <span>{{$book->created_at->format('d M Y')}}</span></li>
                                        <li>Edition : <span>{{$book->edition->editionName}}</span></li>
                                    </ul>
                                    <p>{{$book->bookDescription}}</p>
                                </div>
                            </div><!-- product shop end -->
                        </div> <!--right innr side end -->
                    </div>

                    <!-- product description -->
                    <div class="tab-books">
                        <!--tab nav menu -->
                        <div class="book-nav">
                            <ul class="nav nav-tabs">
                                <li><a href="#description" data-toggle="tab">description</a></li>
                                <li><a href="#additional" data-toggle="tab">additional</a></li>
                                <li><a href="#bookTags" data-toggle="tab">book tags</a></li>
                            </ul>
                        </div>
                        <!-- Tab nav content -->
                        <div class="box">
                            <div class="tab-content">
                                <!-- description content -->
                                <div id="description" class="tab-pane fade in active">
                                    <p>
                                        <span>A</span>{{$book->bookDescription}}
                                    </p>
                                </div>
                                <!-- additional content -->
                                <div id="additional" class="tab-pane fade">
                                    <p>Additional</p>
                                </div>
                                <!-- book tags -->
                                <div id="bookTags" class="tab-pane fade">
                                    <p>Book Tags</p>
                                </div>
                            </div>
                        </div><!-- tab nav content end-->
                    </div><!-- book description -->

                    <!-- INTERESTED BOOK SELL -->
                    <div class="interested-books">
                        <h1 class="interested-title">You may also be interested in the following product(s)</h1>
                        <ul class="product-grid">
                            @foreach($relatedbooks as $book)
                                <li class="item">
                                    <div class="product-image">
                                        <a href="{{URL::to('book/detail/'.$book->id)}}">
                                            <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                                        </a>
                                    </div>
                                    <div class="product-information">
                                        <div class="mini-rating">
                                            @for($i=0;$i<$book->rating;$i++)

                                                <i class="fa fa-star fa-x" aria-hidden="true"></i>

                                            @endfor
                                        </div>
                                        <p class="name"><a href="#">{{$book->bookTitle}}</a></p>
                                        <p class="author">{{$book->author}}</p>
                                        <p class="price">{{$book->bookSalePrice}}</p>
                                        <a href="{{URL::to('book/detail/'.$book->id)}}" class="quick-view"> Quick View</a>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div> <!-- left side end -->

                <!-- right side -->
                <div class="col-md-3 right">
                    <div class="right-side">
                        <div class="block-banner">
                            <a href="#">
                                <img src="{{URL::asset('images/hp-new.jpg')}}">
                            </a>
                        </div>

                        <!-- related products -->
                        <div class="block-related">
                            <div class="block-title">
                                <p>related books</p>
                            </div>


                            <ol class="mini-book-list">
                                @foreach($relatedbooks as $book)
                                    <li class="item">

                                        <div class="book">
                                            <a href="{{URL::to('book/detail/'.$book->id)}}">
                                                <img src="{{URL::to('image/book/'.$book->bookCoverImgUrl)}}">
                                            </a>
                                        </div>
                                        <div class="book-detail">
                                            <p class="name"><a href="#">{{$book->bookTitle}}</a></p>
                                            <p class="author">{{$book->author}}</p>
                                            <div class="mini-rating">
                                                @for($i=0;$i<$book->rating;$i++)
                                                    <i class="fa fa-star fa-x" aria-hidden="true"></i>

                                                @endfor



                                            </div>
                                            <p class="price">{{$book->bookSalePrice}}</p>

                                        </div>
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                    </div>

                </div><!-- right side end -->

            </div>
        </div>


    </div>

@endsection

@section('script')


    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.13/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>
    <script src="{{URL::asset('js/web/star-rating.min.js')}}"></script>
    <script>
      Vue.use(VeeValidate);
      Vue.component('star-rating', VueStarRating.default);
      new Vue({

        el: '#addtocard',

        data: {
          bookdata: {!! $book !!},

          total:0,
          qty:1,

          comment:{
            book_id:null,
            username:null,
            comment:null,
            rating:0,
            approve:true
          },
          isdisabled:false
        },
        methods : {

          subQty(){
            this.qty=this.qty-1;
          },
          addQty(){
            this.qty=this.qty+1;
          },

          submitdata(){

            this.comment.book_id=this.bookdata.id;
            var config = {
              headers: {'X-My-Custom-Header': 'Header-Value'}
            }
            this.$validator.validateAll().then(successsValidate => {
              if (successsValidate) {
                axios.post('/comment/upload',this.comment,config).then(function (response) {
                    $('#commentModal').modal('hide');
                })
                  .catch(function (error) {

                  });
              }
            }).catch(error => {
              this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });

          },
          addToStorage () {
            var temp = Object.assign({}, this.bookdata);
            var a=[];
            var localStoreItems=localStorage.getItem('cart_books');

            if(localStoreItems){
              a=(JSON.parse(localStoreItems));
              a.push({book_id:temp.id,bookTitle:temp.bookTitle,price:temp.bookSalePrice,qty:this.qty,total:temp.bookSalePrice*this.qty});
              localStorage.setItem('cart_books', JSON.stringify(a));

            }else{

              a.push({book_id:temp.id,bookTitle:temp.bookTitle,price:temp.bookSalePrice,qty:this.qty,total:temp.bookSalePrice*this.qty});
              localStorage.setItem('cart_books', JSON.stringify(a));
            }

          },
        },

      })
    </script>
    <script>
      // this is important for IEs
      var polyfilter_scriptpath = '/js/';
    </script>
    <script src="{{URL::asset('js/web/cssParser.js')}}"></script>
    <script src="{{URL::asset('js/web/css-filters-polyfill.js')}}"></script>
    <script src="{{URL::asset('js/web/classie.js')}}"></script>
    <script src="{{URL::asset('js/web/modalEffects.js')}}"></script>
    <script src="{{URL::asset('js/web/app.js')}}"></script>
@endsection