import Datepicker from 'vuejs-datepicker'
import {BPagination} from 'bootstrap-vue/es/components'
import {BTable} from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


export default {
    components: {
        Datepicker,
        BPagination
    },
    data() {
        return {
            start_date: new Date(),
            end_date: new Date(),
            url: '/admin/order/get_delivery_report',
            orders: [],
            total_deli_charge: 0,
            per_page: 2,
            current_page: 1,
        }
    },
    computed: {
        rows() {
            return this.orders.length
        }
    },
    methods: {
        getReport() {
            if (this.start_date >= this.end_date) {
                this.$parent.showToast('Start date must be smaller than end Date', 'error');
            }
            axios({
                method: 'post',
                url: this.url,
                data: {
                    start_date: this.start_date,
                    end_date: this.end_date,
                }
            }).then(({data}) => {
                this.orders = data.orders;
                this.orders.forEach((order) => {
                    order.city = this.getCity(order.town_id)
                })
                this.total_deli_charge = data.total_deli_charge;
            });
        },
        getCity(town_id) {
            switch (Number(town_id)) {
                case 1:
                    return 'Mandalay';
                case 2:
                    return 'Yangon';
                case 3:
                    return 'Other';
            }

        }
    }

}