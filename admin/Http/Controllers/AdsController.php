<?php

namespace Admin\Http\Controllers;

use App\Actions\Ads\CreateAds;

use App\Actions\Ads\DeleteAds;
use App\Actions\Ads\GetAdsById;
use App\Actions\Ads\GetAllAds;
use App\Actions\Ads\UpdateAds;

use App\Http\Controllers\ControllerTrait;
use App\Repositories\AdsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;

class AdsController extends Controller
{
    use ControllerTrait;



    public function __construct(AdsRepository $repository)
    {
        $this->repository = $repository;
    }

    function index()
    {
        // TODO: Implement index() method.
    }

    public function create(Request $request)
    {
        $action = new CreateAds($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return response()->json([$result->errors()],422);
        }
        return [$result];
    }

    public function update(Request $request)
    {

        $action = new UpdateAds($this->repository, collect($request->all()));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return $result->errors();
        }
        return [$result];
    }

    public function delete($id)
    {
        $request=['id'=>$id];

        $action = new DeleteAds($this->repository, collect($request));
        $result = $action->invoke();
        if ($result instanceof Validator) {
            return $result->errors();
        }
        return [$result];
    }

    function getAll()
    {
        $action = new GetAllAds($this->repository);
        $ads = $action->invoke();
        return [$ads];
    }

    function getByID($id)
    {
        $request = ['id' => $id];
        $action = new GetAdsById($this->repository, collect($request));
        $author = $action->invoke();
        return $author;
    }
}