<?php
/**
 * Created by PhpStorm.
 * User: Chan Nyein Thaw
 * Date: 5/11/2017
 * Time: 6:16 PM
 */

namespace App\FileSystem\Images;

use Illuminate\Support\Facades\Storage;

class BookImage extends BaseImageFileSystem
{

    private $temp_path;

    public function __construct($file)
    {

        parent::__construct($file);
        $this->path = 'images/books';
        $this->temp_path = 'images/temp';
    }

    public function base64StringToImage($img_info, $img_string)
    {

        $info = pathinfo($img_info);
        $content = $this->decodeBase64String($img_string);
        $name = trim(base64_encode($info['filename'] . str_random(4)), '=');
        $success = Storage::put($this->temp_path . '/' . $name, $content);


        if ($success) {

            $temp = $this->temp_path . '/' . $name;

            $filename = $this->path . '/' . $name . '.' . $info['extension'];

            if (Storage::move($temp, $filename)) return $name . '.' . $info['extension'];
        }
        return null;
    }

    function decodeBase64String($filestring)
    {
        list($type, $data) = explode(';', $filestring);
        list(, $data) = explode(',', $filestring);
        return base64_decode($data);
    }

    public function convertImageToBase64String()
    {
        $base64 = '';

        $type = pathinfo($this->getStoragePath(), PATHINFO_EXTENSION);

        $base64 = 'data:image/' . $type . ';base64,' . base64_encode(Storage::get($this->getPath() . '/' . $this->getStoredName()));

        return $base64;
    }
}