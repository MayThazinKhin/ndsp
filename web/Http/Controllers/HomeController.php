<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/12/2017
 * Time: 1:02 PM
 */

namespace Web\Http\Controllers;


use App\Actions\Home\LoadHomePage;
use App\Http\Controllers\Controller;
use App\Repositories\BookRepository;
use App\Repositories\CategoryRepository;

class HomeController extends Controller
{
    private $repository,$categoryRepository;
    public function __construct(BookRepository $repository,CategoryRepository $categoryRepository)
    {
        $this->repository=$repository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $actions = (new LoadHomePage($this->repository,$this->categoryRepository))->invoke();


        return view('home.index',$actions);
    }
}