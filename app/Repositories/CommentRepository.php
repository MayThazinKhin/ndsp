<?php

namespace App\Repositories;
use App\Models\Comment;

class CommentRepository extends Repository
{
    function model()
    {
        return Comment::class;
    }

    public function getWithPaginate($_page, $book_id)
    {
        return Comment::where('book_id', '=', $book_id)->paginate($_page);
    }

    public function getRatingCount($rating, $book_id)
    {
        $count = Comment::where('book_id', '=', $book_id)->where('rating', '=', $rating)->count();
        return $count;
    }

    public function getAllRatingCount($book_id)
    {
        $count = Comment::where('book_id', '=', $book_id)->count();
        return $count;
    }

    public function getRating($bookid)
    {

        $rating_0_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 1)->count() * 1;
        $rating_1 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 2)->count() * 2;
        $rating_1_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 3)->count() * 3;
        $rating_2 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 4)->count() * 4;
        $rating_2_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 5)->count() * 5;
        $total_rating = Comment::where('book_id', '=', $bookid)->count();
        $rating = 0;
        if ($total_rating > 0) {
            $rating = ($rating_0_5 + $rating_1 + $rating_1_5 + $rating_2 + $rating_2_5) / $total_rating;
        }
        return floor($rating);
    }

    public function getFiveComments($book_id)
    {
        $comments = Comment::where('book_id', '=', $book_id)->orderby('created_at', 'desc')->take(5)->get();
        return $comments;
    }

    public function getAll($page){
        return Comment::orderByDesc('created_at')->paginate($page);
    }
}