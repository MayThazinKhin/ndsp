<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 14/11/2017
 * Time: 9:12 AM
 */

namespace App\Repositories;


use App\Models\Sale;

class SaleRepository extends  Repository
{

    function model()
    {
        return Sale::class;
    }
}