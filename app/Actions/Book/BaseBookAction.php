<?php


namespace App\Actions\Book;


use App\Actions\Action;
use App\Repositories\BookRepository;
use Illuminate\Support\Collection;

class BaseBookAction extends  Action
{
    protected $rules=[];

    public function __construct(BookRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected  function onValidationSuccess(){}
}