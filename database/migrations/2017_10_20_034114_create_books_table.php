<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('edition_id');

            $table->unsignedInteger('publisher_id');

            $table->mediumText('bookTitle', 500);
            $table->longText('bookDescription')->nullable();

            $table->double('bookSalePrice');
            $table->string('bookCoverImgUrl', 255);
            $table->integer('isEbook')->default(0);
            $table->string('e_book_download', 255)->nullable();
            $table->timestamps();
            $table->foreign('edition_id')->references('id')->on('editions')->onDelete('cascade');

            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
        });
    }

    /**f
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
