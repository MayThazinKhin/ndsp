<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 1:36 PM
 */

namespace App\Actions\Sale;


class CreateSale extends  BaseSaleAction
{
    protected function onValidationSuccess()
    {
        $sale = $this->repository->create($this->data()->toArray());
        if ($sale) {
            return true;
        }
        return false;
    }
}