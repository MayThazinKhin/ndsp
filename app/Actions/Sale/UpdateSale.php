<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 15/11/2017
 * Time: 1:37 PM
 */

namespace App\Actions\Sale;


class UpdateSale extends BaseSaleAction
{
    protected function onValidationSuccess()
    {
        $sale = $this->repository->update($this->data()->toArray());
        if ($sale) {
            return true;
        }
        return false;
    }
}