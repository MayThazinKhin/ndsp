webpackJsonp([1],{

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(167);


/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vee_validate__ = __webpack_require__(6);




__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_2_vee_validate__["default"]);

new __WEBPACK_IMPORTED_MODULE_0_vue___default.a({
    el: '#orderconfirm',

    data: {
        confirm: true,
        books: [],
        email: null,
        phone: null,
        address: null,
        city: null,
        delivery_charge: 0,
        qty: 0,
        tempindex: null
    },
    updated: function updated() {},

    methods: {
        calculateDeliveryCharge: function calculateDeliveryCharge() {
            console.log('hello');
            var totalBookQty = 0;
            this.delivery_charge = 0;
            if (this.books.length > 0) {
                switch (Number(this.city)) {
                    case 1:
                        this.delivery_charge = 1000;
                        break;
                    case 2:
                        this.delivery_charge = 1000;
                        break;
                    case 3:
                        this.delivery_charge = 0;
                        this.books.forEach(function (book) {
                            totalBookQty += Number(book.qty);
                        });
                        if (totalBookQty === 1) {
                            this.delivery_charge = 1000;
                        } else if (totalBookQty > 1) {
                            this.delivery_charge += 1000 + (totalBookQty - 1) * 300;
                        }
                        break;
                }
            }
            console.log(this.delivery_charge);
        },
        remove: function remove(index) {
            console.log(index);
            this.books.splice(index, 1);
            this.calculateDeliveryCharge();
            var localStoreItems = localStorage.getItem('cart_books');

            if (localStoreItems) {
                var a = localStorage.removeItem('cart_books');
                if (this.books.length > 0) {
                    localStorage.setItem('cart_books', JSON.stringify(this.books));
                    $('#edit-modal').modal('hide');
                }
            }
        },
        editQty: function editQty() {
            var localStoreItems = localStorage.getItem('cart_books');

            if (localStoreItems) {
                if (this.books.length > 0) {
                    localStorage.removeItem('cart_books');
                    this.books[this.tempindex].qty = this.qty;
                    localStorage.setItem('cart_books', JSON.stringify(this.books));
                    $('#edit-modal').modal('hide');
                    this.calculateDeliveryCharge();
                    this.calculateDeliveryCharge();
                }
            }
        },
        editClick: function editClick(index, qty) {
            this.qty = qty;
            this.tempindex = index;
        },
        submitdata: function submitdata() {
            var _this = this;

            this.$validator.validateAll().then(function (successsValidate) {
                if (successsValidate) {
                    var config = {
                        headers: { 'X-My-Custom-Header': 'Header-Value' }
                    };
                    __WEBPACK_IMPORTED_MODULE_1_axios___default.a.post('/order/create', {
                        email: _this.email,
                        phone: _this.phone,
                        address: _this.address,
                        books: _this.books,
                        town_id: _this.city
                    }, config).then(function (response) {
                        localStorage.removeItem('cart_books');
                        console.log(response.data);
                        window.location.href = '/';
                        console.log(response.data);
                    });
                }
            });
        },
        selectCity: function selectCity() {
            this.calculateDeliveryCharge();
        }
    },
    mounted: function mounted() {
        this.books = JSON.parse(localStorage.getItem('cart_books'));
        if (this.books) {
            this.confirm = this.books.length > 0 ? false : true;
        } else {
            this.confirm = true;
        }
    }
});

/***/ })

},[166]);