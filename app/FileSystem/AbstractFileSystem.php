<?php


namespace App\FileSystem;

use Faker\Provider\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

abstract class AbstractFileSystem {
    protected $allowedMimeTypes = [];
    protected $path;
    private $file;
    private $storedName;

    public function __construct($file) {
        $this->path = '';
        $this->storedName = '';
        if ($file instanceof UploadedFile) {
            $this->file = $file;
        }

        if (is_string($file)) {
           $this->storedName = $file;
        }
    }

    public function getPath() : string {
        return $this->path;
    }

    public function getStoredName() : string {
        return $this->storedName;
    }

    public function getStoragePath():string{
        return storage_path() . '/app/' .$this->path . '/' . $this->storedName;
    }

    public function getClientOriginalName() : ?string {
        if ($this->file instanceof UploadedFile) {
            return $this->file->getClientOriginalName();
        }

        return '';
    }

    public function getClientOriginalExtension() : string {
        if ($this->file instanceof UploadedFile) {
            return $this->file->getClientOriginalExtension();
        }

        return '';
    }

    public function getClientSize() : ?int {
        if ($this->file instanceof UploadedFile) {
            return $this->file->getClientSize();
        }

        return null;
    }

    public function store() : bool {
        if (!($this->file instanceof UploadedFile)) return false;

        if (!$this->checkMimeType($this->file->getMimeType())) return false;

        $result = false;
        try {
            $fileName = $this->file->store($this->path);
            if ($fileName) {
                $this->storedName = basename($fileName);
                $result = true;
            }
        } catch(\ErrorException $e) { }

        return $result;
    }


    public function delete() : bool {
        if($this->exists()){
            return  unlink(storage_path() . '/app/' .$this->path . '/' . $this->storedName);
        }
        return false;
    }

    public function exists() : bool {
        return Storage::exists($this->path . '/' . $this->storedName);
    }

    public function getFileResponse() {

            return response()->file(storage_path() . '/app/' .$this->path . '/' . $this->storedName);

    }

    private function checkMimeType($mimeType) : bool {
        if (count($this->allowedMimeTypes) > 0)
            return in_array($mimeType, $this->allowedMimeTypes);
        else
            return true;
    }

    public function getFileSize(){
        if($this->exists()){return $this->bytesToHuman(Storage::size($this->path . '/' . $this->storedName));}

        return 0;
    }

    public function bytesToHuman($bytes)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}