<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 10/11/2017
 * Time: 9:48 AM
 */

namespace App\FileSystem\Images;


use App\FileSystem\AbstractFileSystem;

class BaseImageFileSystem extends AbstractFileSystem {

    public function __construct($file) {
        parent::__construct($file);
    }
}