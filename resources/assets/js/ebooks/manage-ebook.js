import Helper from '../core/Helper';
import AuthorSelect from '../core/AuthorSelect';
import CategorySelect from '../core/CategorySelect';
import GenreSelect from '../core/GenreSelect';
import PublisherSelect from '../core/PublisherSelect';
import EditionSelect from '../core/EditionSelect';
let boolListUrl="/admin/ebook";
export default {
    components: {
        AuthorSelect, CategorySelect, GenreSelect, PublisherSelect, EditionSelect
    },
    data: function () {
        return {
            isEdit: false,

            selectedAuthors: [],
            selectedGenres: [],
            selectedCategories: [],
            selectedPublisher: [],
            selectedEdition: [],

            book: {
                id: null,
                bookTitle: '',
                bookCoverImgUrl: '',
                coverInfo: null,
                bookSalePrice: 0,
                bookDescription: '',
                authors: [],
                categories: [],
                genres: [],
                edition_id: null,
                publisher_id: null,
                total_cost_of_books: 0,
                qty: 0,
                isEbook: true,
                e_book_download: null,
                stock_id: null,
                isConsigment: false,
                ebookinfo: null,
            },
            ebookfilename: null
        }
    },
    methods: {
        authorChange(value) {
            this.book.authors = value;
        },
        categoryChange(value) {
            this.book.categories = value;
        },
        genreChange(value) {
            this.book.genres = value;
        },
        publisherChange(value) {
            this.book.publisher_id = value.id;
        },
        editionChange(value) {
            this.book.edition_id = value.id;
        },
        getBookDetail(id) {
            axios.get('/admin/ebook/' + id + '/' + 'detail').then(response => {
                this.book = response.data;
                this.selectedAuthors = response.data.authors;
                this.selectedPublisher = response.data.publisher;
                this.selectedEdition = response.data.edition;
                this.selectedCategories = response.data.categories;
                this.selectedGenres = response.data.genres;
                this.book.bookCoverImgUrl='/api/image/book/'+this.book.bookCoverImgUrl;
                this.book.e_book_download='/api/ebook/download/'+this.book.e_book_download;

            }).catch(() => this.$parent.showToast('An occured while loading data.', 'error'));
        },
        asyncFindGenre(query) {
            if (query) {
                axios.get('/admin/genre/' + query + '/findbyname').then(response => {
                    this.genre_collections = response.data;
                });
            }
        },
        onFileChange(e) {
            this.book.coverInfo = e.target.value;
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length) return;
            this.createImage(files[0]);
        },
        createImage(file) {
            var image = new Image();
            var reader = new FileReader();
            var vm = this;
            reader.onload = (e) => {
                vm.book.bookCoverImgUrl = e.target.result;
            };
            reader.readAsDataURL(file);
        },

        onEbookFileChange(e) {
            this.book.ebookinfo = e.target.value;

            var files = e.target.files || e.dataTransfer.files;

            this.ebookfilename = files[0].name;
            if (!files.length)
                return;
            this.createEbookFile(files[0]);
        },
        createEbookFile(file) {
            var reader = new FileReader();
            var vm = this;
            reader.onload = (e) => {
                vm.book.e_book_download = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        submitdata() {
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    this.performProcess(this.isEdit ? 'edit' : 'create');
                } else {
                    this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },

        performProcess(performtype) {
            axios.post('/admin/ebook/' + performtype, this.book).then(response => {
                this.$parent.showToast(this.isEdit ? 'Updated' : 'Inserted' + ' successfully.', 'success');
                window.location.href = boolListUrl;
            }).catch(error => {
                this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
            });
        },
        checkUrlParam() {
            let book_id = Helper.getUrlParameter('book_id');
            if (book_id != null) {
                this.isEdit = true;
                this.getBookDetail(book_id);
            }
        }
    },
    mounted() {
        this.checkUrlParam();
    }
}