<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 09/11/2017
 * Time: 12:41 PM
 */

namespace App\Actions\Admin;

use App\Actions\Action;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
class AdminLogin extends Action
{
    protected $rules = [
        'username' => 'required',
        'password' => 'required'
    ];

    public function __construct(Collection $data = null)
    {
        parent::__construct($data);
    }


    protected function onValidationSuccess()
    {
        $auth = (['username' => $this->data()['username'],
            'password' => $this->data()['password']]);
        if (!Auth::attempt($auth)) {
            return false;
        }
        Auth::login(Auth::user());


        return true;
    }
}