<?php

use Illuminate\Database\Seeder;

class EditionsTableSeeder extends Seeder
{

    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (range(1, 5) as $i) {
            \App\Models\Edition::create([
                'editionName' => $i. 'edition',
            ]);
        }
    }
}
