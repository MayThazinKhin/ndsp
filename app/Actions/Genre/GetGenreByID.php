<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 24/01/2018
 * Time: 10:06 AM
 */

namespace App\Actions\Genre;


class GetGenreByID extends  BaseGenreAction
{
    protected function onValidationSuccess()
    {
        $author = $this->repository->getBy($this->data()['id']);
        return $author;
    }
}