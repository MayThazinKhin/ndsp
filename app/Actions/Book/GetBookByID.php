<?php

namespace App\Actions\Book;


class GetBookByID extends BaseBookAction
{
    protected function onValidationSuccess()
    {
        $book = $this->repository->getBookByID($this->data()['id']);
        return $book;
    }
}