<?php

namespace App\Actions\Ebook;

use App\Actions\Action;
use App\Repositories\EbookRepository;
use Illuminate\Support\Collection;

class BaseEbookAction extends Action
{
    protected $rules = [];

    public function __construct(EbookRepository $repository,Collection $data = null)
    {
        parent::__construct($data);
        $this->repository=$repository;
    }

    protected  function onValidationSuccess(){}
}