
var clicked = false;
var isGrow = true;
var sideBar = $('.sidebar');
var Content = $(".content");
var group2 = $('.group-2');



$(document).ready(function() {
    // initialize material select logic
    $('select').material_select();


    // sidebar minimize logic
    $("#close-btn").on('click', ()=>{
        sideBar.toggleClass("grow-sideBar");
        if(isGrow){
            // console.log('to small');
            Content.css('margin-left', '60px');
            Content.css('transition', 'margin-left .2s ease-out');
        } else {
            Content.css('margin-left', '200px');
            Content.css('transition', 'margin-left .2s ease-out');
        }
        isGrow = !isGrow;
    });

    // add active class to sidebar list  when click
    var li = $('.sidebar li');
    li.on('click', function(){
        li.removeClass('selected');
        $(this).addClass('selected');
    });

    // activate dropdown when hover
    $('.admin-dropdown').dropdown({
        hover: true, // Activate on hover
    });

    // radio buttons logic
    // default checked
    if($('#confirm').prop('checked')){
        $('.radio-group-2').addClass('show-radio');
    }

    $('input[type="radio"]').click(function(){
        if($('#confirm').prop('checked')){
            // $('.radio-group-2').css('display' , 'flex');
            $('.radio-group-2').addClass('show-radio');
        } else {
            $('.radio-group-2').removeClass('show-radio');
        }
    })
    $(document).ready(function(){
        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
    });

    //$('#modal1').modal('open');

    //$('#modal1').modal('close');

    $('.modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal

            opacity: .5, // Opacity of modal background
            inDuration: 300, // Transition in duration
            outDuration: 200, // Transition out duration
            startingTop: '4%', // Starting top style attribute
            endingTop: '10%', // Ending top style attribute
        }
    );

  $(document).ready(function(){
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    Materialize.updateTextFields();
  });
});