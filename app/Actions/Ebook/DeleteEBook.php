<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 05/01/2018
 * Time: 2:02 PM
 */

namespace App\Actions\Ebook;


use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;

class DeleteEBook extends  BaseEbookAction
{
    protected $rules = [
        'id' => 'required'
    ];

    protected function onValidationSuccess()
    {
        $_book = $this->repository->remove($this->data()['id']);

        //if book does not exists
        if (!$_book) {
            return false;
        }

        //if book exists delete book image
        $bookImg = new BookImage($_book->bookCoverImgUrl);
        $status = $bookImg->delete();

        $pdf = new BookPdf($_book->e_book_download);
        $status = $pdf->delete();

        return true;

    }
}