@extends('layout.app')
@section('title','Books')
@section('ebook','selected')
@section('content')


    <ebook-list inline-template>

        <div>
            <h3>Books list</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <a :href="route.manageURL" class="waves-effect waves-light btn"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Ebook</a>
                            {{--<button @click="fuck" class="waves-effect waves-light btn"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New Book</button>--}}
                        </div>

                        <div class="content-up-right">
                            <multiselect :show-labels="false" :searchable="false" class="search-input" @input="onOptionValueChange" v-model="selectedOption"  track-by="text" label="text" :options="filterOptions"  :allow-empty="false" ></multiselect>

                            <div class="search-bar">
                                <author-select :multi="false" :sstyle="'search-input'" :value="selectedFilter"  @input="authorChange" v-if="selectedOption.value=='author'"></author-select>
                                <category-select :multi="false" :sstyle="'search-input'" :value="selectedFilter"  @input="categoryChange" v-if="selectedOption.value=='category'"></category-select>
                                <genre-select :multi="false" :sstyle="'search-input'" :value="selectedFilter"  @input="genreChange" v-if="selectedOption.value=='genre'"></genre-select>
                                <button class="search-btn waves-effect waves-light" @click="onSearchClick">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <table class="highlight striped data-table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Publisher</th>
                            <th>Edition</th>
                            <th>Authors</th>
                            <th>Categories</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="book,index in books">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{book.bookTitle}}</td>
                            <td>@{{book.publisher.publisherName}}</td>
                            <td>@{{book.edition.editionName}}</td>
                            <td>@{{book.author}}</td>
                            <td>@{{book.category}}</td>
                            <td>
                                <a :href="route.detailURL+book.id" class="btn-floating"><i class="material-icons fa fa-pencil-square-o "></i></a>
                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(book.id)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </ebook-list>
@endsection
