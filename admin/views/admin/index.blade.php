@extends('layout.app')
@section('title','User')
@section('admins','selected')

@section('content')
    <admin-list inline-template>
        <div>
            <h3>User List</h3>
            <div class="content-down">
                <delete-modal @input="performdelete"></delete-modal>
                <div class="card-content">
                    <div class="content-up">
                        <div class="content-up-left">
                            <button class="modal-trigger waves-effect waves-light btn" data-target="user-modal" @click="toggleAddModal"><i class="fa fa-plus material-icons left" aria-hidden="true"></i>Add New User</button>
                        </div>
                    </div>

                    <!-- Data Lists -->
                    <div id="user-modal" class="modal user-modal">
                        <form @submit.prevent="submitdata()">
                            <div class="modal-content">
                                <h4>Add User</h4>
                                <input type="text" name="username"  v-model="user.username" v-validate="'required'" placeholder="User Name">
                                <div id="cemail-error" class="error" ><span v-if="errors.has('username')">Required user name.</span></div>

                                <input type="password"  placeholder="Password" v-validate="'required'"  v-model="user.password" name="password">
                                <div id="cemail-error" class="error" ><span v-if="errors.has('password')">Required password.</span></div>

                                <input type="password"  placeholder="Confirm Password" v-validate="'confirmed:password'" data-vv-as="password" v-model="user.password_confirmation" name="password_confirmation">
                                <div id="cemail-error" class="error" ><span v-if="errors.has('password_confirmation')">Password and Confirm Password doesn't match.</span></div>

                            </div>
                            <div class="modal-footer">
                                <a data-target="user-modal" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                                <button type="submit" class="modal-action waves-effect waves-green btn-flat add-btn">Add</button>
                            </div>
                        </form>
                    </div>
                    <div id="modal2" class="modal">
                        <form @submit.prevent="changePassword">
                        <div class="modal-content">
                            <h4>Edit User</h4>
                            <input type="password"  placeholder="Password" v-validate="'required'"  v-model="user.password" name="fpassword">
                            <div id="cemail-error" class="error" v-show="errors.has('fpassword')">Required Password.</div>
                            <input type="password"  placeholder="Confirm Password" v-validate="'confirmed:password'" data-vv-as="password" v-model="user.password_confirmation" name="editpassword_confirmation">
                            <div id="cemail-error" class="error" v-show="errors.has('editpassword_confirmation')">Password and Confirm Password doesn't match.</div>
                        </div>
                        <div class="modal-footer">
                            <a data-target="modal2"  class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                            <button type="submit" class="modal-action waves-effect waves-green btn-flat add-btn">Update</button>
                        </div>
                        </form>
                    </div>


                    <table class="highlight striped data-table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr v-for="user,index in users">
                            <td>@{{pagination.from+index}}</td>
                            <td>@{{user.username}}</td>
                            <td>
                                <button  data-target="modal2" class="modal-trigger btn-floating" @click="toggleEdit(user)"><i class="material-icons fa fa-pencil-square-o "></i></button>
                                <button class="btn-floating waves-effect waves-light modal-trigger" data-target="delete-modal" @click="toggleDelete(user.username)"><i class="material-icons fa fa-trash-o"></i></button>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div><!-- card-content end -->
            </div>
            <div class="paginations">
                <vue-pagination  :length.number="pagination.last_page" v-model="pagination.current_page" @input="onSearchClick"></vue-pagination>
            </div>
        </div>
    </admin-list>
@endsection