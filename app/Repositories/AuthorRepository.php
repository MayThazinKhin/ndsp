<?php

namespace App\Repositories;

use App\Models\Author;


class AuthorRepository extends  Repository
{
    function model()
    {
        return Author::class;
    }

    function findByName($name){
        return Author::where('authorName','LIKE',$name.'%')->take(10)->get();
    }
}