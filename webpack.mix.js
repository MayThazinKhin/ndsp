let mix = require('laravel-mix');


// .js('resources/assets/js/app.js', 'public/js/admin')
//   .js('resources/assets/js/users/login.js','public/js')
//   .extract(['vue','v-toaster','vue-multiselect','axios','vee-validate','vue-date']);
// mix
//     .js('resources/assets/js/app.js', 'public/js');


if (process.env.section) {
    require(`${__dirname}/webpack.mix.${process.env.section}.js`);
}