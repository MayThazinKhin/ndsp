<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 18/01/2018
 * Time: 1:47 PM
 */

namespace App\Actions\Book\Filter;


use App\Actions\Book\BaseBookAction;

class ByGenreName extends  BaseBookAction
{
    public $pages = 20;

    protected $rules = [];

    protected function onValidationSuccess()
    {
        return $this->repository->getBookByFilterName($this->data()['genreName'], $this->pages,'genreName','genres');
    }
}