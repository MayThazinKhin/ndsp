<?php



namespace App\Actions\Ebook;

use App\FileSystem\Images\BookImage;
use App\FileSystem\PDF_Files\BookPdf;

class CreateEbook extends BaseEbookAction
{
    protected $rules = [
        'bookTitle' => 'required',
        'bookCoverImgUrl' => 'required',
        'edition_id' => 'required',
        'publisher_id' => 'required',
        'bookSalePrice' => 'required',
        'e_book_download' => 'required'
    ];


    protected function onValidationSuccess()
    {
        $ebook = $this->data();


        $bookImage = new BookImage($ebook['bookCoverImgUrl']);
        $cover_name = $bookImage->base64StringToImage($ebook['coverInfo'], $ebook['bookCoverImgUrl']);
        if (!$cover_name) {return false;}


        $ebook['bookCoverImgUrl'] = $cover_name;


        $pdf = new BookPdf($ebook['e_book_download']);
        $ebookname = $pdf->base64StringToFile($ebook['ebookinfo'], $ebook['e_book_download']);
        if (!$ebookname) {return false;}

        $ebook['e_book_download'] = $ebookname;


        $insertedBook = $this->repository->create($ebook->toArray());

        if($insertedBook){
            //insert into author_book table
            $insertedBook->authors()->attach($ebook['authors']);

            //insert into category_book table
            $insertedBook->categories()->attach($ebook['categories']);

            //insert into genre_book table
            $insertedBook->genres()->attach($ebook['genres']);
        }
    }
}