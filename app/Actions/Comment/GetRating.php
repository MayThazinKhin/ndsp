<?php
/**
 * Created by PhpStorm.
 * User: Angelo
 * Date: 16/01/2018
 * Time: 2:38 PM
 */

namespace App\Actions\Comment;


use App\Models\Comment;

class GetRating extends  BaseCommentAction
{
    protected function onValidationSuccess()
    {
        $bookid=$this->data()['book_id'];
        $approve=1;
        $rating_0_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 1)->where('approve', '=',$approve)->count();
        $rating_1 =Comment::where('book_id', '=', $bookid)->where('rating', '=', 2)->where('approve', '=',$approve)->count();
        $rating_1_5 =Comment::where('book_id', '=', $bookid)->where('rating', '=', 3)->where('approve', '=',$approve)->count();
        $rating_2 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 4)->where('approve', '=',$approve)->count();
        $rating_2_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 5)->where('approve', '=',$approve)->count();
        $rating_3 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 6)->where('approve', '=',$approve)->count();
        $rating_3_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 7)->where('approve', '=',$approve)->count();
        $rating_4 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 8)->where('approve', '=',$approve)->count();
        $rating_4_5 =Comment::where('book_id', '=', $bookid)->where('rating', '=', 9)->where('approve', '=',$approve)->count();
        $rating_5 = Comment::where('book_id', '=', $bookid)->where('rating', '=', 10)->where('approve', '=',$approve)->count();

        $total_rating =  Comment::where('book_id', '=',  $bookid)->where('approve', '=', $approve)->count();
        $rating=0;
        if($total_rating >0) {
            $rating = ($rating_0_5 + $rating_1 + $rating_1_5 + $rating_2 + $rating_2_5 + $rating_3 + $rating_3_5 + $rating_4 + $rating_4_5 + $rating_5) / $total_rating;
        }
        return $rating;
    }
}