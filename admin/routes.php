<?php

Route::name('admin.')->prefix('admin')->group(function () {

    Route::name('login')->get('/', 'AuthController@index');

    Route::name('logout')->get('/logout', 'AuthController@adminLogout');

    Route::name('login-post')->post('login-post', 'AuthController@login');

    Route::name('asyncbook')->get('/asyncbook/{q}', 'BookController@asyncbook');
});

Route::middleware('admin.auth')->name('admin.')->prefix('admin')->group(function () {

    Route::name('author.')->prefix('author')->group(function () {

        Route::name('index')->get('/', 'AuthorController@index');

        Route::name('create')->post('create', 'AuthorController@create');

        Route::name('edit')->post('edit', 'AuthorController@update');

        Route::name('delete')->get('{id}/delete', 'AuthorController@delete');

        Route::name('all')->get('all', 'AuthorController@getAll');

        Route::name('detail')->get('{id}/detail', 'AuthorController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'AuthorController@getByName');
    });

    Route::name('genre.')->prefix('genre')->group(function () {

        Route::name('index')->get('/', 'GenreController@index');

        Route::name('create')->post('create', 'GenreController@create');

        Route::name('edit')->post('edit', 'GenreController@update');

        Route::name('delete')->get('{id}/delete', 'GenreController@delete');

        Route::name('all')->get('all', 'GenreController@getAll');

        Route::name('detail')->get('{id}/detail', 'GenreController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'GenreController@getByName');
    });

    Route::name('category.')->prefix('category')->group(function () {

        Route::name('index')->get('/', 'CategoryController@index');

        Route::name('create')->post('create', 'CategoryController@create');

        Route::name('edit')->post('edit', 'CategoryController@update');

        Route::name('delete')->get('{id}/delete', 'CategoryController@delete');

        Route::name('all')->get('all', 'CategoryController@getAll');

        Route::name('detail')->get('{id}/detail', 'CategoryController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'CategoryController@getByName');

    });

    Route::name('edition.')->prefix('edition')->group(function () {

        Route::name('index')->get('/', 'EditionController@index');

        Route::name('create')->post('create', 'EditionController@create');

        Route::name('edit')->post('edit', 'EditionController@update');

        Route::name('delete')->get('{id}/delete', 'EditionController@delete');

        Route::name('all')->get('all', 'EditionController@getAll');

        Route::name('detail')->get('{id}/detail', 'EditionController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'EditionController@getByName');
    });

    Route::name('publisher.')->prefix('publisher')->group(function () {

        Route::name('index')->get('/', 'PublisherController@index');

        Route::name('create')->post('create', 'PublisherController@create');

        Route::name('edit')->post('edit', 'PublisherController@update');

        Route::name('delete')->get('{id}/delete', 'PublisherController@delete');

        Route::name('all')->get('all', 'PublisherController@getAll');

        Route::name('detail')->get('{id}/detail', 'PublisherController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'PublisherController@getByName');
    });

    Route::name('book.')->prefix('book')->group(function () {

        Route::name('index')->get('/', 'BookController@index');

        Route::name('create')->post('create', 'BookController@create');

        Route::name('action')->get('action', 'BookController@showActionPage');

        Route::name('edit')->post('edit', 'BookController@update');

        Route::name('delete')->get('delete/{id}', 'BookController@delete');

        Route::name('all')->get('all', 'BookController@getAll');

        Route::name('detail')->get('{id}/detail', 'BookController@getByID');

        Route::name('findby.')->prefix('findby')->group(function () {
            Route::name('consigment')->get('consigment/{consigment}', 'BookController@viewByConsigment');
            Route::name('author')->get('author/{author_id}/{consigment}', 'BookController@viewByAuthor');
            Route::name('category')->get('category/{category_id}/{consigment}', 'BookController@viewByCategory');
            Route::name('genre')->get('genre/{genre_id}/{consigment}', 'BookController@viewByGenre');
            Route::name('publisher')->get('publisher/{publisher_id}/{consigment}', 'BookController@viewByPublisher');
        });
    });

    Route::name('ebook.')->prefix('ebook')->group(function () {

        Route::name('index')->get('/', 'EbookController@index');

        Route::name('create')->post('create', 'EbookController@create');

        Route::name('action')->get('action', 'EbookController@showActionPage');

        Route::name('edit')->post('edit', 'EbookController@update');

        Route::name('delete')->get('/delete/{id}', 'EbookController@delete');

        Route::name('all')->get('all', 'EbookController@getAll');

        Route::name('detail')->get('{id}/detail', 'EbookController@getByID');
        Route::name('findby.')->prefix('findby')->group(function () {

            Route::name('author')->get('author/{author_id}', 'EbookController@viewByAuthor');
            Route::name('category')->get('category/{category_id}', 'EbookController@viewByCategory');
            Route::name('genre')->get('genre/{genre_id}', 'EbookController@viewByGenre');

        });
    });

    Route::name('comment.')->prefix('comment')->group(function () {

        Route::name('index')->get('/', 'CommentController@index');
        Route::name('get_comments')->get('get_comments', 'CommentController@getAllComments');
        Route::name('delete')->get('delete/{id}', 'CommentController@delete');

    });

    Route::name('user.')->prefix('user')->group(function () {

        Route::name('index')->get('/', 'AdminController@index');

        Route::name('create')->post('create', 'AdminController@create');

        Route::name('update')->post('update', 'AdminController@update');

        Route::name('delete')->get('{username}/delete', 'AdminController@delete');

        Route::name('all')->get('all', 'AdminController@getAll');

        Route::name('detail')->get('{id}/detail', 'AdminController@getByID');
    });

    Route::name('ads.')->prefix('ads')->group(function () {

        Route::name('create')->post('create', 'AdsController@create');

        Route::name('update')->post('update', 'AdsController@update');

        Route::name('delete')->get('{id}/delete', 'AdsController@delete');

        Route::name('all')->get('all', 'AdsController@getAll');

        Route::name('detail')->get('{id}/detail', 'AdsController@getByID');
    });

    Route::name('purchase.')->prefix('purchase')->group(function () {

        Route::name('index')->get('/', 'PurchaseController@index');

        Route::name('create')->post('create', 'PurchaseController@create');

        Route::name('update')->post('update', 'PurchaseController@update');

        Route::name('delete')->get('delete/{id}', 'PurchaseController@delete');

        Route::name('all')->get('all', 'PurchaseController@getAll');

        Route::name('detail')->get('detail/{id}', 'PurchaseController@getByID');
    });

    Route::name('article.')->prefix('article')->group(function () {

        Route::name('index')->get('/', 'ArticleController@index');
        Route::name('create')->get('/create', 'ArticleController@createIndex');
        Route::name('showedit')->get('/showedit/{id}', 'ArticleController@editIndex');

        Route::name('create')->post('create', 'ArticleController@create');

        Route::name('edit')->post('edit', 'ArticleController@update');

        Route::name('delete')->get('{id}/delete', 'ArticleController@delete');

        Route::name('all')->get('all', 'ArticleController@getAll');

        Route::name('detail')->get('{id}/detail', 'ArticleController@getByID');

        Route::name('findbyname')->get('{name}/findbyname', 'ArticleController@getByName');
    });


    Route::name('order.')->prefix('order')->group(function () {

        Route::name('index')->get('/', 'OrderController@index');
        Route::name('create')->get('/create', 'OrderController@createIndex');
        Route::name('create')->post('/create', 'OrderController@create');
        Route::name('view-detail')->get('view-detail', 'OrderController@detailViewIndex');
        Route::name('detail')->get('detail', 'OrderController@detail');
        Route::name('delete')->get('delete', 'OrderController@delete');
        Route::name('book/delete')->get('book/delete/{id}', 'OrderController@deleteOrderBook');
        Route::name('all')->get('all/{status}', 'OrderController@getAll');
        Route::name('filterbyphone')->get('filterbyphone/{status}/{phone}', 'OrderController@filterOrderByPhone');
        Route::name('filterbyemail')->get('filterbyemail/{status}/{email}', 'OrderController@filterOrderByEmail');
        Route::name('updatestatus')->post('updatestatus', 'OrderController@updateStatus');
        Route::name('show_delivery_report')->get('/show_delivery_report', 'OrderController@showDeliveryReport');
        Route::name('get_delivery_report')->post('/get_delivery_report', 'OrderController@getDeliveryReport');
    });
});


