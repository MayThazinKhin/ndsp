import VuePagination from '../core/VuePagination';
import BookSelect from '../core/BookSelect';
import deleteModal from '../core/DeleteModal';
import Helper from '../core/Helper';
import datepicker from 'vue-date';
import multiselect from 'vue-multiselect';
export default {
    components: {
        VuePagination, deleteModal,BookSelect,datepicker,multiselect
    },

    data: function () {
        return {
            route: {
                get: '/admin/purchase/all?page=',
                create: '/admin/purchase/create',
                edit: '/admin/purchase/update',
                delete: '/admin/purchase/delete/'
            },
            isSingleLoading: false,
            collections: [],
            selectedValues: [],
            isEdit: false,

            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1,
                last_page: 1,
            },
            transactions: [],
            transaction_id:null,
            transaction:{
                id:null,
                created_at:'',
                book_id:null,
                qty:0,
                total_cost_of_books:0,

            }
        }
    },
    methods: {
        asyncFind(query) {
            if (query) {
                this.isSingleLoading = true;
                axios.get('/api/book/search/' + query ).then(response => {
                    this.collections = response.data;
                    this.isSingleLoading = false;
                });
            }
        },
        formatDate(date){
            return Helper.formatDate(date);
        },
        onValueChange(value){
            if(value.id){
                this.transaction.book_id=value.id;
            }
        },
        clearData() {
            this.transaction.id = null;
            this.transaction.created_at = this.formatDate(new Date());
            this.transaction.book_id=null;
            this.transaction.qty=0;
            this.transaction.total_cost_of_books=0;

            this.selectedValues=[];
            this.$validator.clean();
        },
        getTransactions(url) {
            axios.get(url + this.pagination.current_page).then(response => {
                this.pagination = response.data;
                this.transactions = response.data.data;
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else {
                    this.$parent.showToast('Error occured while loading data.');
                }
            });
        },
        toggleAddModal() {
            this.clearData();
            this.isEdit = false;
        },
        toggleEdit(transa) {
            var temp = Object.assign({}, transa);

            this.transaction.id = temp.id;
            this.transaction.book_id = temp.book.id;
            this.transaction.total_cost_of_books = temp.total_cost_of_books;
            this.transaction.qty = temp.qty;
            this.transaction.created_at = this.formatDate(temp.created_at);
            this.selectedValues = {id: temp.book.id, bookTitle: temp.book.bookTitle};
            this.isEdit = true;
        },
        toggleDelete(id) {
            this.transaction_id = id;
        },
        onPaginationClick() {
            this.getTransactions(this.route.get);
        },

        onSearchClick() {
            this.getTransactions(this.route.get);
        },

        submitdata(){
            this.$validator.validateAll().then(successsValidate => {
                if (successsValidate) {
                    let _url = this.isEdit ? this.route.edit : this.route.create;
                    this.performProcess(_url);
                }
            }).catch(() => {
                this.$parent.showToast('Opps,Something has gone wrong.', 'warn');
            });
        },
        performProcess(url) {
            axios.post(url, this.transaction).then(response => {
                this.$parent.showToast('Success', 'success');
                this.getTransactions(this.route.get);
                $('#stock_modal').modal('close');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else if(error.response.data.status == 422) {
                    this.$parent.showToast('Invalid data.', 'warn');
                }else{
                    this.$parent.showToast('An occured while ' + this.isEdit ? 'updating' : 'inserting' + ' data.Please try agian!', 'error');
                }
            });
        },
        performdelete() {
            axios.get(this.route.delete + this.transaction_id).then(response => {
                this.getTransactions(this.route.get);
                this.transaction_id = null;
                this.$parent.showToast('Success', 'success');
            }).catch(error => {
                if (error.response.data.status == 401) {
                    window.location.href=Helper.loginPage();
                }else{
                    this.$parent.showToast('Error occured while deleting data.');
                }
            });
            $('#delete-modal').modal('close');
        },
    },
    mounted() {
        this.getTransactions(this.route.get);
    }
}